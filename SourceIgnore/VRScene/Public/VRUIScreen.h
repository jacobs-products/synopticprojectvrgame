#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/Button.h"
#include "VRUIScreen.generated.h"

UCLASS()
class VRSCENE_API UVRUIScreen : public UUserWidget
{
    GENERATED_BODY()

public:
    UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
        UButton* VRButton;
};