#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "VRUIScreen.h"
#include "CloseButtonClass.generated.h"

UCLASS()
class VRSCENE_API ACloseButtonClass : public AActor
{
    GENERATED_BODY()

public:
    // Sets default values for this actor's properties
    ACloseButtonClass();

protected:
    // Called when the game starts or when spawned
    virtual void BeginPlay() override;

public:
    // Called every frame
    virtual void Tick(float DeltaTime) override;

    // Function to handle button click
    UFUNCTION()
        void OnButtonClicked();

    // Reference to your Widget
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
        TSubclassOf<UUserWidget> UserWidgetClass;

    UVRUIScreen* UserWidget;
};