#include "CloseButtonClass.h"
#include "Blueprint/UserWidget.h"

// Sets default values
ACloseButtonClass::ACloseButtonClass()
{
    // Set this actor to call Tick() every frame
    PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ACloseButtonClass::BeginPlay()
{
    Super::BeginPlay();

    // Create a UUserWidget
    if (UserWidgetClass)
    {
        UserWidget = Cast<UVRUIScreen>(CreateWidget<UUserWidget>(GetWorld(), UserWidgetClass));

        // Bind the OnClicked event of the Button to the custom function
        if (UserWidget && UserWidget->VRButton)
        {
            UserWidget->VRButton->OnClicked.AddDynamic(this, &ACloseButtonClass::OnButtonClicked);
        }
    }
}

// Called every frame
void ACloseButtonClass::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
}

void ACloseButtonClass::OnButtonClicked()
{
    if (UserWidget)
    {
        UserWidget->SetVisibility(ESlateVisibility::Hidden);
    }
}