// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "OculusXRSceneAnchorComponent.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef OCULUSXRSCENE_OculusXRSceneAnchorComponent_generated_h
#error "OculusXRSceneAnchorComponent.generated.h already included, missing '#pragma once' in OculusXRSceneAnchorComponent.h"
#endif
#define OCULUSXRSCENE_OculusXRSceneAnchorComponent_generated_h

#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRScene_Public_OculusXRSceneAnchorComponent_h_18_SPARSE_DATA
#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRScene_Public_OculusXRSceneAnchorComponent_h_18_RPC_WRAPPERS
#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRScene_Public_OculusXRSceneAnchorComponent_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRScene_Public_OculusXRSceneAnchorComponent_h_18_ACCESSORS
#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRScene_Public_OculusXRSceneAnchorComponent_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUOculusXRSceneAnchorComponent(); \
	friend struct Z_Construct_UClass_UOculusXRSceneAnchorComponent_Statics; \
public: \
	DECLARE_CLASS(UOculusXRSceneAnchorComponent, UOculusXRAnchorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/OculusXRScene"), NO_API) \
	DECLARE_SERIALIZER(UOculusXRSceneAnchorComponent)


#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRScene_Public_OculusXRSceneAnchorComponent_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUOculusXRSceneAnchorComponent(); \
	friend struct Z_Construct_UClass_UOculusXRSceneAnchorComponent_Statics; \
public: \
	DECLARE_CLASS(UOculusXRSceneAnchorComponent, UOculusXRAnchorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/OculusXRScene"), NO_API) \
	DECLARE_SERIALIZER(UOculusXRSceneAnchorComponent)


#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRScene_Public_OculusXRSceneAnchorComponent_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UOculusXRSceneAnchorComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOculusXRSceneAnchorComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOculusXRSceneAnchorComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOculusXRSceneAnchorComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOculusXRSceneAnchorComponent(UOculusXRSceneAnchorComponent&&); \
	NO_API UOculusXRSceneAnchorComponent(const UOculusXRSceneAnchorComponent&); \
public: \
	NO_API virtual ~UOculusXRSceneAnchorComponent();


#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRScene_Public_OculusXRSceneAnchorComponent_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOculusXRSceneAnchorComponent(UOculusXRSceneAnchorComponent&&); \
	NO_API UOculusXRSceneAnchorComponent(const UOculusXRSceneAnchorComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOculusXRSceneAnchorComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOculusXRSceneAnchorComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOculusXRSceneAnchorComponent) \
	NO_API virtual ~UOculusXRSceneAnchorComponent();


#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRScene_Public_OculusXRSceneAnchorComponent_h_15_PROLOG
#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRScene_Public_OculusXRSceneAnchorComponent_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRScene_Public_OculusXRSceneAnchorComponent_h_18_SPARSE_DATA \
	FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRScene_Public_OculusXRSceneAnchorComponent_h_18_RPC_WRAPPERS \
	FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRScene_Public_OculusXRSceneAnchorComponent_h_18_ACCESSORS \
	FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRScene_Public_OculusXRSceneAnchorComponent_h_18_INCLASS \
	FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRScene_Public_OculusXRSceneAnchorComponent_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRScene_Public_OculusXRSceneAnchorComponent_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRScene_Public_OculusXRSceneAnchorComponent_h_18_SPARSE_DATA \
	FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRScene_Public_OculusXRSceneAnchorComponent_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRScene_Public_OculusXRSceneAnchorComponent_h_18_ACCESSORS \
	FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRScene_Public_OculusXRSceneAnchorComponent_h_18_INCLASS_NO_PURE_DECLS \
	FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRScene_Public_OculusXRSceneAnchorComponent_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> OCULUSXRSCENE_API UClass* StaticClass<class UOculusXRSceneAnchorComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRScene_Public_OculusXRSceneAnchorComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
