// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeOculusXRScene_init() {}
	static FPackageRegistrationInfo Z_Registration_Info_UPackage__Script_OculusXRScene;
	FORCENOINLINE UPackage* Z_Construct_UPackage__Script_OculusXRScene()
	{
		if (!Z_Registration_Info_UPackage__Script_OculusXRScene.OuterSingleton)
		{
			static const UECodeGen_Private::FPackageParams PackageParams = {
				"/Script/OculusXRScene",
				nullptr,
				0,
				PKG_CompiledIn | 0x00000000,
				0xFB7B6A59,
				0x47BD7CA9,
				METADATA_PARAMS(nullptr, 0)
			};
			UECodeGen_Private::ConstructUPackage(Z_Registration_Info_UPackage__Script_OculusXRScene.OuterSingleton, PackageParams);
		}
		return Z_Registration_Info_UPackage__Script_OculusXRScene.OuterSingleton;
	}
	static FRegisterCompiledInInfo Z_CompiledInDeferPackage_UPackage__Script_OculusXRScene(Z_Construct_UPackage__Script_OculusXRScene, TEXT("/Script/OculusXRScene"), Z_Registration_Info_UPackage__Script_OculusXRScene, CONSTRUCT_RELOAD_VERSION_INFO(FPackageReloadVersionInfo, 0xFB7B6A59, 0x47BD7CA9));
PRAGMA_ENABLE_DEPRECATION_WARNINGS
