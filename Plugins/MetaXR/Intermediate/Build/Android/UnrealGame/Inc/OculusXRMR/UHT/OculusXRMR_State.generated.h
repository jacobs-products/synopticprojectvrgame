// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "OculusXRMR_State.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef OCULUSXRMR_OculusXRMR_State_generated_h
#error "OculusXRMR_State.generated.h already included, missing '#pragma once' in OculusXRMR_State.h"
#endif
#define OCULUSXRMR_OculusXRMR_State_generated_h

#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMR_Private_OculusXRMR_State_h_13_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FOculusXRTrackedCamera_Statics; \
	OCULUSXRMR_API static class UScriptStruct* StaticStruct();


template<> OCULUSXRMR_API UScriptStruct* StaticStruct<struct FOculusXRTrackedCamera>();

#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMR_Private_OculusXRMR_State_h_93_SPARSE_DATA
#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMR_Private_OculusXRMR_State_h_93_RPC_WRAPPERS
#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMR_Private_OculusXRMR_State_h_93_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMR_Private_OculusXRMR_State_h_93_ACCESSORS
#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMR_Private_OculusXRMR_State_h_93_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUOculusXRMR_State(); \
	friend struct Z_Construct_UClass_UOculusXRMR_State_Statics; \
public: \
	DECLARE_CLASS(UOculusXRMR_State, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/OculusXRMR"), NO_API) \
	DECLARE_SERIALIZER(UOculusXRMR_State)


#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMR_Private_OculusXRMR_State_h_93_INCLASS \
private: \
	static void StaticRegisterNativesUOculusXRMR_State(); \
	friend struct Z_Construct_UClass_UOculusXRMR_State_Statics; \
public: \
	DECLARE_CLASS(UOculusXRMR_State, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/OculusXRMR"), NO_API) \
	DECLARE_SERIALIZER(UOculusXRMR_State)


#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMR_Private_OculusXRMR_State_h_93_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UOculusXRMR_State(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOculusXRMR_State) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOculusXRMR_State); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOculusXRMR_State); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOculusXRMR_State(UOculusXRMR_State&&); \
	NO_API UOculusXRMR_State(const UOculusXRMR_State&); \
public: \
	NO_API virtual ~UOculusXRMR_State();


#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMR_Private_OculusXRMR_State_h_93_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOculusXRMR_State(UOculusXRMR_State&&); \
	NO_API UOculusXRMR_State(const UOculusXRMR_State&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOculusXRMR_State); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOculusXRMR_State); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOculusXRMR_State) \
	NO_API virtual ~UOculusXRMR_State();


#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMR_Private_OculusXRMR_State_h_90_PROLOG
#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMR_Private_OculusXRMR_State_h_93_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMR_Private_OculusXRMR_State_h_93_SPARSE_DATA \
	FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMR_Private_OculusXRMR_State_h_93_RPC_WRAPPERS \
	FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMR_Private_OculusXRMR_State_h_93_ACCESSORS \
	FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMR_Private_OculusXRMR_State_h_93_INCLASS \
	FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMR_Private_OculusXRMR_State_h_93_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMR_Private_OculusXRMR_State_h_93_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMR_Private_OculusXRMR_State_h_93_SPARSE_DATA \
	FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMR_Private_OculusXRMR_State_h_93_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMR_Private_OculusXRMR_State_h_93_ACCESSORS \
	FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMR_Private_OculusXRMR_State_h_93_INCLASS_NO_PURE_DECLS \
	FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMR_Private_OculusXRMR_State_h_93_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> OCULUSXRMR_API UClass* StaticClass<class UOculusXRMR_State>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMR_Private_OculusXRMR_State_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
