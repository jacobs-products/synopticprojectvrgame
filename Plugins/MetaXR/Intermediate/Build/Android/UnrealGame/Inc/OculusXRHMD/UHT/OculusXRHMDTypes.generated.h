// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "OculusXRHMDTypes.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef OCULUSXRHMD_OculusXRHMDTypes_generated_h
#error "OculusXRHMDTypes.generated.h already included, missing '#pragma once' in OculusXRHMDTypes.h"
#endif
#define OCULUSXRHMD_OculusXRHMDTypes_generated_h

#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRHMD_Public_OculusXRHMDTypes_h_16_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FOculusXRSplashDesc_Statics; \
	OCULUSXRHMD_API static class UScriptStruct* StaticStruct();


template<> OCULUSXRHMD_API UScriptStruct* StaticStruct<struct FOculusXRSplashDesc>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRHMD_Public_OculusXRHMDTypes_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
