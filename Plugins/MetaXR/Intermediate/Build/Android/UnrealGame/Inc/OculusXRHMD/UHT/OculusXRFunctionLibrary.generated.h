// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "OculusXRFunctionLibrary.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UTexture2D;
enum class EControllerHand : uint8;
enum class EOculusXRBoundaryType : uint8;
enum class EOculusXRColorSpace : uint8;
enum class EOculusXRControllerType : uint8;
enum class EOculusXRDeviceType : uint8;
enum class EOculusXRFoveatedRenderingLevel : uint8;
enum class EOculusXRFoveatedRenderingMethod : uint8;
enum class EOculusXRProcessorPerformanceLevel : uint8;
enum class EOculusXRTrackedDeviceType : uint8;
struct FLinearColor;
struct FOculusXRGuardianTestResult;
struct FOculusXRHmdUserProfile;
#ifdef OCULUSXRHMD_OculusXRFunctionLibrary_generated_h
#error "OculusXRFunctionLibrary.generated.h already included, missing '#pragma once' in OculusXRFunctionLibrary.h"
#endif
#define OCULUSXRHMD_OculusXRFunctionLibrary_generated_h

#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRHMD_Public_OculusXRFunctionLibrary_h_32_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FOculusXRHmdUserProfileField_Statics; \
	OCULUSXRHMD_API static class UScriptStruct* StaticStruct();


template<> OCULUSXRHMD_API UScriptStruct* StaticStruct<struct FOculusXRHmdUserProfileField>();

#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRHMD_Public_OculusXRFunctionLibrary_h_48_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FOculusXRHmdUserProfile_Statics; \
	OCULUSXRHMD_API static class UScriptStruct* StaticStruct();


template<> OCULUSXRHMD_API UScriptStruct* StaticStruct<struct FOculusXRHmdUserProfile>();

#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRHMD_Public_OculusXRFunctionLibrary_h_212_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FOculusXRGuardianTestResult_Statics; \
	OCULUSXRHMD_API static class UScriptStruct* StaticStruct();


template<> OCULUSXRHMD_API UScriptStruct* StaticStruct<struct FOculusXRGuardianTestResult>();

#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRHMD_Public_OculusXRFunctionLibrary_h_248_SPARSE_DATA
#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRHMD_Public_OculusXRFunctionLibrary_h_248_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSetGuardianVisibility); \
	DECLARE_FUNCTION(execGetNodeGuardianIntersection); \
	DECLARE_FUNCTION(execGetPointGuardianIntersection); \
	DECLARE_FUNCTION(execGetPlayAreaTransform); \
	DECLARE_FUNCTION(execGetGuardianDimensions); \
	DECLARE_FUNCTION(execGetGuardianPoints); \
	DECLARE_FUNCTION(execIsGuardianConfigured); \
	DECLARE_FUNCTION(execIsGuardianDisplayed); \
	DECLARE_FUNCTION(execIsColorPassthroughSupported); \
	DECLARE_FUNCTION(execIsPassthroughSupported); \
	DECLARE_FUNCTION(execSetLocalDimmingOn); \
	DECLARE_FUNCTION(execSetClientColorDesc); \
	DECLARE_FUNCTION(execGetHmdColorDesc); \
	DECLARE_FUNCTION(execGetSystemHmd3DofModeEnabled); \
	DECLARE_FUNCTION(execSetColorScaleAndOffset); \
	DECLARE_FUNCTION(execEnableOrientationTracking); \
	DECLARE_FUNCTION(execEnablePositionTracking); \
	DECLARE_FUNCTION(execSetDisplayFrequency); \
	DECLARE_FUNCTION(execGetCurrentDisplayFrequency); \
	DECLARE_FUNCTION(execGetAvailableDisplayFrequencies); \
	DECLARE_FUNCTION(execGetControllerType); \
	DECLARE_FUNCTION(execGetDeviceType); \
	DECLARE_FUNCTION(execGetDeviceName); \
	DECLARE_FUNCTION(execGetEyeTrackedFoveatedRenderingSupported); \
	DECLARE_FUNCTION(execSetFoveatedRenderingLevel); \
	DECLARE_FUNCTION(execGetFoveatedRenderingLevel); \
	DECLARE_FUNCTION(execSetFoveatedRenderingMethod); \
	DECLARE_FUNCTION(execGetFoveatedRenderingMethod); \
	DECLARE_FUNCTION(execGetGPUFrameTime); \
	DECLARE_FUNCTION(execGetGPUUtilization); \
	DECLARE_FUNCTION(execHasSystemOverlayPresent); \
	DECLARE_FUNCTION(execHasInputFocus); \
	DECLARE_FUNCTION(execClearLoadingSplashScreens); \
	DECLARE_FUNCTION(execAddLoadingSplashScreen); \
	DECLARE_FUNCTION(execGetBaseRotationAndPositionOffset); \
	DECLARE_FUNCTION(execSetBaseRotationAndPositionOffset); \
	DECLARE_FUNCTION(execSetPositionScale3D); \
	DECLARE_FUNCTION(execGetBaseRotationAndBaseOffsetInMeters); \
	DECLARE_FUNCTION(execSetBaseRotationAndBaseOffsetInMeters); \
	DECLARE_FUNCTION(execGetUserProfile); \
	DECLARE_FUNCTION(execSetSuggestedCpuAndGpuPerformanceLevels); \
	DECLARE_FUNCTION(execGetSuggestedCpuAndGpuPerformanceLevels); \
	DECLARE_FUNCTION(execSetCPUAndGPULevels); \
	DECLARE_FUNCTION(execIsDeviceTracked); \
	DECLARE_FUNCTION(execGetRawSensorData); \
	DECLARE_FUNCTION(execGetPose);


#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRHMD_Public_OculusXRFunctionLibrary_h_248_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSetGuardianVisibility); \
	DECLARE_FUNCTION(execGetNodeGuardianIntersection); \
	DECLARE_FUNCTION(execGetPointGuardianIntersection); \
	DECLARE_FUNCTION(execGetPlayAreaTransform); \
	DECLARE_FUNCTION(execGetGuardianDimensions); \
	DECLARE_FUNCTION(execGetGuardianPoints); \
	DECLARE_FUNCTION(execIsGuardianConfigured); \
	DECLARE_FUNCTION(execIsGuardianDisplayed); \
	DECLARE_FUNCTION(execIsColorPassthroughSupported); \
	DECLARE_FUNCTION(execIsPassthroughSupported); \
	DECLARE_FUNCTION(execSetLocalDimmingOn); \
	DECLARE_FUNCTION(execSetClientColorDesc); \
	DECLARE_FUNCTION(execGetHmdColorDesc); \
	DECLARE_FUNCTION(execGetSystemHmd3DofModeEnabled); \
	DECLARE_FUNCTION(execSetColorScaleAndOffset); \
	DECLARE_FUNCTION(execEnableOrientationTracking); \
	DECLARE_FUNCTION(execEnablePositionTracking); \
	DECLARE_FUNCTION(execSetDisplayFrequency); \
	DECLARE_FUNCTION(execGetCurrentDisplayFrequency); \
	DECLARE_FUNCTION(execGetAvailableDisplayFrequencies); \
	DECLARE_FUNCTION(execGetControllerType); \
	DECLARE_FUNCTION(execGetDeviceType); \
	DECLARE_FUNCTION(execGetDeviceName); \
	DECLARE_FUNCTION(execGetEyeTrackedFoveatedRenderingSupported); \
	DECLARE_FUNCTION(execSetFoveatedRenderingLevel); \
	DECLARE_FUNCTION(execGetFoveatedRenderingLevel); \
	DECLARE_FUNCTION(execSetFoveatedRenderingMethod); \
	DECLARE_FUNCTION(execGetFoveatedRenderingMethod); \
	DECLARE_FUNCTION(execGetGPUFrameTime); \
	DECLARE_FUNCTION(execGetGPUUtilization); \
	DECLARE_FUNCTION(execHasSystemOverlayPresent); \
	DECLARE_FUNCTION(execHasInputFocus); \
	DECLARE_FUNCTION(execClearLoadingSplashScreens); \
	DECLARE_FUNCTION(execAddLoadingSplashScreen); \
	DECLARE_FUNCTION(execGetBaseRotationAndPositionOffset); \
	DECLARE_FUNCTION(execSetBaseRotationAndPositionOffset); \
	DECLARE_FUNCTION(execSetPositionScale3D); \
	DECLARE_FUNCTION(execGetBaseRotationAndBaseOffsetInMeters); \
	DECLARE_FUNCTION(execSetBaseRotationAndBaseOffsetInMeters); \
	DECLARE_FUNCTION(execGetUserProfile); \
	DECLARE_FUNCTION(execSetSuggestedCpuAndGpuPerformanceLevels); \
	DECLARE_FUNCTION(execGetSuggestedCpuAndGpuPerformanceLevels); \
	DECLARE_FUNCTION(execSetCPUAndGPULevels); \
	DECLARE_FUNCTION(execIsDeviceTracked); \
	DECLARE_FUNCTION(execGetRawSensorData); \
	DECLARE_FUNCTION(execGetPose);


#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRHMD_Public_OculusXRFunctionLibrary_h_248_ACCESSORS
#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRHMD_Public_OculusXRFunctionLibrary_h_248_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUOculusXRFunctionLibrary(); \
	friend struct Z_Construct_UClass_UOculusXRFunctionLibrary_Statics; \
public: \
	DECLARE_CLASS(UOculusXRFunctionLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/OculusXRHMD"), NO_API) \
	DECLARE_SERIALIZER(UOculusXRFunctionLibrary)


#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRHMD_Public_OculusXRFunctionLibrary_h_248_INCLASS \
private: \
	static void StaticRegisterNativesUOculusXRFunctionLibrary(); \
	friend struct Z_Construct_UClass_UOculusXRFunctionLibrary_Statics; \
public: \
	DECLARE_CLASS(UOculusXRFunctionLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/OculusXRHMD"), NO_API) \
	DECLARE_SERIALIZER(UOculusXRFunctionLibrary)


#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRHMD_Public_OculusXRFunctionLibrary_h_248_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UOculusXRFunctionLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOculusXRFunctionLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOculusXRFunctionLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOculusXRFunctionLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOculusXRFunctionLibrary(UOculusXRFunctionLibrary&&); \
	NO_API UOculusXRFunctionLibrary(const UOculusXRFunctionLibrary&); \
public: \
	NO_API virtual ~UOculusXRFunctionLibrary();


#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRHMD_Public_OculusXRFunctionLibrary_h_248_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UOculusXRFunctionLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOculusXRFunctionLibrary(UOculusXRFunctionLibrary&&); \
	NO_API UOculusXRFunctionLibrary(const UOculusXRFunctionLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOculusXRFunctionLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOculusXRFunctionLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOculusXRFunctionLibrary) \
	NO_API virtual ~UOculusXRFunctionLibrary();


#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRHMD_Public_OculusXRFunctionLibrary_h_245_PROLOG
#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRHMD_Public_OculusXRFunctionLibrary_h_248_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRHMD_Public_OculusXRFunctionLibrary_h_248_SPARSE_DATA \
	FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRHMD_Public_OculusXRFunctionLibrary_h_248_RPC_WRAPPERS \
	FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRHMD_Public_OculusXRFunctionLibrary_h_248_ACCESSORS \
	FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRHMD_Public_OculusXRFunctionLibrary_h_248_INCLASS \
	FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRHMD_Public_OculusXRFunctionLibrary_h_248_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRHMD_Public_OculusXRFunctionLibrary_h_248_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRHMD_Public_OculusXRFunctionLibrary_h_248_SPARSE_DATA \
	FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRHMD_Public_OculusXRFunctionLibrary_h_248_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRHMD_Public_OculusXRFunctionLibrary_h_248_ACCESSORS \
	FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRHMD_Public_OculusXRFunctionLibrary_h_248_INCLASS_NO_PURE_DECLS \
	FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRHMD_Public_OculusXRFunctionLibrary_h_248_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class OculusXRFunctionLibrary."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> OCULUSXRHMD_API UClass* StaticClass<class UOculusXRFunctionLibrary>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRHMD_Public_OculusXRFunctionLibrary_h


#define FOREACH_ENUM_EOCULUSXRTRACKEDDEVICETYPE(op) \
	op(EOculusXRTrackedDeviceType::None) \
	op(EOculusXRTrackedDeviceType::HMD) \
	op(EOculusXRTrackedDeviceType::LTouch) \
	op(EOculusXRTrackedDeviceType::RTouch) \
	op(EOculusXRTrackedDeviceType::Touch) \
	op(EOculusXRTrackedDeviceType::DeviceObjectZero) \
	op(EOculusXRTrackedDeviceType::All) 

enum class EOculusXRTrackedDeviceType : uint8;
template<> struct TIsUEnumClass<EOculusXRTrackedDeviceType> { enum { Value = true }; };
template<> OCULUSXRHMD_API UEnum* StaticEnum<EOculusXRTrackedDeviceType>();

#define FOREACH_ENUM_EOCULUSXRFOVEATEDRENDERINGMETHOD(op) \
	op(EOculusXRFoveatedRenderingMethod::FixedFoveatedRendering) \
	op(EOculusXRFoveatedRenderingMethod::EyeTrackedFoveatedRendering) 

enum class EOculusXRFoveatedRenderingMethod : uint8;
template<> struct TIsUEnumClass<EOculusXRFoveatedRenderingMethod> { enum { Value = true }; };
template<> OCULUSXRHMD_API UEnum* StaticEnum<EOculusXRFoveatedRenderingMethod>();

#define FOREACH_ENUM_EOCULUSXRFOVEATEDRENDERINGLEVEL(op) \
	op(EOculusXRFoveatedRenderingLevel::Off) \
	op(EOculusXRFoveatedRenderingLevel::Low) \
	op(EOculusXRFoveatedRenderingLevel::Medium) \
	op(EOculusXRFoveatedRenderingLevel::High) \
	op(EOculusXRFoveatedRenderingLevel::HighTop) 

enum class EOculusXRFoveatedRenderingLevel : uint8;
template<> struct TIsUEnumClass<EOculusXRFoveatedRenderingLevel> { enum { Value = true }; };
template<> OCULUSXRHMD_API UEnum* StaticEnum<EOculusXRFoveatedRenderingLevel>();

#define FOREACH_ENUM_EOCULUSXRBOUNDARYTYPE(op) \
	op(EOculusXRBoundaryType::Boundary_Outer) \
	op(EOculusXRBoundaryType::Boundary_PlayArea) 

enum class EOculusXRBoundaryType : uint8;
template<> struct TIsUEnumClass<EOculusXRBoundaryType> { enum { Value = true }; };
template<> OCULUSXRHMD_API UEnum* StaticEnum<EOculusXRBoundaryType>();

#define FOREACH_ENUM_EOCULUSXRCOLORSPACE(op) \
	op(EOculusXRColorSpace::Unknown) \
	op(EOculusXRColorSpace::Unmanaged) \
	op(EOculusXRColorSpace::Rec_2020) \
	op(EOculusXRColorSpace::Rec_709) \
	op(EOculusXRColorSpace::Rift_CV1) \
	op(EOculusXRColorSpace::Rift_S) \
	op(EOculusXRColorSpace::Quest) \
	op(EOculusXRColorSpace::P3) \
	op(EOculusXRColorSpace::Adobe_RGB) 

enum class EOculusXRColorSpace : uint8;
template<> struct TIsUEnumClass<EOculusXRColorSpace> { enum { Value = true }; };
template<> OCULUSXRHMD_API UEnum* StaticEnum<EOculusXRColorSpace>();

#define FOREACH_ENUM_EOCULUSXRHANDTRACKINGSUPPORT(op) \
	op(EOculusXRHandTrackingSupport::ControllersOnly) \
	op(EOculusXRHandTrackingSupport::ControllersAndHands) \
	op(EOculusXRHandTrackingSupport::HandsOnly) 

enum class EOculusXRHandTrackingSupport : uint8;
template<> struct TIsUEnumClass<EOculusXRHandTrackingSupport> { enum { Value = true }; };
template<> OCULUSXRHMD_API UEnum* StaticEnum<EOculusXRHandTrackingSupport>();

#define FOREACH_ENUM_EOCULUSXRHANDTRACKINGFREQUENCY(op) \
	op(EOculusXRHandTrackingFrequency::LOW) \
	op(EOculusXRHandTrackingFrequency::HIGH) 

enum class EOculusXRHandTrackingFrequency : uint8;
template<> struct TIsUEnumClass<EOculusXRHandTrackingFrequency> { enum { Value = true }; };
template<> OCULUSXRHMD_API UEnum* StaticEnum<EOculusXRHandTrackingFrequency>();

#define FOREACH_ENUM_EOCULUSXRHANDTRACKINGVERSION(op) \
	op(EOculusXRHandTrackingVersion::Default) \
	op(EOculusXRHandTrackingVersion::V1) \
	op(EOculusXRHandTrackingVersion::V2) 

enum class EOculusXRHandTrackingVersion : uint8;
template<> struct TIsUEnumClass<EOculusXRHandTrackingVersion> { enum { Value = true }; };
template<> OCULUSXRHMD_API UEnum* StaticEnum<EOculusXRHandTrackingVersion>();

#define FOREACH_ENUM_EOCULUSXRPROCESSORPERFORMANCELEVEL(op) \
	op(EOculusXRProcessorPerformanceLevel::PowerSavings) \
	op(EOculusXRProcessorPerformanceLevel::SustainedLow) \
	op(EOculusXRProcessorPerformanceLevel::SustainedHigh) \
	op(EOculusXRProcessorPerformanceLevel::Boost) 

enum class EOculusXRProcessorPerformanceLevel : uint8;
template<> struct TIsUEnumClass<EOculusXRProcessorPerformanceLevel> { enum { Value = true }; };
template<> OCULUSXRHMD_API UEnum* StaticEnum<EOculusXRProcessorPerformanceLevel>();

#define FOREACH_ENUM_EOCULUSXRDEVICETYPE(op) \
	op(EOculusXRDeviceType::OculusMobile_Deprecated0) \
	op(EOculusXRDeviceType::OculusQuest_Deprecated) \
	op(EOculusXRDeviceType::OculusQuest2) \
	op(EOculusXRDeviceType::MetaQuestPro) \
	op(EOculusXRDeviceType::Rift) \
	op(EOculusXRDeviceType::Rift_S) \
	op(EOculusXRDeviceType::Quest_Link_Deprecated) \
	op(EOculusXRDeviceType::Quest2_Link) \
	op(EOculusXRDeviceType::MetaQuestProLink) \
	op(EOculusXRDeviceType::OculusUnknown) 

enum class EOculusXRDeviceType : uint8;
template<> struct TIsUEnumClass<EOculusXRDeviceType> { enum { Value = true }; };
template<> OCULUSXRHMD_API UEnum* StaticEnum<EOculusXRDeviceType>();

#define FOREACH_ENUM_EOCULUSXRCONTROLLERTYPE(op) \
	op(EOculusXRControllerType::None) \
	op(EOculusXRControllerType::MetaQuestTouch) \
	op(EOculusXRControllerType::MetaQuestTouchPro) \
	op(EOculusXRControllerType::Unknown) 

enum class EOculusXRControllerType : uint8;
template<> struct TIsUEnumClass<EOculusXRControllerType> { enum { Value = true }; };
template<> OCULUSXRHMD_API UEnum* StaticEnum<EOculusXRControllerType>();

#define FOREACH_ENUM_EOCULUSXRXRAPI(op) \
	op(EOculusXRXrApi::OVRPluginOpenXR) \
	op(EOculusXRXrApi::NativeOpenXR) 

enum class EOculusXRXrApi : uint8;
template<> struct TIsUEnumClass<EOculusXRXrApi> { enum { Value = true }; };
template<> OCULUSXRHMD_API UEnum* StaticEnum<EOculusXRXrApi>();

#define FOREACH_ENUM_EOCULUSXRCONTROLLERPOSEALIGNMENT(op) \
	op(EOculusXRControllerPoseAlignment::Default) \
	op(EOculusXRControllerPoseAlignment::Grip) \
	op(EOculusXRControllerPoseAlignment::Aim) 

enum class EOculusXRControllerPoseAlignment : uint8;
template<> struct TIsUEnumClass<EOculusXRControllerPoseAlignment> { enum { Value = true }; };
template<> OCULUSXRHMD_API UEnum* StaticEnum<EOculusXRControllerPoseAlignment>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
