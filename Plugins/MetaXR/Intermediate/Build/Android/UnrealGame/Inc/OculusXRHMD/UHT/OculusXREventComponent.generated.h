// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "OculusXREventComponent.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef OCULUSXRHMD_OculusXREventComponent_generated_h
#error "OculusXREventComponent.generated.h already included, missing '#pragma once' in OculusXREventComponent.h"
#endif
#define OCULUSXRHMD_OculusXREventComponent_generated_h

#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRHMD_Public_OculusXREventComponent_h_17_DELEGATE \
struct OculusXREventComponent_eventOculusDisplayRefreshRateChangedEventDelegate_Parms \
{ \
	float fromRefreshRate; \
	float toRefreshRate; \
}; \
static inline void FOculusDisplayRefreshRateChangedEventDelegate_DelegateWrapper(const FMulticastScriptDelegate& OculusDisplayRefreshRateChangedEventDelegate, float fromRefreshRate, float toRefreshRate) \
{ \
	OculusXREventComponent_eventOculusDisplayRefreshRateChangedEventDelegate_Parms Parms; \
	Parms.fromRefreshRate=fromRefreshRate; \
	Parms.toRefreshRate=toRefreshRate; \
	OculusDisplayRefreshRateChangedEventDelegate.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRHMD_Public_OculusXREventComponent_h_18_DELEGATE \
struct OculusXREventComponent_eventOculusEyeTrackingStateChangedEventDelegate_Parms \
{ \
	bool bEyeTrackingOn; \
}; \
static inline void FOculusEyeTrackingStateChangedEventDelegate_DelegateWrapper(const FMulticastScriptDelegate& OculusEyeTrackingStateChangedEventDelegate, bool bEyeTrackingOn) \
{ \
	OculusXREventComponent_eventOculusEyeTrackingStateChangedEventDelegate_Parms Parms; \
	Parms.bEyeTrackingOn=bEyeTrackingOn ? true : false; \
	OculusEyeTrackingStateChangedEventDelegate.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRHMD_Public_OculusXREventComponent_h_14_SPARSE_DATA
#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRHMD_Public_OculusXREventComponent_h_14_RPC_WRAPPERS
#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRHMD_Public_OculusXREventComponent_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRHMD_Public_OculusXREventComponent_h_14_ACCESSORS
#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRHMD_Public_OculusXREventComponent_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUOculusXREventComponent(); \
	friend struct Z_Construct_UClass_UOculusXREventComponent_Statics; \
public: \
	DECLARE_CLASS(UOculusXREventComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/OculusXRHMD"), NO_API) \
	DECLARE_SERIALIZER(UOculusXREventComponent)


#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRHMD_Public_OculusXREventComponent_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUOculusXREventComponent(); \
	friend struct Z_Construct_UClass_UOculusXREventComponent_Statics; \
public: \
	DECLARE_CLASS(UOculusXREventComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/OculusXRHMD"), NO_API) \
	DECLARE_SERIALIZER(UOculusXREventComponent)


#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRHMD_Public_OculusXREventComponent_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UOculusXREventComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOculusXREventComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOculusXREventComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOculusXREventComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOculusXREventComponent(UOculusXREventComponent&&); \
	NO_API UOculusXREventComponent(const UOculusXREventComponent&); \
public: \
	NO_API virtual ~UOculusXREventComponent();


#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRHMD_Public_OculusXREventComponent_h_14_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UOculusXREventComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOculusXREventComponent(UOculusXREventComponent&&); \
	NO_API UOculusXREventComponent(const UOculusXREventComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOculusXREventComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOculusXREventComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOculusXREventComponent) \
	NO_API virtual ~UOculusXREventComponent();


#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRHMD_Public_OculusXREventComponent_h_11_PROLOG
#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRHMD_Public_OculusXREventComponent_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRHMD_Public_OculusXREventComponent_h_14_SPARSE_DATA \
	FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRHMD_Public_OculusXREventComponent_h_14_RPC_WRAPPERS \
	FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRHMD_Public_OculusXREventComponent_h_14_ACCESSORS \
	FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRHMD_Public_OculusXREventComponent_h_14_INCLASS \
	FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRHMD_Public_OculusXREventComponent_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRHMD_Public_OculusXREventComponent_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRHMD_Public_OculusXREventComponent_h_14_SPARSE_DATA \
	FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRHMD_Public_OculusXREventComponent_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRHMD_Public_OculusXREventComponent_h_14_ACCESSORS \
	FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRHMD_Public_OculusXREventComponent_h_14_INCLASS_NO_PURE_DECLS \
	FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRHMD_Public_OculusXREventComponent_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> OCULUSXRHMD_API UClass* StaticClass<class UOculusXREventComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRHMD_Public_OculusXREventComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
