// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "OculusXREyeTrackingComponent.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef OCULUSXRMOVEMENT_OculusXREyeTrackingComponent_generated_h
#error "OculusXREyeTrackingComponent.generated.h already included, missing '#pragma once' in OculusXREyeTrackingComponent.h"
#endif
#define OCULUSXRMOVEMENT_OculusXREyeTrackingComponent_generated_h

#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMovement_Public_OculusXREyeTrackingComponent_h_35_SPARSE_DATA
#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMovement_Public_OculusXREyeTrackingComponent_h_35_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execClearRotationValues);


#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMovement_Public_OculusXREyeTrackingComponent_h_35_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execClearRotationValues);


#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMovement_Public_OculusXREyeTrackingComponent_h_35_ACCESSORS
#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMovement_Public_OculusXREyeTrackingComponent_h_35_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUOculusXREyeTrackingComponent(); \
	friend struct Z_Construct_UClass_UOculusXREyeTrackingComponent_Statics; \
public: \
	DECLARE_CLASS(UOculusXREyeTrackingComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/OculusXRMovement"), NO_API) \
	DECLARE_SERIALIZER(UOculusXREyeTrackingComponent)


#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMovement_Public_OculusXREyeTrackingComponent_h_35_INCLASS \
private: \
	static void StaticRegisterNativesUOculusXREyeTrackingComponent(); \
	friend struct Z_Construct_UClass_UOculusXREyeTrackingComponent_Statics; \
public: \
	DECLARE_CLASS(UOculusXREyeTrackingComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/OculusXRMovement"), NO_API) \
	DECLARE_SERIALIZER(UOculusXREyeTrackingComponent)


#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMovement_Public_OculusXREyeTrackingComponent_h_35_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UOculusXREyeTrackingComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOculusXREyeTrackingComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOculusXREyeTrackingComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOculusXREyeTrackingComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOculusXREyeTrackingComponent(UOculusXREyeTrackingComponent&&); \
	NO_API UOculusXREyeTrackingComponent(const UOculusXREyeTrackingComponent&); \
public: \
	NO_API virtual ~UOculusXREyeTrackingComponent();


#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMovement_Public_OculusXREyeTrackingComponent_h_35_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOculusXREyeTrackingComponent(UOculusXREyeTrackingComponent&&); \
	NO_API UOculusXREyeTrackingComponent(const UOculusXREyeTrackingComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOculusXREyeTrackingComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOculusXREyeTrackingComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UOculusXREyeTrackingComponent) \
	NO_API virtual ~UOculusXREyeTrackingComponent();


#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMovement_Public_OculusXREyeTrackingComponent_h_32_PROLOG
#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMovement_Public_OculusXREyeTrackingComponent_h_35_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMovement_Public_OculusXREyeTrackingComponent_h_35_SPARSE_DATA \
	FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMovement_Public_OculusXREyeTrackingComponent_h_35_RPC_WRAPPERS \
	FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMovement_Public_OculusXREyeTrackingComponent_h_35_ACCESSORS \
	FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMovement_Public_OculusXREyeTrackingComponent_h_35_INCLASS \
	FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMovement_Public_OculusXREyeTrackingComponent_h_35_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMovement_Public_OculusXREyeTrackingComponent_h_35_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMovement_Public_OculusXREyeTrackingComponent_h_35_SPARSE_DATA \
	FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMovement_Public_OculusXREyeTrackingComponent_h_35_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMovement_Public_OculusXREyeTrackingComponent_h_35_ACCESSORS \
	FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMovement_Public_OculusXREyeTrackingComponent_h_35_INCLASS_NO_PURE_DECLS \
	FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMovement_Public_OculusXREyeTrackingComponent_h_35_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> OCULUSXRMOVEMENT_API UClass* StaticClass<class UOculusXREyeTrackingComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMovement_Public_OculusXREyeTrackingComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
