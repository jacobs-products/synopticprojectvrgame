// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "OculusXRFaceTrackingComponent.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
enum class EOculusXRFaceExpression : uint8;
#ifdef OCULUSXRMOVEMENT_OculusXRFaceTrackingComponent_generated_h
#error "OculusXRFaceTrackingComponent.generated.h already included, missing '#pragma once' in OculusXRFaceTrackingComponent.h"
#endif
#define OCULUSXRMOVEMENT_OculusXRFaceTrackingComponent_generated_h

#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMovement_Public_OculusXRFaceTrackingComponent_h_21_SPARSE_DATA
#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMovement_Public_OculusXRFaceTrackingComponent_h_21_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execClearExpressionValues); \
	DECLARE_FUNCTION(execGetExpressionValue); \
	DECLARE_FUNCTION(execSetExpressionValue);


#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMovement_Public_OculusXRFaceTrackingComponent_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execClearExpressionValues); \
	DECLARE_FUNCTION(execGetExpressionValue); \
	DECLARE_FUNCTION(execSetExpressionValue);


#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMovement_Public_OculusXRFaceTrackingComponent_h_21_ACCESSORS
#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMovement_Public_OculusXRFaceTrackingComponent_h_21_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUOculusXRFaceTrackingComponent(); \
	friend struct Z_Construct_UClass_UOculusXRFaceTrackingComponent_Statics; \
public: \
	DECLARE_CLASS(UOculusXRFaceTrackingComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/OculusXRMovement"), NO_API) \
	DECLARE_SERIALIZER(UOculusXRFaceTrackingComponent)


#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMovement_Public_OculusXRFaceTrackingComponent_h_21_INCLASS \
private: \
	static void StaticRegisterNativesUOculusXRFaceTrackingComponent(); \
	friend struct Z_Construct_UClass_UOculusXRFaceTrackingComponent_Statics; \
public: \
	DECLARE_CLASS(UOculusXRFaceTrackingComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/OculusXRMovement"), NO_API) \
	DECLARE_SERIALIZER(UOculusXRFaceTrackingComponent)


#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMovement_Public_OculusXRFaceTrackingComponent_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UOculusXRFaceTrackingComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOculusXRFaceTrackingComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOculusXRFaceTrackingComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOculusXRFaceTrackingComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOculusXRFaceTrackingComponent(UOculusXRFaceTrackingComponent&&); \
	NO_API UOculusXRFaceTrackingComponent(const UOculusXRFaceTrackingComponent&); \
public: \
	NO_API virtual ~UOculusXRFaceTrackingComponent();


#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMovement_Public_OculusXRFaceTrackingComponent_h_21_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOculusXRFaceTrackingComponent(UOculusXRFaceTrackingComponent&&); \
	NO_API UOculusXRFaceTrackingComponent(const UOculusXRFaceTrackingComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOculusXRFaceTrackingComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOculusXRFaceTrackingComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UOculusXRFaceTrackingComponent) \
	NO_API virtual ~UOculusXRFaceTrackingComponent();


#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMovement_Public_OculusXRFaceTrackingComponent_h_18_PROLOG
#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMovement_Public_OculusXRFaceTrackingComponent_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMovement_Public_OculusXRFaceTrackingComponent_h_21_SPARSE_DATA \
	FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMovement_Public_OculusXRFaceTrackingComponent_h_21_RPC_WRAPPERS \
	FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMovement_Public_OculusXRFaceTrackingComponent_h_21_ACCESSORS \
	FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMovement_Public_OculusXRFaceTrackingComponent_h_21_INCLASS \
	FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMovement_Public_OculusXRFaceTrackingComponent_h_21_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMovement_Public_OculusXRFaceTrackingComponent_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMovement_Public_OculusXRFaceTrackingComponent_h_21_SPARSE_DATA \
	FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMovement_Public_OculusXRFaceTrackingComponent_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMovement_Public_OculusXRFaceTrackingComponent_h_21_ACCESSORS \
	FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMovement_Public_OculusXRFaceTrackingComponent_h_21_INCLASS_NO_PURE_DECLS \
	FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMovement_Public_OculusXRFaceTrackingComponent_h_21_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> OCULUSXRMOVEMENT_API UClass* StaticClass<class UOculusXRFaceTrackingComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Users_jvt05_Documents_Unreal_Projects_UnrealVRScene_Plugins_MetaXR_Source_OculusXRMovement_Public_OculusXRFaceTrackingComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
