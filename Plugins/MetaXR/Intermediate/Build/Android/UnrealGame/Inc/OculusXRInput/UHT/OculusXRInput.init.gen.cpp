// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeOculusXRInput_init() {}
	static FPackageRegistrationInfo Z_Registration_Info_UPackage__Script_OculusXRInput;
	FORCENOINLINE UPackage* Z_Construct_UPackage__Script_OculusXRInput()
	{
		if (!Z_Registration_Info_UPackage__Script_OculusXRInput.OuterSingleton)
		{
			static const UECodeGen_Private::FPackageParams PackageParams = {
				"/Script/OculusXRInput",
				nullptr,
				0,
				PKG_CompiledIn | 0x00000000,
				0x398EBE0D,
				0x5BD48E20,
				METADATA_PARAMS(nullptr, 0)
			};
			UECodeGen_Private::ConstructUPackage(Z_Registration_Info_UPackage__Script_OculusXRInput.OuterSingleton, PackageParams);
		}
		return Z_Registration_Info_UPackage__Script_OculusXRInput.OuterSingleton;
	}
	static FRegisterCompiledInInfo Z_CompiledInDeferPackage_UPackage__Script_OculusXRInput(Z_Construct_UPackage__Script_OculusXRInput, TEXT("/Script/OculusXRInput"), Z_Registration_Info_UPackage__Script_OculusXRInput, CONSTRUCT_RELOAD_VERSION_INFO(FPackageReloadVersionInfo, 0x398EBE0D, 0x5BD48E20));
PRAGMA_ENABLE_DEPRECATION_WARNINGS
