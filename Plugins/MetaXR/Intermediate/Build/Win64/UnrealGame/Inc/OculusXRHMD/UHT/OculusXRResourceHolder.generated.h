// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "OculusXRResourceHolder.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef OCULUSXRHMD_OculusXRResourceHolder_generated_h
#error "OculusXRResourceHolder.generated.h already included, missing '#pragma once' in OculusXRResourceHolder.h"
#endif
#define OCULUSXRHMD_OculusXRResourceHolder_generated_h

#define FID_OculusXR_release_HostProject_Plugins_OculusXR_Source_OculusXRHMD_Private_OculusXRResourceHolder_h_16_SPARSE_DATA
#define FID_OculusXR_release_HostProject_Plugins_OculusXR_Source_OculusXRHMD_Private_OculusXRResourceHolder_h_16_RPC_WRAPPERS
#define FID_OculusXR_release_HostProject_Plugins_OculusXR_Source_OculusXRHMD_Private_OculusXRResourceHolder_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_OculusXR_release_HostProject_Plugins_OculusXR_Source_OculusXRHMD_Private_OculusXRResourceHolder_h_16_ACCESSORS
#define FID_OculusXR_release_HostProject_Plugins_OculusXR_Source_OculusXRHMD_Private_OculusXRResourceHolder_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUOculusXRResourceHolder(); \
	friend struct Z_Construct_UClass_UOculusXRResourceHolder_Statics; \
public: \
	DECLARE_CLASS(UOculusXRResourceHolder, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/OculusXRHMD"), NO_API) \
	DECLARE_SERIALIZER(UOculusXRResourceHolder)


#define FID_OculusXR_release_HostProject_Plugins_OculusXR_Source_OculusXRHMD_Private_OculusXRResourceHolder_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUOculusXRResourceHolder(); \
	friend struct Z_Construct_UClass_UOculusXRResourceHolder_Statics; \
public: \
	DECLARE_CLASS(UOculusXRResourceHolder, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/OculusXRHMD"), NO_API) \
	DECLARE_SERIALIZER(UOculusXRResourceHolder)


#define FID_OculusXR_release_HostProject_Plugins_OculusXR_Source_OculusXRHMD_Private_OculusXRResourceHolder_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UOculusXRResourceHolder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOculusXRResourceHolder) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOculusXRResourceHolder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOculusXRResourceHolder); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOculusXRResourceHolder(UOculusXRResourceHolder&&); \
	NO_API UOculusXRResourceHolder(const UOculusXRResourceHolder&); \
public: \
	NO_API virtual ~UOculusXRResourceHolder();


#define FID_OculusXR_release_HostProject_Plugins_OculusXR_Source_OculusXRHMD_Private_OculusXRResourceHolder_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UOculusXRResourceHolder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOculusXRResourceHolder(UOculusXRResourceHolder&&); \
	NO_API UOculusXRResourceHolder(const UOculusXRResourceHolder&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOculusXRResourceHolder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOculusXRResourceHolder); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOculusXRResourceHolder) \
	NO_API virtual ~UOculusXRResourceHolder();


#define FID_OculusXR_release_HostProject_Plugins_OculusXR_Source_OculusXRHMD_Private_OculusXRResourceHolder_h_13_PROLOG
#define FID_OculusXR_release_HostProject_Plugins_OculusXR_Source_OculusXRHMD_Private_OculusXRResourceHolder_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_OculusXR_release_HostProject_Plugins_OculusXR_Source_OculusXRHMD_Private_OculusXRResourceHolder_h_16_SPARSE_DATA \
	FID_OculusXR_release_HostProject_Plugins_OculusXR_Source_OculusXRHMD_Private_OculusXRResourceHolder_h_16_RPC_WRAPPERS \
	FID_OculusXR_release_HostProject_Plugins_OculusXR_Source_OculusXRHMD_Private_OculusXRResourceHolder_h_16_ACCESSORS \
	FID_OculusXR_release_HostProject_Plugins_OculusXR_Source_OculusXRHMD_Private_OculusXRResourceHolder_h_16_INCLASS \
	FID_OculusXR_release_HostProject_Plugins_OculusXR_Source_OculusXRHMD_Private_OculusXRResourceHolder_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_OculusXR_release_HostProject_Plugins_OculusXR_Source_OculusXRHMD_Private_OculusXRResourceHolder_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_OculusXR_release_HostProject_Plugins_OculusXR_Source_OculusXRHMD_Private_OculusXRResourceHolder_h_16_SPARSE_DATA \
	FID_OculusXR_release_HostProject_Plugins_OculusXR_Source_OculusXRHMD_Private_OculusXRResourceHolder_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_OculusXR_release_HostProject_Plugins_OculusXR_Source_OculusXRHMD_Private_OculusXRResourceHolder_h_16_ACCESSORS \
	FID_OculusXR_release_HostProject_Plugins_OculusXR_Source_OculusXRHMD_Private_OculusXRResourceHolder_h_16_INCLASS_NO_PURE_DECLS \
	FID_OculusXR_release_HostProject_Plugins_OculusXR_Source_OculusXRHMD_Private_OculusXRResourceHolder_h_16_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class OculusXRResourceHolder."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> OCULUSXRHMD_API UClass* StaticClass<class UOculusXRResourceHolder>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_OculusXR_release_HostProject_Plugins_OculusXR_Source_OculusXRHMD_Private_OculusXRResourceHolder_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
