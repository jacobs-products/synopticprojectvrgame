// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "OculusXRPassthroughColorLutAsset.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef OCULUSXREDITOR_OculusXRPassthroughColorLutAsset_generated_h
#error "OculusXRPassthroughColorLutAsset.generated.h already included, missing '#pragma once' in OculusXRPassthroughColorLutAsset.h"
#endif
#define OCULUSXREDITOR_OculusXRPassthroughColorLutAsset_generated_h

#define FID_OculusXR_release_HostProject_Plugins_OculusXR_Source_OculusXREditor_Private_OculusXRPassthroughColorLutAsset_h_29_SPARSE_DATA
#define FID_OculusXR_release_HostProject_Plugins_OculusXR_Source_OculusXREditor_Private_OculusXRPassthroughColorLutAsset_h_29_RPC_WRAPPERS
#define FID_OculusXR_release_HostProject_Plugins_OculusXR_Source_OculusXREditor_Private_OculusXRPassthroughColorLutAsset_h_29_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_OculusXR_release_HostProject_Plugins_OculusXR_Source_OculusXREditor_Private_OculusXRPassthroughColorLutAsset_h_29_ACCESSORS
#define FID_OculusXR_release_HostProject_Plugins_OculusXR_Source_OculusXREditor_Private_OculusXRPassthroughColorLutAsset_h_29_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUOculusXRPassthroughColorLutFactory(); \
	friend struct Z_Construct_UClass_UOculusXRPassthroughColorLutFactory_Statics; \
public: \
	DECLARE_CLASS(UOculusXRPassthroughColorLutFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/OculusXREditor"), OCULUSXREDITOR_API) \
	DECLARE_SERIALIZER(UOculusXRPassthroughColorLutFactory)


#define FID_OculusXR_release_HostProject_Plugins_OculusXR_Source_OculusXREditor_Private_OculusXRPassthroughColorLutAsset_h_29_INCLASS \
private: \
	static void StaticRegisterNativesUOculusXRPassthroughColorLutFactory(); \
	friend struct Z_Construct_UClass_UOculusXRPassthroughColorLutFactory_Statics; \
public: \
	DECLARE_CLASS(UOculusXRPassthroughColorLutFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/OculusXREditor"), OCULUSXREDITOR_API) \
	DECLARE_SERIALIZER(UOculusXRPassthroughColorLutFactory)


#define FID_OculusXR_release_HostProject_Plugins_OculusXR_Source_OculusXREditor_Private_OculusXRPassthroughColorLutAsset_h_29_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	OCULUSXREDITOR_API UOculusXRPassthroughColorLutFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOculusXRPassthroughColorLutFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(OCULUSXREDITOR_API, UOculusXRPassthroughColorLutFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOculusXRPassthroughColorLutFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	OCULUSXREDITOR_API UOculusXRPassthroughColorLutFactory(UOculusXRPassthroughColorLutFactory&&); \
	OCULUSXREDITOR_API UOculusXRPassthroughColorLutFactory(const UOculusXRPassthroughColorLutFactory&); \
public: \
	OCULUSXREDITOR_API virtual ~UOculusXRPassthroughColorLutFactory();


#define FID_OculusXR_release_HostProject_Plugins_OculusXR_Source_OculusXREditor_Private_OculusXRPassthroughColorLutAsset_h_29_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	OCULUSXREDITOR_API UOculusXRPassthroughColorLutFactory(UOculusXRPassthroughColorLutFactory&&); \
	OCULUSXREDITOR_API UOculusXRPassthroughColorLutFactory(const UOculusXRPassthroughColorLutFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(OCULUSXREDITOR_API, UOculusXRPassthroughColorLutFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOculusXRPassthroughColorLutFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOculusXRPassthroughColorLutFactory) \
	OCULUSXREDITOR_API virtual ~UOculusXRPassthroughColorLutFactory();


#define FID_OculusXR_release_HostProject_Plugins_OculusXR_Source_OculusXREditor_Private_OculusXRPassthroughColorLutAsset_h_26_PROLOG
#define FID_OculusXR_release_HostProject_Plugins_OculusXR_Source_OculusXREditor_Private_OculusXRPassthroughColorLutAsset_h_29_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_OculusXR_release_HostProject_Plugins_OculusXR_Source_OculusXREditor_Private_OculusXRPassthroughColorLutAsset_h_29_SPARSE_DATA \
	FID_OculusXR_release_HostProject_Plugins_OculusXR_Source_OculusXREditor_Private_OculusXRPassthroughColorLutAsset_h_29_RPC_WRAPPERS \
	FID_OculusXR_release_HostProject_Plugins_OculusXR_Source_OculusXREditor_Private_OculusXRPassthroughColorLutAsset_h_29_ACCESSORS \
	FID_OculusXR_release_HostProject_Plugins_OculusXR_Source_OculusXREditor_Private_OculusXRPassthroughColorLutAsset_h_29_INCLASS \
	FID_OculusXR_release_HostProject_Plugins_OculusXR_Source_OculusXREditor_Private_OculusXRPassthroughColorLutAsset_h_29_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_OculusXR_release_HostProject_Plugins_OculusXR_Source_OculusXREditor_Private_OculusXRPassthroughColorLutAsset_h_29_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_OculusXR_release_HostProject_Plugins_OculusXR_Source_OculusXREditor_Private_OculusXRPassthroughColorLutAsset_h_29_SPARSE_DATA \
	FID_OculusXR_release_HostProject_Plugins_OculusXR_Source_OculusXREditor_Private_OculusXRPassthroughColorLutAsset_h_29_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_OculusXR_release_HostProject_Plugins_OculusXR_Source_OculusXREditor_Private_OculusXRPassthroughColorLutAsset_h_29_ACCESSORS \
	FID_OculusXR_release_HostProject_Plugins_OculusXR_Source_OculusXREditor_Private_OculusXRPassthroughColorLutAsset_h_29_INCLASS_NO_PURE_DECLS \
	FID_OculusXR_release_HostProject_Plugins_OculusXR_Source_OculusXREditor_Private_OculusXRPassthroughColorLutAsset_h_29_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> OCULUSXREDITOR_API UClass* StaticClass<class UOculusXRPassthroughColorLutFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_OculusXR_release_HostProject_Plugins_OculusXR_Source_OculusXREditor_Private_OculusXRPassthroughColorLutAsset_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
