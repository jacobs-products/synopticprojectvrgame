// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "OculusXRHMD/Public/OculusXRFunctionLibrary.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeOculusXRFunctionLibrary() {}
// Cross Module References
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FLinearColor();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FRotator();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector2D();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	ENGINE_API UClass* Z_Construct_UClass_UTexture2D_NoRegister();
	HEADMOUNTEDDISPLAY_API UEnum* Z_Construct_UEnum_HeadMountedDisplay_EOrientPositionSelector();
	INPUTCORE_API UEnum* Z_Construct_UEnum_InputCore_EControllerHand();
	OCULUSXRHMD_API UClass* Z_Construct_UClass_UOculusXRFunctionLibrary();
	OCULUSXRHMD_API UClass* Z_Construct_UClass_UOculusXRFunctionLibrary_NoRegister();
	OCULUSXRHMD_API UEnum* Z_Construct_UEnum_OculusXRHMD_EOculusXRBoundaryType();
	OCULUSXRHMD_API UEnum* Z_Construct_UEnum_OculusXRHMD_EOculusXRColorSpace();
	OCULUSXRHMD_API UEnum* Z_Construct_UEnum_OculusXRHMD_EOculusXRControllerPoseAlignment();
	OCULUSXRHMD_API UEnum* Z_Construct_UEnum_OculusXRHMD_EOculusXRControllerType();
	OCULUSXRHMD_API UEnum* Z_Construct_UEnum_OculusXRHMD_EOculusXRDeviceType();
	OCULUSXRHMD_API UEnum* Z_Construct_UEnum_OculusXRHMD_EOculusXRFoveatedRenderingLevel();
	OCULUSXRHMD_API UEnum* Z_Construct_UEnum_OculusXRHMD_EOculusXRFoveatedRenderingMethod();
	OCULUSXRHMD_API UEnum* Z_Construct_UEnum_OculusXRHMD_EOculusXRHandTrackingFrequency();
	OCULUSXRHMD_API UEnum* Z_Construct_UEnum_OculusXRHMD_EOculusXRHandTrackingSupport();
	OCULUSXRHMD_API UEnum* Z_Construct_UEnum_OculusXRHMD_EOculusXRHandTrackingVersion();
	OCULUSXRHMD_API UEnum* Z_Construct_UEnum_OculusXRHMD_EOculusXRProcessorPerformanceLevel();
	OCULUSXRHMD_API UEnum* Z_Construct_UEnum_OculusXRHMD_EOculusXRTrackedDeviceType();
	OCULUSXRHMD_API UEnum* Z_Construct_UEnum_OculusXRHMD_EOculusXRXrApi();
	OCULUSXRHMD_API UScriptStruct* Z_Construct_UScriptStruct_FOculusXRGuardianTestResult();
	OCULUSXRHMD_API UScriptStruct* Z_Construct_UScriptStruct_FOculusXRHmdUserProfile();
	OCULUSXRHMD_API UScriptStruct* Z_Construct_UScriptStruct_FOculusXRHmdUserProfileField();
	UPackage* Z_Construct_UPackage__Script_OculusXRHMD();
// End Cross Module References
	static FEnumRegistrationInfo Z_Registration_Info_UEnum_EOculusXRTrackedDeviceType;
	static UEnum* EOculusXRTrackedDeviceType_StaticEnum()
	{
		if (!Z_Registration_Info_UEnum_EOculusXRTrackedDeviceType.OuterSingleton)
		{
			Z_Registration_Info_UEnum_EOculusXRTrackedDeviceType.OuterSingleton = GetStaticEnum(Z_Construct_UEnum_OculusXRHMD_EOculusXRTrackedDeviceType, Z_Construct_UPackage__Script_OculusXRHMD(), TEXT("EOculusXRTrackedDeviceType"));
		}
		return Z_Registration_Info_UEnum_EOculusXRTrackedDeviceType.OuterSingleton;
	}
	template<> OCULUSXRHMD_API UEnum* StaticEnum<EOculusXRTrackedDeviceType>()
	{
		return EOculusXRTrackedDeviceType_StaticEnum();
	}
	struct Z_Construct_UEnum_OculusXRHMD_EOculusXRTrackedDeviceType_Statics
	{
		static const UECodeGen_Private::FEnumeratorParam Enumerators[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[];
#endif
		static const UECodeGen_Private::FEnumParams EnumParams;
	};
	const UECodeGen_Private::FEnumeratorParam Z_Construct_UEnum_OculusXRHMD_EOculusXRTrackedDeviceType_Statics::Enumerators[] = {
		{ "EOculusXRTrackedDeviceType::None", (int64)EOculusXRTrackedDeviceType::None },
		{ "EOculusXRTrackedDeviceType::HMD", (int64)EOculusXRTrackedDeviceType::HMD },
		{ "EOculusXRTrackedDeviceType::LTouch", (int64)EOculusXRTrackedDeviceType::LTouch },
		{ "EOculusXRTrackedDeviceType::RTouch", (int64)EOculusXRTrackedDeviceType::RTouch },
		{ "EOculusXRTrackedDeviceType::Touch", (int64)EOculusXRTrackedDeviceType::Touch },
		{ "EOculusXRTrackedDeviceType::DeviceObjectZero", (int64)EOculusXRTrackedDeviceType::DeviceObjectZero },
		{ "EOculusXRTrackedDeviceType::All", (int64)EOculusXRTrackedDeviceType::All },
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UEnum_OculusXRHMD_EOculusXRTrackedDeviceType_Statics::Enum_MetaDataParams[] = {
		{ "All.DisplayName", "All Devices" },
		{ "All.Name", "EOculusXRTrackedDeviceType::All" },
		{ "BlueprintType", "true" },
		{ "Comment", "/* Tracked device types corresponding to ovrTrackedDeviceType enum*/" },
		{ "DeviceObjectZero.DisplayName", "DeviceObject Zero" },
		{ "DeviceObjectZero.Name", "EOculusXRTrackedDeviceType::DeviceObjectZero" },
		{ "HMD.DisplayName", "HMD" },
		{ "HMD.Name", "EOculusXRTrackedDeviceType::HMD" },
		{ "LTouch.DisplayName", "Left Hand" },
		{ "LTouch.Name", "EOculusXRTrackedDeviceType::LTouch" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "None.DisplayName", "No Devices" },
		{ "None.Name", "EOculusXRTrackedDeviceType::None" },
		{ "RTouch.DisplayName", "Right Hand" },
		{ "RTouch.Name", "EOculusXRTrackedDeviceType::RTouch" },
		{ "ToolTip", "Tracked device types corresponding to ovrTrackedDeviceType enum" },
		{ "Touch.DisplayName", "All Hands" },
		{ "Touch.Name", "EOculusXRTrackedDeviceType::Touch" },
	};
#endif
	const UECodeGen_Private::FEnumParams Z_Construct_UEnum_OculusXRHMD_EOculusXRTrackedDeviceType_Statics::EnumParams = {
		(UObject*(*)())Z_Construct_UPackage__Script_OculusXRHMD,
		nullptr,
		"EOculusXRTrackedDeviceType",
		"EOculusXRTrackedDeviceType",
		Z_Construct_UEnum_OculusXRHMD_EOculusXRTrackedDeviceType_Statics::Enumerators,
		UE_ARRAY_COUNT(Z_Construct_UEnum_OculusXRHMD_EOculusXRTrackedDeviceType_Statics::Enumerators),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EEnumFlags::None,
		(uint8)UEnum::ECppForm::EnumClass,
		METADATA_PARAMS(Z_Construct_UEnum_OculusXRHMD_EOculusXRTrackedDeviceType_Statics::Enum_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UEnum_OculusXRHMD_EOculusXRTrackedDeviceType_Statics::Enum_MetaDataParams))
	};
	UEnum* Z_Construct_UEnum_OculusXRHMD_EOculusXRTrackedDeviceType()
	{
		if (!Z_Registration_Info_UEnum_EOculusXRTrackedDeviceType.InnerSingleton)
		{
			UECodeGen_Private::ConstructUEnum(Z_Registration_Info_UEnum_EOculusXRTrackedDeviceType.InnerSingleton, Z_Construct_UEnum_OculusXRHMD_EOculusXRTrackedDeviceType_Statics::EnumParams);
		}
		return Z_Registration_Info_UEnum_EOculusXRTrackedDeviceType.InnerSingleton;
	}
	static FStructRegistrationInfo Z_Registration_Info_UScriptStruct_OculusXRHmdUserProfileField;
class UScriptStruct* FOculusXRHmdUserProfileField::StaticStruct()
{
	if (!Z_Registration_Info_UScriptStruct_OculusXRHmdUserProfileField.OuterSingleton)
	{
		Z_Registration_Info_UScriptStruct_OculusXRHmdUserProfileField.OuterSingleton = GetStaticStruct(Z_Construct_UScriptStruct_FOculusXRHmdUserProfileField, Z_Construct_UPackage__Script_OculusXRHMD(), TEXT("OculusXRHmdUserProfileField"));
	}
	return Z_Registration_Info_UScriptStruct_OculusXRHmdUserProfileField.OuterSingleton;
}
template<> OCULUSXRHMD_API UScriptStruct* StaticStruct<FOculusXRHmdUserProfileField>()
{
	return FOculusXRHmdUserProfileField::StaticStruct();
}
	struct Z_Construct_UScriptStruct_FOculusXRHmdUserProfileField_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_FieldName_MetaData[];
#endif
		static const UECodeGen_Private::FStrPropertyParams NewProp_FieldName;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_FieldValue_MetaData[];
#endif
		static const UECodeGen_Private::FStrPropertyParams NewProp_FieldValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UECodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOculusXRHmdUserProfileField_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "DisplayName", "HMD User Profile Data Field" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOculusXRHmdUserProfileField_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOculusXRHmdUserProfileField>();
	}
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOculusXRHmdUserProfileField_Statics::NewProp_FieldName_MetaData[] = {
		{ "Category", "Input|HeadMountedDisplay" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
	};
#endif
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOculusXRHmdUserProfileField_Statics::NewProp_FieldName = { "FieldName", nullptr, (EPropertyFlags)0x0010000000000004, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(FOculusXRHmdUserProfileField, FieldName), METADATA_PARAMS(Z_Construct_UScriptStruct_FOculusXRHmdUserProfileField_Statics::NewProp_FieldName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOculusXRHmdUserProfileField_Statics::NewProp_FieldName_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOculusXRHmdUserProfileField_Statics::NewProp_FieldValue_MetaData[] = {
		{ "Category", "Input|HeadMountedDisplay" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
	};
#endif
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOculusXRHmdUserProfileField_Statics::NewProp_FieldValue = { "FieldValue", nullptr, (EPropertyFlags)0x0010000000000004, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(FOculusXRHmdUserProfileField, FieldValue), METADATA_PARAMS(Z_Construct_UScriptStruct_FOculusXRHmdUserProfileField_Statics::NewProp_FieldValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOculusXRHmdUserProfileField_Statics::NewProp_FieldValue_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOculusXRHmdUserProfileField_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOculusXRHmdUserProfileField_Statics::NewProp_FieldName,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOculusXRHmdUserProfileField_Statics::NewProp_FieldValue,
	};
	const UECodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOculusXRHmdUserProfileField_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OculusXRHMD,
		nullptr,
		&NewStructOps,
		"OculusXRHmdUserProfileField",
		sizeof(FOculusXRHmdUserProfileField),
		alignof(FOculusXRHmdUserProfileField),
		Z_Construct_UScriptStruct_FOculusXRHmdUserProfileField_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOculusXRHmdUserProfileField_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOculusXRHmdUserProfileField_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOculusXRHmdUserProfileField_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOculusXRHmdUserProfileField()
	{
		if (!Z_Registration_Info_UScriptStruct_OculusXRHmdUserProfileField.InnerSingleton)
		{
			UECodeGen_Private::ConstructUScriptStruct(Z_Registration_Info_UScriptStruct_OculusXRHmdUserProfileField.InnerSingleton, Z_Construct_UScriptStruct_FOculusXRHmdUserProfileField_Statics::ReturnStructParams);
		}
		return Z_Registration_Info_UScriptStruct_OculusXRHmdUserProfileField.InnerSingleton;
	}
	static FStructRegistrationInfo Z_Registration_Info_UScriptStruct_OculusXRHmdUserProfile;
class UScriptStruct* FOculusXRHmdUserProfile::StaticStruct()
{
	if (!Z_Registration_Info_UScriptStruct_OculusXRHmdUserProfile.OuterSingleton)
	{
		Z_Registration_Info_UScriptStruct_OculusXRHmdUserProfile.OuterSingleton = GetStaticStruct(Z_Construct_UScriptStruct_FOculusXRHmdUserProfile, Z_Construct_UPackage__Script_OculusXRHMD(), TEXT("OculusXRHmdUserProfile"));
	}
	return Z_Registration_Info_UScriptStruct_OculusXRHmdUserProfile.OuterSingleton;
}
template<> OCULUSXRHMD_API UScriptStruct* StaticStruct<FOculusXRHmdUserProfile>()
{
	return FOculusXRHmdUserProfile::StaticStruct();
}
	struct Z_Construct_UScriptStruct_FOculusXRHmdUserProfile_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UECodeGen_Private::FStrPropertyParams NewProp_Name;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Gender_MetaData[];
#endif
		static const UECodeGen_Private::FStrPropertyParams NewProp_Gender;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_PlayerHeight_MetaData[];
#endif
		static const UECodeGen_Private::FFloatPropertyParams NewProp_PlayerHeight;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_EyeHeight_MetaData[];
#endif
		static const UECodeGen_Private::FFloatPropertyParams NewProp_EyeHeight;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_IPD_MetaData[];
#endif
		static const UECodeGen_Private::FFloatPropertyParams NewProp_IPD;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_NeckToEyeDistance_MetaData[];
#endif
		static const UECodeGen_Private::FStructPropertyParams NewProp_NeckToEyeDistance;
		static const UECodeGen_Private::FStructPropertyParams NewProp_ExtraFields_Inner;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_ExtraFields_MetaData[];
#endif
		static const UECodeGen_Private::FArrayPropertyParams NewProp_ExtraFields;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UECodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOculusXRHmdUserProfile_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "DisplayName", "HMD User Profile Data" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOculusXRHmdUserProfile_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOculusXRHmdUserProfile>();
	}
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOculusXRHmdUserProfile_Statics::NewProp_Name_MetaData[] = {
		{ "Category", "Input|HeadMountedDisplay" },
		{ "Comment", "/** Name of the user's profile. */" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "ToolTip", "Name of the user's profile." },
	};
#endif
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOculusXRHmdUserProfile_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000004, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(FOculusXRHmdUserProfile, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FOculusXRHmdUserProfile_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOculusXRHmdUserProfile_Statics::NewProp_Name_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOculusXRHmdUserProfile_Statics::NewProp_Gender_MetaData[] = {
		{ "Category", "Input|HeadMountedDisplay" },
		{ "Comment", "/** Gender of the user (\"male\", \"female\", etc). */" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "ToolTip", "Gender of the user (\"male\", \"female\", etc)." },
	};
#endif
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOculusXRHmdUserProfile_Statics::NewProp_Gender = { "Gender", nullptr, (EPropertyFlags)0x0010000000000004, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(FOculusXRHmdUserProfile, Gender), METADATA_PARAMS(Z_Construct_UScriptStruct_FOculusXRHmdUserProfile_Statics::NewProp_Gender_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOculusXRHmdUserProfile_Statics::NewProp_Gender_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOculusXRHmdUserProfile_Statics::NewProp_PlayerHeight_MetaData[] = {
		{ "Category", "Input|HeadMountedDisplay" },
		{ "Comment", "/** Height of the player, in meters */" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "ToolTip", "Height of the player, in meters" },
	};
#endif
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FOculusXRHmdUserProfile_Statics::NewProp_PlayerHeight = { "PlayerHeight", nullptr, (EPropertyFlags)0x0010000000000004, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(FOculusXRHmdUserProfile, PlayerHeight), METADATA_PARAMS(Z_Construct_UScriptStruct_FOculusXRHmdUserProfile_Statics::NewProp_PlayerHeight_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOculusXRHmdUserProfile_Statics::NewProp_PlayerHeight_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOculusXRHmdUserProfile_Statics::NewProp_EyeHeight_MetaData[] = {
		{ "Category", "Input|HeadMountedDisplay" },
		{ "Comment", "/** Height of the player, in meters */" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "ToolTip", "Height of the player, in meters" },
	};
#endif
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FOculusXRHmdUserProfile_Statics::NewProp_EyeHeight = { "EyeHeight", nullptr, (EPropertyFlags)0x0010000000000004, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(FOculusXRHmdUserProfile, EyeHeight), METADATA_PARAMS(Z_Construct_UScriptStruct_FOculusXRHmdUserProfile_Statics::NewProp_EyeHeight_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOculusXRHmdUserProfile_Statics::NewProp_EyeHeight_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOculusXRHmdUserProfile_Statics::NewProp_IPD_MetaData[] = {
		{ "Category", "Input|HeadMountedDisplay" },
		{ "Comment", "/** Interpupillary distance of the player, in meters */" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "ToolTip", "Interpupillary distance of the player, in meters" },
	};
#endif
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FOculusXRHmdUserProfile_Statics::NewProp_IPD = { "IPD", nullptr, (EPropertyFlags)0x0010000000000004, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(FOculusXRHmdUserProfile, IPD), METADATA_PARAMS(Z_Construct_UScriptStruct_FOculusXRHmdUserProfile_Statics::NewProp_IPD_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOculusXRHmdUserProfile_Statics::NewProp_IPD_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOculusXRHmdUserProfile_Statics::NewProp_NeckToEyeDistance_MetaData[] = {
		{ "Category", "Input|HeadMountedDisplay" },
		{ "Comment", "/** Neck-to-eye distance, in meters. X - horizontal, Y - vertical. */" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "ToolTip", "Neck-to-eye distance, in meters. X - horizontal, Y - vertical." },
	};
#endif
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOculusXRHmdUserProfile_Statics::NewProp_NeckToEyeDistance = { "NeckToEyeDistance", nullptr, (EPropertyFlags)0x0010000000000004, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(FOculusXRHmdUserProfile, NeckToEyeDistance), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UScriptStruct_FOculusXRHmdUserProfile_Statics::NewProp_NeckToEyeDistance_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOculusXRHmdUserProfile_Statics::NewProp_NeckToEyeDistance_MetaData)) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOculusXRHmdUserProfile_Statics::NewProp_ExtraFields_Inner = { "ExtraFields", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UScriptStruct_FOculusXRHmdUserProfileField, METADATA_PARAMS(nullptr, 0) }; // 3875323592
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOculusXRHmdUserProfile_Statics::NewProp_ExtraFields_MetaData[] = {
		{ "Category", "Input|HeadMountedDisplay" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
	};
#endif
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FOculusXRHmdUserProfile_Statics::NewProp_ExtraFields = { "ExtraFields", nullptr, (EPropertyFlags)0x0010000000000004, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(FOculusXRHmdUserProfile, ExtraFields), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FOculusXRHmdUserProfile_Statics::NewProp_ExtraFields_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOculusXRHmdUserProfile_Statics::NewProp_ExtraFields_MetaData)) }; // 3875323592
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOculusXRHmdUserProfile_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOculusXRHmdUserProfile_Statics::NewProp_Name,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOculusXRHmdUserProfile_Statics::NewProp_Gender,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOculusXRHmdUserProfile_Statics::NewProp_PlayerHeight,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOculusXRHmdUserProfile_Statics::NewProp_EyeHeight,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOculusXRHmdUserProfile_Statics::NewProp_IPD,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOculusXRHmdUserProfile_Statics::NewProp_NeckToEyeDistance,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOculusXRHmdUserProfile_Statics::NewProp_ExtraFields_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOculusXRHmdUserProfile_Statics::NewProp_ExtraFields,
	};
	const UECodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOculusXRHmdUserProfile_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OculusXRHMD,
		nullptr,
		&NewStructOps,
		"OculusXRHmdUserProfile",
		sizeof(FOculusXRHmdUserProfile),
		alignof(FOculusXRHmdUserProfile),
		Z_Construct_UScriptStruct_FOculusXRHmdUserProfile_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOculusXRHmdUserProfile_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOculusXRHmdUserProfile_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOculusXRHmdUserProfile_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOculusXRHmdUserProfile()
	{
		if (!Z_Registration_Info_UScriptStruct_OculusXRHmdUserProfile.InnerSingleton)
		{
			UECodeGen_Private::ConstructUScriptStruct(Z_Registration_Info_UScriptStruct_OculusXRHmdUserProfile.InnerSingleton, Z_Construct_UScriptStruct_FOculusXRHmdUserProfile_Statics::ReturnStructParams);
		}
		return Z_Registration_Info_UScriptStruct_OculusXRHmdUserProfile.InnerSingleton;
	}
	static FEnumRegistrationInfo Z_Registration_Info_UEnum_EOculusXRFoveatedRenderingMethod;
	static UEnum* EOculusXRFoveatedRenderingMethod_StaticEnum()
	{
		if (!Z_Registration_Info_UEnum_EOculusXRFoveatedRenderingMethod.OuterSingleton)
		{
			Z_Registration_Info_UEnum_EOculusXRFoveatedRenderingMethod.OuterSingleton = GetStaticEnum(Z_Construct_UEnum_OculusXRHMD_EOculusXRFoveatedRenderingMethod, Z_Construct_UPackage__Script_OculusXRHMD(), TEXT("EOculusXRFoveatedRenderingMethod"));
		}
		return Z_Registration_Info_UEnum_EOculusXRFoveatedRenderingMethod.OuterSingleton;
	}
	template<> OCULUSXRHMD_API UEnum* StaticEnum<EOculusXRFoveatedRenderingMethod>()
	{
		return EOculusXRFoveatedRenderingMethod_StaticEnum();
	}
	struct Z_Construct_UEnum_OculusXRHMD_EOculusXRFoveatedRenderingMethod_Statics
	{
		static const UECodeGen_Private::FEnumeratorParam Enumerators[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[];
#endif
		static const UECodeGen_Private::FEnumParams EnumParams;
	};
	const UECodeGen_Private::FEnumeratorParam Z_Construct_UEnum_OculusXRHMD_EOculusXRFoveatedRenderingMethod_Statics::Enumerators[] = {
		{ "EOculusXRFoveatedRenderingMethod::FixedFoveatedRendering", (int64)EOculusXRFoveatedRenderingMethod::FixedFoveatedRendering },
		{ "EOculusXRFoveatedRenderingMethod::EyeTrackedFoveatedRendering", (int64)EOculusXRFoveatedRenderingMethod::EyeTrackedFoveatedRendering },
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UEnum_OculusXRHMD_EOculusXRFoveatedRenderingMethod_Statics::Enum_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "EyeTrackedFoveatedRendering.Name", "EOculusXRFoveatedRenderingMethod::EyeTrackedFoveatedRendering" },
		{ "FixedFoveatedRendering.Name", "EOculusXRFoveatedRenderingMethod::FixedFoveatedRendering" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
	};
#endif
	const UECodeGen_Private::FEnumParams Z_Construct_UEnum_OculusXRHMD_EOculusXRFoveatedRenderingMethod_Statics::EnumParams = {
		(UObject*(*)())Z_Construct_UPackage__Script_OculusXRHMD,
		nullptr,
		"EOculusXRFoveatedRenderingMethod",
		"EOculusXRFoveatedRenderingMethod",
		Z_Construct_UEnum_OculusXRHMD_EOculusXRFoveatedRenderingMethod_Statics::Enumerators,
		UE_ARRAY_COUNT(Z_Construct_UEnum_OculusXRHMD_EOculusXRFoveatedRenderingMethod_Statics::Enumerators),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EEnumFlags::None,
		(uint8)UEnum::ECppForm::EnumClass,
		METADATA_PARAMS(Z_Construct_UEnum_OculusXRHMD_EOculusXRFoveatedRenderingMethod_Statics::Enum_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UEnum_OculusXRHMD_EOculusXRFoveatedRenderingMethod_Statics::Enum_MetaDataParams))
	};
	UEnum* Z_Construct_UEnum_OculusXRHMD_EOculusXRFoveatedRenderingMethod()
	{
		if (!Z_Registration_Info_UEnum_EOculusXRFoveatedRenderingMethod.InnerSingleton)
		{
			UECodeGen_Private::ConstructUEnum(Z_Registration_Info_UEnum_EOculusXRFoveatedRenderingMethod.InnerSingleton, Z_Construct_UEnum_OculusXRHMD_EOculusXRFoveatedRenderingMethod_Statics::EnumParams);
		}
		return Z_Registration_Info_UEnum_EOculusXRFoveatedRenderingMethod.InnerSingleton;
	}
	static FEnumRegistrationInfo Z_Registration_Info_UEnum_EOculusXRFoveatedRenderingLevel;
	static UEnum* EOculusXRFoveatedRenderingLevel_StaticEnum()
	{
		if (!Z_Registration_Info_UEnum_EOculusXRFoveatedRenderingLevel.OuterSingleton)
		{
			Z_Registration_Info_UEnum_EOculusXRFoveatedRenderingLevel.OuterSingleton = GetStaticEnum(Z_Construct_UEnum_OculusXRHMD_EOculusXRFoveatedRenderingLevel, Z_Construct_UPackage__Script_OculusXRHMD(), TEXT("EOculusXRFoveatedRenderingLevel"));
		}
		return Z_Registration_Info_UEnum_EOculusXRFoveatedRenderingLevel.OuterSingleton;
	}
	template<> OCULUSXRHMD_API UEnum* StaticEnum<EOculusXRFoveatedRenderingLevel>()
	{
		return EOculusXRFoveatedRenderingLevel_StaticEnum();
	}
	struct Z_Construct_UEnum_OculusXRHMD_EOculusXRFoveatedRenderingLevel_Statics
	{
		static const UECodeGen_Private::FEnumeratorParam Enumerators[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[];
#endif
		static const UECodeGen_Private::FEnumParams EnumParams;
	};
	const UECodeGen_Private::FEnumeratorParam Z_Construct_UEnum_OculusXRHMD_EOculusXRFoveatedRenderingLevel_Statics::Enumerators[] = {
		{ "EOculusXRFoveatedRenderingLevel::Off", (int64)EOculusXRFoveatedRenderingLevel::Off },
		{ "EOculusXRFoveatedRenderingLevel::Low", (int64)EOculusXRFoveatedRenderingLevel::Low },
		{ "EOculusXRFoveatedRenderingLevel::Medium", (int64)EOculusXRFoveatedRenderingLevel::Medium },
		{ "EOculusXRFoveatedRenderingLevel::High", (int64)EOculusXRFoveatedRenderingLevel::High },
		{ "EOculusXRFoveatedRenderingLevel::HighTop", (int64)EOculusXRFoveatedRenderingLevel::HighTop },
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UEnum_OculusXRHMD_EOculusXRFoveatedRenderingLevel_Statics::Enum_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "High.Name", "EOculusXRFoveatedRenderingLevel::High" },
		{ "HighTop.Comment", "// High foveation setting with more detail toward the bottom of the view and more foveation near the top\n" },
		{ "HighTop.Name", "EOculusXRFoveatedRenderingLevel::HighTop" },
		{ "HighTop.ToolTip", "High foveation setting with more detail toward the bottom of the view and more foveation near the top" },
		{ "Low.Name", "EOculusXRFoveatedRenderingLevel::Low" },
		{ "Medium.Name", "EOculusXRFoveatedRenderingLevel::Medium" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "Off.Name", "EOculusXRFoveatedRenderingLevel::Off" },
	};
#endif
	const UECodeGen_Private::FEnumParams Z_Construct_UEnum_OculusXRHMD_EOculusXRFoveatedRenderingLevel_Statics::EnumParams = {
		(UObject*(*)())Z_Construct_UPackage__Script_OculusXRHMD,
		nullptr,
		"EOculusXRFoveatedRenderingLevel",
		"EOculusXRFoveatedRenderingLevel",
		Z_Construct_UEnum_OculusXRHMD_EOculusXRFoveatedRenderingLevel_Statics::Enumerators,
		UE_ARRAY_COUNT(Z_Construct_UEnum_OculusXRHMD_EOculusXRFoveatedRenderingLevel_Statics::Enumerators),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EEnumFlags::None,
		(uint8)UEnum::ECppForm::EnumClass,
		METADATA_PARAMS(Z_Construct_UEnum_OculusXRHMD_EOculusXRFoveatedRenderingLevel_Statics::Enum_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UEnum_OculusXRHMD_EOculusXRFoveatedRenderingLevel_Statics::Enum_MetaDataParams))
	};
	UEnum* Z_Construct_UEnum_OculusXRHMD_EOculusXRFoveatedRenderingLevel()
	{
		if (!Z_Registration_Info_UEnum_EOculusXRFoveatedRenderingLevel.InnerSingleton)
		{
			UECodeGen_Private::ConstructUEnum(Z_Registration_Info_UEnum_EOculusXRFoveatedRenderingLevel.InnerSingleton, Z_Construct_UEnum_OculusXRHMD_EOculusXRFoveatedRenderingLevel_Statics::EnumParams);
		}
		return Z_Registration_Info_UEnum_EOculusXRFoveatedRenderingLevel.InnerSingleton;
	}
	static FEnumRegistrationInfo Z_Registration_Info_UEnum_EOculusXRBoundaryType;
	static UEnum* EOculusXRBoundaryType_StaticEnum()
	{
		if (!Z_Registration_Info_UEnum_EOculusXRBoundaryType.OuterSingleton)
		{
			Z_Registration_Info_UEnum_EOculusXRBoundaryType.OuterSingleton = GetStaticEnum(Z_Construct_UEnum_OculusXRHMD_EOculusXRBoundaryType, Z_Construct_UPackage__Script_OculusXRHMD(), TEXT("EOculusXRBoundaryType"));
		}
		return Z_Registration_Info_UEnum_EOculusXRBoundaryType.OuterSingleton;
	}
	template<> OCULUSXRHMD_API UEnum* StaticEnum<EOculusXRBoundaryType>()
	{
		return EOculusXRBoundaryType_StaticEnum();
	}
	struct Z_Construct_UEnum_OculusXRHMD_EOculusXRBoundaryType_Statics
	{
		static const UECodeGen_Private::FEnumeratorParam Enumerators[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[];
#endif
		static const UECodeGen_Private::FEnumParams EnumParams;
	};
	const UECodeGen_Private::FEnumeratorParam Z_Construct_UEnum_OculusXRHMD_EOculusXRBoundaryType_Statics::Enumerators[] = {
		{ "EOculusXRBoundaryType::Boundary_Outer", (int64)EOculusXRBoundaryType::Boundary_Outer },
		{ "EOculusXRBoundaryType::Boundary_PlayArea", (int64)EOculusXRBoundaryType::Boundary_PlayArea },
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UEnum_OculusXRHMD_EOculusXRBoundaryType_Statics::Enum_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Boundary_Outer.DisplayName", "Outer Boundary" },
		{ "Boundary_Outer.Name", "EOculusXRBoundaryType::Boundary_Outer" },
		{ "Boundary_PlayArea.DisplayName", "Play Area" },
		{ "Boundary_PlayArea.Name", "EOculusXRBoundaryType::Boundary_PlayArea" },
		{ "Comment", "/* Guardian boundary types*/" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "ToolTip", "Guardian boundary types" },
	};
#endif
	const UECodeGen_Private::FEnumParams Z_Construct_UEnum_OculusXRHMD_EOculusXRBoundaryType_Statics::EnumParams = {
		(UObject*(*)())Z_Construct_UPackage__Script_OculusXRHMD,
		nullptr,
		"EOculusXRBoundaryType",
		"EOculusXRBoundaryType",
		Z_Construct_UEnum_OculusXRHMD_EOculusXRBoundaryType_Statics::Enumerators,
		UE_ARRAY_COUNT(Z_Construct_UEnum_OculusXRHMD_EOculusXRBoundaryType_Statics::Enumerators),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EEnumFlags::None,
		(uint8)UEnum::ECppForm::EnumClass,
		METADATA_PARAMS(Z_Construct_UEnum_OculusXRHMD_EOculusXRBoundaryType_Statics::Enum_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UEnum_OculusXRHMD_EOculusXRBoundaryType_Statics::Enum_MetaDataParams))
	};
	UEnum* Z_Construct_UEnum_OculusXRHMD_EOculusXRBoundaryType()
	{
		if (!Z_Registration_Info_UEnum_EOculusXRBoundaryType.InnerSingleton)
		{
			UECodeGen_Private::ConstructUEnum(Z_Registration_Info_UEnum_EOculusXRBoundaryType.InnerSingleton, Z_Construct_UEnum_OculusXRHMD_EOculusXRBoundaryType_Statics::EnumParams);
		}
		return Z_Registration_Info_UEnum_EOculusXRBoundaryType.InnerSingleton;
	}
	static FEnumRegistrationInfo Z_Registration_Info_UEnum_EOculusXRColorSpace;
	static UEnum* EOculusXRColorSpace_StaticEnum()
	{
		if (!Z_Registration_Info_UEnum_EOculusXRColorSpace.OuterSingleton)
		{
			Z_Registration_Info_UEnum_EOculusXRColorSpace.OuterSingleton = GetStaticEnum(Z_Construct_UEnum_OculusXRHMD_EOculusXRColorSpace, Z_Construct_UPackage__Script_OculusXRHMD(), TEXT("EOculusXRColorSpace"));
		}
		return Z_Registration_Info_UEnum_EOculusXRColorSpace.OuterSingleton;
	}
	template<> OCULUSXRHMD_API UEnum* StaticEnum<EOculusXRColorSpace>()
	{
		return EOculusXRColorSpace_StaticEnum();
	}
	struct Z_Construct_UEnum_OculusXRHMD_EOculusXRColorSpace_Statics
	{
		static const UECodeGen_Private::FEnumeratorParam Enumerators[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[];
#endif
		static const UECodeGen_Private::FEnumParams EnumParams;
	};
	const UECodeGen_Private::FEnumeratorParam Z_Construct_UEnum_OculusXRHMD_EOculusXRColorSpace_Statics::Enumerators[] = {
		{ "EOculusXRColorSpace::Unknown", (int64)EOculusXRColorSpace::Unknown },
		{ "EOculusXRColorSpace::Unmanaged", (int64)EOculusXRColorSpace::Unmanaged },
		{ "EOculusXRColorSpace::Rec_2020", (int64)EOculusXRColorSpace::Rec_2020 },
		{ "EOculusXRColorSpace::Rec_709", (int64)EOculusXRColorSpace::Rec_709 },
		{ "EOculusXRColorSpace::Rift_CV1", (int64)EOculusXRColorSpace::Rift_CV1 },
		{ "EOculusXRColorSpace::Rift_S", (int64)EOculusXRColorSpace::Rift_S },
		{ "EOculusXRColorSpace::Quest", (int64)EOculusXRColorSpace::Quest },
		{ "EOculusXRColorSpace::P3", (int64)EOculusXRColorSpace::P3 },
		{ "EOculusXRColorSpace::Adobe_RGB", (int64)EOculusXRColorSpace::Adobe_RGB },
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UEnum_OculusXRHMD_EOculusXRColorSpace_Statics::Enum_MetaDataParams[] = {
		{ "Adobe_RGB.Comment", "/// Similar to sRGB but with deeper greens using D65 white point\n" },
		{ "Adobe_RGB.Name", "EOculusXRColorSpace::Adobe_RGB" },
		{ "Adobe_RGB.ToolTip", "Similar to sRGB but with deeper greens using D65 white point" },
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "P3.Comment", "/// DCI-P3 color space. See documentation for more details\n" },
		{ "P3.DisplayName", "P3 (Recommended)" },
		{ "P3.Name", "EOculusXRColorSpace::P3" },
		{ "P3.ToolTip", "DCI-P3 color space. See documentation for more details" },
		{ "Quest.Comment", "/// Oculus Quest's native color space is slightly different than Rift CV1\n" },
		{ "Quest.DisplayName", "Quest 1" },
		{ "Quest.Name", "EOculusXRColorSpace::Quest" },
		{ "Quest.ToolTip", "Oculus Quest's native color space is slightly different than Rift CV1" },
		{ "Rec_2020.Comment", "/// Color space for standardized color across all Oculus HMDs with D65 white point\n" },
		{ "Rec_2020.Name", "EOculusXRColorSpace::Rec_2020" },
		{ "Rec_2020.ToolTip", "Color space for standardized color across all Oculus HMDs with D65 white point" },
		{ "Rec_709.Comment", "/// Rec. 709 is used on Oculus Go and shares the same primary color coordinates as sRGB\n" },
		{ "Rec_709.Name", "EOculusXRColorSpace::Rec_709" },
		{ "Rec_709.ToolTip", "Rec. 709 is used on Oculus Go and shares the same primary color coordinates as sRGB" },
		{ "Rift_CV1.Comment", "/// Oculus Rift CV1 uses a unique color space, see documentation for more info\n" },
		{ "Rift_CV1.DisplayName", "Rift CV1" },
		{ "Rift_CV1.Name", "EOculusXRColorSpace::Rift_CV1" },
		{ "Rift_CV1.ToolTip", "Oculus Rift CV1 uses a unique color space, see documentation for more info" },
		{ "Rift_S.Comment", "/// Oculus Rift S uses a unique color space, see documentation for more info\n" },
		{ "Rift_S.Name", "EOculusXRColorSpace::Rift_S" },
		{ "Rift_S.ToolTip", "Oculus Rift S uses a unique color space, see documentation for more info" },
		{ "Unknown.Comment", "/// The default value from GetHmdColorSpace until SetClientColorDesc is called. Only valid on PC, and will be remapped to Quest on Mobile\n" },
		{ "Unknown.Name", "EOculusXRColorSpace::Unknown" },
		{ "Unknown.ToolTip", "The default value from GetHmdColorSpace until SetClientColorDesc is called. Only valid on PC, and will be remapped to Quest on Mobile" },
		{ "Unmanaged.Comment", "/// No color correction, not recommended for production use. See documentation for more info\n" },
		{ "Unmanaged.Name", "EOculusXRColorSpace::Unmanaged" },
		{ "Unmanaged.ToolTip", "No color correction, not recommended for production use. See documentation for more info" },
	};
#endif
	const UECodeGen_Private::FEnumParams Z_Construct_UEnum_OculusXRHMD_EOculusXRColorSpace_Statics::EnumParams = {
		(UObject*(*)())Z_Construct_UPackage__Script_OculusXRHMD,
		nullptr,
		"EOculusXRColorSpace",
		"EOculusXRColorSpace",
		Z_Construct_UEnum_OculusXRHMD_EOculusXRColorSpace_Statics::Enumerators,
		UE_ARRAY_COUNT(Z_Construct_UEnum_OculusXRHMD_EOculusXRColorSpace_Statics::Enumerators),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EEnumFlags::None,
		(uint8)UEnum::ECppForm::EnumClass,
		METADATA_PARAMS(Z_Construct_UEnum_OculusXRHMD_EOculusXRColorSpace_Statics::Enum_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UEnum_OculusXRHMD_EOculusXRColorSpace_Statics::Enum_MetaDataParams))
	};
	UEnum* Z_Construct_UEnum_OculusXRHMD_EOculusXRColorSpace()
	{
		if (!Z_Registration_Info_UEnum_EOculusXRColorSpace.InnerSingleton)
		{
			UECodeGen_Private::ConstructUEnum(Z_Registration_Info_UEnum_EOculusXRColorSpace.InnerSingleton, Z_Construct_UEnum_OculusXRHMD_EOculusXRColorSpace_Statics::EnumParams);
		}
		return Z_Registration_Info_UEnum_EOculusXRColorSpace.InnerSingleton;
	}
	static FEnumRegistrationInfo Z_Registration_Info_UEnum_EOculusXRHandTrackingSupport;
	static UEnum* EOculusXRHandTrackingSupport_StaticEnum()
	{
		if (!Z_Registration_Info_UEnum_EOculusXRHandTrackingSupport.OuterSingleton)
		{
			Z_Registration_Info_UEnum_EOculusXRHandTrackingSupport.OuterSingleton = GetStaticEnum(Z_Construct_UEnum_OculusXRHMD_EOculusXRHandTrackingSupport, Z_Construct_UPackage__Script_OculusXRHMD(), TEXT("EOculusXRHandTrackingSupport"));
		}
		return Z_Registration_Info_UEnum_EOculusXRHandTrackingSupport.OuterSingleton;
	}
	template<> OCULUSXRHMD_API UEnum* StaticEnum<EOculusXRHandTrackingSupport>()
	{
		return EOculusXRHandTrackingSupport_StaticEnum();
	}
	struct Z_Construct_UEnum_OculusXRHMD_EOculusXRHandTrackingSupport_Statics
	{
		static const UECodeGen_Private::FEnumeratorParam Enumerators[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[];
#endif
		static const UECodeGen_Private::FEnumParams EnumParams;
	};
	const UECodeGen_Private::FEnumeratorParam Z_Construct_UEnum_OculusXRHMD_EOculusXRHandTrackingSupport_Statics::Enumerators[] = {
		{ "EOculusXRHandTrackingSupport::ControllersOnly", (int64)EOculusXRHandTrackingSupport::ControllersOnly },
		{ "EOculusXRHandTrackingSupport::ControllersAndHands", (int64)EOculusXRHandTrackingSupport::ControllersAndHands },
		{ "EOculusXRHandTrackingSupport::HandsOnly", (int64)EOculusXRHandTrackingSupport::HandsOnly },
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UEnum_OculusXRHMD_EOculusXRHandTrackingSupport_Statics::Enum_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/*\n* Hand tracking settings. Please check https://developer.oculus.com/documentation/unreal/unreal-hand-tracking/\n* for detailed information.\n*/" },
		{ "ControllersAndHands.Name", "EOculusXRHandTrackingSupport::ControllersAndHands" },
		{ "ControllersOnly.Name", "EOculusXRHandTrackingSupport::ControllersOnly" },
		{ "HandsOnly.Name", "EOculusXRHandTrackingSupport::HandsOnly" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "ToolTip", "* Hand tracking settings. Please check https://developer.oculus.com/documentation/unreal/unreal-hand-tracking/\n* for detailed information." },
	};
#endif
	const UECodeGen_Private::FEnumParams Z_Construct_UEnum_OculusXRHMD_EOculusXRHandTrackingSupport_Statics::EnumParams = {
		(UObject*(*)())Z_Construct_UPackage__Script_OculusXRHMD,
		nullptr,
		"EOculusXRHandTrackingSupport",
		"EOculusXRHandTrackingSupport",
		Z_Construct_UEnum_OculusXRHMD_EOculusXRHandTrackingSupport_Statics::Enumerators,
		UE_ARRAY_COUNT(Z_Construct_UEnum_OculusXRHMD_EOculusXRHandTrackingSupport_Statics::Enumerators),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EEnumFlags::None,
		(uint8)UEnum::ECppForm::EnumClass,
		METADATA_PARAMS(Z_Construct_UEnum_OculusXRHMD_EOculusXRHandTrackingSupport_Statics::Enum_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UEnum_OculusXRHMD_EOculusXRHandTrackingSupport_Statics::Enum_MetaDataParams))
	};
	UEnum* Z_Construct_UEnum_OculusXRHMD_EOculusXRHandTrackingSupport()
	{
		if (!Z_Registration_Info_UEnum_EOculusXRHandTrackingSupport.InnerSingleton)
		{
			UECodeGen_Private::ConstructUEnum(Z_Registration_Info_UEnum_EOculusXRHandTrackingSupport.InnerSingleton, Z_Construct_UEnum_OculusXRHMD_EOculusXRHandTrackingSupport_Statics::EnumParams);
		}
		return Z_Registration_Info_UEnum_EOculusXRHandTrackingSupport.InnerSingleton;
	}
	static FEnumRegistrationInfo Z_Registration_Info_UEnum_EOculusXRHandTrackingFrequency;
	static UEnum* EOculusXRHandTrackingFrequency_StaticEnum()
	{
		if (!Z_Registration_Info_UEnum_EOculusXRHandTrackingFrequency.OuterSingleton)
		{
			Z_Registration_Info_UEnum_EOculusXRHandTrackingFrequency.OuterSingleton = GetStaticEnum(Z_Construct_UEnum_OculusXRHMD_EOculusXRHandTrackingFrequency, Z_Construct_UPackage__Script_OculusXRHMD(), TEXT("EOculusXRHandTrackingFrequency"));
		}
		return Z_Registration_Info_UEnum_EOculusXRHandTrackingFrequency.OuterSingleton;
	}
	template<> OCULUSXRHMD_API UEnum* StaticEnum<EOculusXRHandTrackingFrequency>()
	{
		return EOculusXRHandTrackingFrequency_StaticEnum();
	}
	struct Z_Construct_UEnum_OculusXRHMD_EOculusXRHandTrackingFrequency_Statics
	{
		static const UECodeGen_Private::FEnumeratorParam Enumerators[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[];
#endif
		static const UECodeGen_Private::FEnumParams EnumParams;
	};
	const UECodeGen_Private::FEnumeratorParam Z_Construct_UEnum_OculusXRHMD_EOculusXRHandTrackingFrequency_Statics::Enumerators[] = {
		{ "EOculusXRHandTrackingFrequency::LOW", (int64)EOculusXRHandTrackingFrequency::LOW },
		{ "EOculusXRHandTrackingFrequency::HIGH", (int64)EOculusXRHandTrackingFrequency::HIGH },
		{ "EOculusXRHandTrackingFrequency::MAX", (int64)EOculusXRHandTrackingFrequency::MAX },
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UEnum_OculusXRHMD_EOculusXRHandTrackingFrequency_Statics::Enum_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "HIGH.Name", "EOculusXRHandTrackingFrequency::HIGH" },
		{ "LOW.Name", "EOculusXRHandTrackingFrequency::LOW" },
		{ "MAX.Name", "EOculusXRHandTrackingFrequency::MAX" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
	};
#endif
	const UECodeGen_Private::FEnumParams Z_Construct_UEnum_OculusXRHMD_EOculusXRHandTrackingFrequency_Statics::EnumParams = {
		(UObject*(*)())Z_Construct_UPackage__Script_OculusXRHMD,
		nullptr,
		"EOculusXRHandTrackingFrequency",
		"EOculusXRHandTrackingFrequency",
		Z_Construct_UEnum_OculusXRHMD_EOculusXRHandTrackingFrequency_Statics::Enumerators,
		UE_ARRAY_COUNT(Z_Construct_UEnum_OculusXRHMD_EOculusXRHandTrackingFrequency_Statics::Enumerators),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EEnumFlags::None,
		(uint8)UEnum::ECppForm::EnumClass,
		METADATA_PARAMS(Z_Construct_UEnum_OculusXRHMD_EOculusXRHandTrackingFrequency_Statics::Enum_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UEnum_OculusXRHMD_EOculusXRHandTrackingFrequency_Statics::Enum_MetaDataParams))
	};
	UEnum* Z_Construct_UEnum_OculusXRHMD_EOculusXRHandTrackingFrequency()
	{
		if (!Z_Registration_Info_UEnum_EOculusXRHandTrackingFrequency.InnerSingleton)
		{
			UECodeGen_Private::ConstructUEnum(Z_Registration_Info_UEnum_EOculusXRHandTrackingFrequency.InnerSingleton, Z_Construct_UEnum_OculusXRHMD_EOculusXRHandTrackingFrequency_Statics::EnumParams);
		}
		return Z_Registration_Info_UEnum_EOculusXRHandTrackingFrequency.InnerSingleton;
	}
	static FEnumRegistrationInfo Z_Registration_Info_UEnum_EOculusXRHandTrackingVersion;
	static UEnum* EOculusXRHandTrackingVersion_StaticEnum()
	{
		if (!Z_Registration_Info_UEnum_EOculusXRHandTrackingVersion.OuterSingleton)
		{
			Z_Registration_Info_UEnum_EOculusXRHandTrackingVersion.OuterSingleton = GetStaticEnum(Z_Construct_UEnum_OculusXRHMD_EOculusXRHandTrackingVersion, Z_Construct_UPackage__Script_OculusXRHMD(), TEXT("EOculusXRHandTrackingVersion"));
		}
		return Z_Registration_Info_UEnum_EOculusXRHandTrackingVersion.OuterSingleton;
	}
	template<> OCULUSXRHMD_API UEnum* StaticEnum<EOculusXRHandTrackingVersion>()
	{
		return EOculusXRHandTrackingVersion_StaticEnum();
	}
	struct Z_Construct_UEnum_OculusXRHMD_EOculusXRHandTrackingVersion_Statics
	{
		static const UECodeGen_Private::FEnumeratorParam Enumerators[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[];
#endif
		static const UECodeGen_Private::FEnumParams EnumParams;
	};
	const UECodeGen_Private::FEnumeratorParam Z_Construct_UEnum_OculusXRHMD_EOculusXRHandTrackingVersion_Statics::Enumerators[] = {
		{ "EOculusXRHandTrackingVersion::Default", (int64)EOculusXRHandTrackingVersion::Default },
		{ "EOculusXRHandTrackingVersion::V1", (int64)EOculusXRHandTrackingVersion::V1 },
		{ "EOculusXRHandTrackingVersion::V2", (int64)EOculusXRHandTrackingVersion::V2 },
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UEnum_OculusXRHMD_EOculusXRHandTrackingVersion_Statics::Enum_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Default.Name", "EOculusXRHandTrackingVersion::Default" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "V1.Name", "EOculusXRHandTrackingVersion::V1" },
		{ "V2.Name", "EOculusXRHandTrackingVersion::V2" },
	};
#endif
	const UECodeGen_Private::FEnumParams Z_Construct_UEnum_OculusXRHMD_EOculusXRHandTrackingVersion_Statics::EnumParams = {
		(UObject*(*)())Z_Construct_UPackage__Script_OculusXRHMD,
		nullptr,
		"EOculusXRHandTrackingVersion",
		"EOculusXRHandTrackingVersion",
		Z_Construct_UEnum_OculusXRHMD_EOculusXRHandTrackingVersion_Statics::Enumerators,
		UE_ARRAY_COUNT(Z_Construct_UEnum_OculusXRHMD_EOculusXRHandTrackingVersion_Statics::Enumerators),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EEnumFlags::None,
		(uint8)UEnum::ECppForm::EnumClass,
		METADATA_PARAMS(Z_Construct_UEnum_OculusXRHMD_EOculusXRHandTrackingVersion_Statics::Enum_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UEnum_OculusXRHMD_EOculusXRHandTrackingVersion_Statics::Enum_MetaDataParams))
	};
	UEnum* Z_Construct_UEnum_OculusXRHMD_EOculusXRHandTrackingVersion()
	{
		if (!Z_Registration_Info_UEnum_EOculusXRHandTrackingVersion.InnerSingleton)
		{
			UECodeGen_Private::ConstructUEnum(Z_Registration_Info_UEnum_EOculusXRHandTrackingVersion.InnerSingleton, Z_Construct_UEnum_OculusXRHMD_EOculusXRHandTrackingVersion_Statics::EnumParams);
		}
		return Z_Registration_Info_UEnum_EOculusXRHandTrackingVersion.InnerSingleton;
	}
	static FEnumRegistrationInfo Z_Registration_Info_UEnum_EOculusXRProcessorPerformanceLevel;
	static UEnum* EOculusXRProcessorPerformanceLevel_StaticEnum()
	{
		if (!Z_Registration_Info_UEnum_EOculusXRProcessorPerformanceLevel.OuterSingleton)
		{
			Z_Registration_Info_UEnum_EOculusXRProcessorPerformanceLevel.OuterSingleton = GetStaticEnum(Z_Construct_UEnum_OculusXRHMD_EOculusXRProcessorPerformanceLevel, Z_Construct_UPackage__Script_OculusXRHMD(), TEXT("EOculusXRProcessorPerformanceLevel"));
		}
		return Z_Registration_Info_UEnum_EOculusXRProcessorPerformanceLevel.OuterSingleton;
	}
	template<> OCULUSXRHMD_API UEnum* StaticEnum<EOculusXRProcessorPerformanceLevel>()
	{
		return EOculusXRProcessorPerformanceLevel_StaticEnum();
	}
	struct Z_Construct_UEnum_OculusXRHMD_EOculusXRProcessorPerformanceLevel_Statics
	{
		static const UECodeGen_Private::FEnumeratorParam Enumerators[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[];
#endif
		static const UECodeGen_Private::FEnumParams EnumParams;
	};
	const UECodeGen_Private::FEnumeratorParam Z_Construct_UEnum_OculusXRHMD_EOculusXRProcessorPerformanceLevel_Statics::Enumerators[] = {
		{ "EOculusXRProcessorPerformanceLevel::PowerSavings", (int64)EOculusXRProcessorPerformanceLevel::PowerSavings },
		{ "EOculusXRProcessorPerformanceLevel::SustainedLow", (int64)EOculusXRProcessorPerformanceLevel::SustainedLow },
		{ "EOculusXRProcessorPerformanceLevel::SustainedHigh", (int64)EOculusXRProcessorPerformanceLevel::SustainedHigh },
		{ "EOculusXRProcessorPerformanceLevel::Boost", (int64)EOculusXRProcessorPerformanceLevel::Boost },
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UEnum_OculusXRHMD_EOculusXRProcessorPerformanceLevel_Statics::Enum_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Boost.DisplayName", "Boost(*)" },
		{ "Boost.Name", "EOculusXRProcessorPerformanceLevel::Boost" },
		{ "Boost.ToolTip", "Allow XR Runtime to step up beyond the thermally sustainable range for short period. (Currently equivalent to SustainedHigh and not recommended to be used on Quest)" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "PowerSavings.DisplayName", "PowerSavings" },
		{ "PowerSavings.Name", "EOculusXRProcessorPerformanceLevel::PowerSavings" },
		{ "PowerSavings.ToolTip", "Usually used in non-XR section (head-locked / static screen), during which power savings are to be prioritized" },
		{ "SustainedHigh.DisplayName", "SustainedHigh" },
		{ "SustainedHigh.Name", "EOculusXRProcessorPerformanceLevel::SustainedHigh" },
		{ "SustainedHigh.ToolTip", "Let XR Runtime to perform consistent XR compositing and frame rendering within a thermally sustainable range" },
		{ "SustainedLow.DisplayName", "SustainedLow" },
		{ "SustainedLow.Name", "EOculusXRProcessorPerformanceLevel::SustainedLow" },
		{ "SustainedLow.ToolTip", "App enters a low and stable complexity section, during which reducing power is more important than occasional late rendering frames" },
	};
#endif
	const UECodeGen_Private::FEnumParams Z_Construct_UEnum_OculusXRHMD_EOculusXRProcessorPerformanceLevel_Statics::EnumParams = {
		(UObject*(*)())Z_Construct_UPackage__Script_OculusXRHMD,
		nullptr,
		"EOculusXRProcessorPerformanceLevel",
		"EOculusXRProcessorPerformanceLevel",
		Z_Construct_UEnum_OculusXRHMD_EOculusXRProcessorPerformanceLevel_Statics::Enumerators,
		UE_ARRAY_COUNT(Z_Construct_UEnum_OculusXRHMD_EOculusXRProcessorPerformanceLevel_Statics::Enumerators),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EEnumFlags::None,
		(uint8)UEnum::ECppForm::EnumClass,
		METADATA_PARAMS(Z_Construct_UEnum_OculusXRHMD_EOculusXRProcessorPerformanceLevel_Statics::Enum_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UEnum_OculusXRHMD_EOculusXRProcessorPerformanceLevel_Statics::Enum_MetaDataParams))
	};
	UEnum* Z_Construct_UEnum_OculusXRHMD_EOculusXRProcessorPerformanceLevel()
	{
		if (!Z_Registration_Info_UEnum_EOculusXRProcessorPerformanceLevel.InnerSingleton)
		{
			UECodeGen_Private::ConstructUEnum(Z_Registration_Info_UEnum_EOculusXRProcessorPerformanceLevel.InnerSingleton, Z_Construct_UEnum_OculusXRHMD_EOculusXRProcessorPerformanceLevel_Statics::EnumParams);
		}
		return Z_Registration_Info_UEnum_EOculusXRProcessorPerformanceLevel.InnerSingleton;
	}
	static FEnumRegistrationInfo Z_Registration_Info_UEnum_EOculusXRDeviceType;
	static UEnum* EOculusXRDeviceType_StaticEnum()
	{
		if (!Z_Registration_Info_UEnum_EOculusXRDeviceType.OuterSingleton)
		{
			Z_Registration_Info_UEnum_EOculusXRDeviceType.OuterSingleton = GetStaticEnum(Z_Construct_UEnum_OculusXRHMD_EOculusXRDeviceType, Z_Construct_UPackage__Script_OculusXRHMD(), TEXT("EOculusXRDeviceType"));
		}
		return Z_Registration_Info_UEnum_EOculusXRDeviceType.OuterSingleton;
	}
	template<> OCULUSXRHMD_API UEnum* StaticEnum<EOculusXRDeviceType>()
	{
		return EOculusXRDeviceType_StaticEnum();
	}
	struct Z_Construct_UEnum_OculusXRHMD_EOculusXRDeviceType_Statics
	{
		static const UECodeGen_Private::FEnumeratorParam Enumerators[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[];
#endif
		static const UECodeGen_Private::FEnumParams EnumParams;
	};
	const UECodeGen_Private::FEnumeratorParam Z_Construct_UEnum_OculusXRHMD_EOculusXRDeviceType_Statics::Enumerators[] = {
		{ "EOculusXRDeviceType::OculusMobile_Deprecated0", (int64)EOculusXRDeviceType::OculusMobile_Deprecated0 },
		{ "EOculusXRDeviceType::OculusQuest_Deprecated", (int64)EOculusXRDeviceType::OculusQuest_Deprecated },
		{ "EOculusXRDeviceType::OculusQuest2", (int64)EOculusXRDeviceType::OculusQuest2 },
		{ "EOculusXRDeviceType::MetaQuestPro", (int64)EOculusXRDeviceType::MetaQuestPro },
		{ "EOculusXRDeviceType::Rift", (int64)EOculusXRDeviceType::Rift },
		{ "EOculusXRDeviceType::Rift_S", (int64)EOculusXRDeviceType::Rift_S },
		{ "EOculusXRDeviceType::Quest_Link_Deprecated", (int64)EOculusXRDeviceType::Quest_Link_Deprecated },
		{ "EOculusXRDeviceType::Quest2_Link", (int64)EOculusXRDeviceType::Quest2_Link },
		{ "EOculusXRDeviceType::MetaQuestProLink", (int64)EOculusXRDeviceType::MetaQuestProLink },
		{ "EOculusXRDeviceType::OculusUnknown", (int64)EOculusXRDeviceType::OculusUnknown },
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UEnum_OculusXRHMD_EOculusXRDeviceType_Statics::Enum_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "MetaQuestPro.Name", "EOculusXRDeviceType::MetaQuestPro" },
		{ "MetaQuestProLink.Name", "EOculusXRDeviceType::MetaQuestProLink" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "OculusMobile_Deprecated0.Comment", "//mobile HMDs\n" },
		{ "OculusMobile_Deprecated0.Name", "EOculusXRDeviceType::OculusMobile_Deprecated0" },
		{ "OculusMobile_Deprecated0.ToolTip", "mobile HMDs" },
		{ "OculusQuest2.Name", "EOculusXRDeviceType::OculusQuest2" },
		{ "OculusQuest_Deprecated.Name", "EOculusXRDeviceType::OculusQuest_Deprecated" },
		{ "OculusUnknown.Comment", "//default\n" },
		{ "OculusUnknown.Name", "EOculusXRDeviceType::OculusUnknown" },
		{ "OculusUnknown.ToolTip", "default" },
		{ "Quest2_Link.Name", "EOculusXRDeviceType::Quest2_Link" },
		{ "Quest_Link_Deprecated.Name", "EOculusXRDeviceType::Quest_Link_Deprecated" },
		{ "Rift.Comment", "//PC HMDs\n" },
		{ "Rift.Name", "EOculusXRDeviceType::Rift" },
		{ "Rift.ToolTip", "PC HMDs" },
		{ "Rift_S.Name", "EOculusXRDeviceType::Rift_S" },
	};
#endif
	const UECodeGen_Private::FEnumParams Z_Construct_UEnum_OculusXRHMD_EOculusXRDeviceType_Statics::EnumParams = {
		(UObject*(*)())Z_Construct_UPackage__Script_OculusXRHMD,
		nullptr,
		"EOculusXRDeviceType",
		"EOculusXRDeviceType",
		Z_Construct_UEnum_OculusXRHMD_EOculusXRDeviceType_Statics::Enumerators,
		UE_ARRAY_COUNT(Z_Construct_UEnum_OculusXRHMD_EOculusXRDeviceType_Statics::Enumerators),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EEnumFlags::None,
		(uint8)UEnum::ECppForm::EnumClass,
		METADATA_PARAMS(Z_Construct_UEnum_OculusXRHMD_EOculusXRDeviceType_Statics::Enum_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UEnum_OculusXRHMD_EOculusXRDeviceType_Statics::Enum_MetaDataParams))
	};
	UEnum* Z_Construct_UEnum_OculusXRHMD_EOculusXRDeviceType()
	{
		if (!Z_Registration_Info_UEnum_EOculusXRDeviceType.InnerSingleton)
		{
			UECodeGen_Private::ConstructUEnum(Z_Registration_Info_UEnum_EOculusXRDeviceType.InnerSingleton, Z_Construct_UEnum_OculusXRHMD_EOculusXRDeviceType_Statics::EnumParams);
		}
		return Z_Registration_Info_UEnum_EOculusXRDeviceType.InnerSingleton;
	}
	static FEnumRegistrationInfo Z_Registration_Info_UEnum_EOculusXRControllerType;
	static UEnum* EOculusXRControllerType_StaticEnum()
	{
		if (!Z_Registration_Info_UEnum_EOculusXRControllerType.OuterSingleton)
		{
			Z_Registration_Info_UEnum_EOculusXRControllerType.OuterSingleton = GetStaticEnum(Z_Construct_UEnum_OculusXRHMD_EOculusXRControllerType, Z_Construct_UPackage__Script_OculusXRHMD(), TEXT("EOculusXRControllerType"));
		}
		return Z_Registration_Info_UEnum_EOculusXRControllerType.OuterSingleton;
	}
	template<> OCULUSXRHMD_API UEnum* StaticEnum<EOculusXRControllerType>()
	{
		return EOculusXRControllerType_StaticEnum();
	}
	struct Z_Construct_UEnum_OculusXRHMD_EOculusXRControllerType_Statics
	{
		static const UECodeGen_Private::FEnumeratorParam Enumerators[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[];
#endif
		static const UECodeGen_Private::FEnumParams EnumParams;
	};
	const UECodeGen_Private::FEnumeratorParam Z_Construct_UEnum_OculusXRHMD_EOculusXRControllerType_Statics::Enumerators[] = {
		{ "EOculusXRControllerType::None", (int64)EOculusXRControllerType::None },
		{ "EOculusXRControllerType::MetaQuestTouch", (int64)EOculusXRControllerType::MetaQuestTouch },
		{ "EOculusXRControllerType::MetaQuestTouchPro", (int64)EOculusXRControllerType::MetaQuestTouchPro },
		{ "EOculusXRControllerType::Unknown", (int64)EOculusXRControllerType::Unknown },
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UEnum_OculusXRHMD_EOculusXRControllerType_Statics::Enum_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "MetaQuestTouch.Name", "EOculusXRControllerType::MetaQuestTouch" },
		{ "MetaQuestTouchPro.Name", "EOculusXRControllerType::MetaQuestTouchPro" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "None.Name", "EOculusXRControllerType::None" },
		{ "Unknown.Name", "EOculusXRControllerType::Unknown" },
	};
#endif
	const UECodeGen_Private::FEnumParams Z_Construct_UEnum_OculusXRHMD_EOculusXRControllerType_Statics::EnumParams = {
		(UObject*(*)())Z_Construct_UPackage__Script_OculusXRHMD,
		nullptr,
		"EOculusXRControllerType",
		"EOculusXRControllerType",
		Z_Construct_UEnum_OculusXRHMD_EOculusXRControllerType_Statics::Enumerators,
		UE_ARRAY_COUNT(Z_Construct_UEnum_OculusXRHMD_EOculusXRControllerType_Statics::Enumerators),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EEnumFlags::None,
		(uint8)UEnum::ECppForm::EnumClass,
		METADATA_PARAMS(Z_Construct_UEnum_OculusXRHMD_EOculusXRControllerType_Statics::Enum_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UEnum_OculusXRHMD_EOculusXRControllerType_Statics::Enum_MetaDataParams))
	};
	UEnum* Z_Construct_UEnum_OculusXRHMD_EOculusXRControllerType()
	{
		if (!Z_Registration_Info_UEnum_EOculusXRControllerType.InnerSingleton)
		{
			UECodeGen_Private::ConstructUEnum(Z_Registration_Info_UEnum_EOculusXRControllerType.InnerSingleton, Z_Construct_UEnum_OculusXRHMD_EOculusXRControllerType_Statics::EnumParams);
		}
		return Z_Registration_Info_UEnum_EOculusXRControllerType.InnerSingleton;
	}
	static FEnumRegistrationInfo Z_Registration_Info_UEnum_EOculusXRXrApi;
	static UEnum* EOculusXRXrApi_StaticEnum()
	{
		if (!Z_Registration_Info_UEnum_EOculusXRXrApi.OuterSingleton)
		{
			Z_Registration_Info_UEnum_EOculusXRXrApi.OuterSingleton = GetStaticEnum(Z_Construct_UEnum_OculusXRHMD_EOculusXRXrApi, Z_Construct_UPackage__Script_OculusXRHMD(), TEXT("EOculusXRXrApi"));
		}
		return Z_Registration_Info_UEnum_EOculusXRXrApi.OuterSingleton;
	}
	template<> OCULUSXRHMD_API UEnum* StaticEnum<EOculusXRXrApi>()
	{
		return EOculusXRXrApi_StaticEnum();
	}
	struct Z_Construct_UEnum_OculusXRHMD_EOculusXRXrApi_Statics
	{
		static const UECodeGen_Private::FEnumeratorParam Enumerators[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[];
#endif
		static const UECodeGen_Private::FEnumParams EnumParams;
	};
	const UECodeGen_Private::FEnumeratorParam Z_Construct_UEnum_OculusXRHMD_EOculusXRXrApi_Statics::Enumerators[] = {
		{ "EOculusXRXrApi::OVRPluginOpenXR", (int64)EOculusXRXrApi::OVRPluginOpenXR },
		{ "EOculusXRXrApi::NativeOpenXR", (int64)EOculusXRXrApi::NativeOpenXR },
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UEnum_OculusXRHMD_EOculusXRXrApi_Statics::Enum_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "NativeOpenXR.DisplayName", "Epic Native OpenXR with Oculus vendor extensions" },
		{ "NativeOpenXR.Name", "EOculusXRXrApi::NativeOpenXR" },
		{ "NativeOpenXR.ToolTip", "Disable Legacy Oculus in favor of the native OpenXR implementation, with Oculus vendor extensions. Must enable the OpenXR plugin. This will be where Epic focuses XR development going forward. Oculus OpenXR extensions may be moved into a separate plugin (or plugins) in the future to improve modularity. The features supported by OpenXR are listed in the OpenXR specification on khronos.org, and the features supported by a given runtime can be verified with the \\\"OpenXR Explorer\\\" application on GitHub." },
		{ "OVRPluginOpenXR.DisplayName", "Oculus OVRPlugin + OpenXR backend (current recommended)" },
		{ "OVRPluginOpenXR.Name", "EOculusXRXrApi::OVRPluginOpenXR" },
		{ "OVRPluginOpenXR.ToolTip", "Oculus plugin integration using OpenXR backend on both Mobile and PC. All new features will ship on backend for the forseeable future." },
	};
#endif
	const UECodeGen_Private::FEnumParams Z_Construct_UEnum_OculusXRHMD_EOculusXRXrApi_Statics::EnumParams = {
		(UObject*(*)())Z_Construct_UPackage__Script_OculusXRHMD,
		nullptr,
		"EOculusXRXrApi",
		"EOculusXRXrApi",
		Z_Construct_UEnum_OculusXRHMD_EOculusXRXrApi_Statics::Enumerators,
		UE_ARRAY_COUNT(Z_Construct_UEnum_OculusXRHMD_EOculusXRXrApi_Statics::Enumerators),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EEnumFlags::None,
		(uint8)UEnum::ECppForm::EnumClass,
		METADATA_PARAMS(Z_Construct_UEnum_OculusXRHMD_EOculusXRXrApi_Statics::Enum_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UEnum_OculusXRHMD_EOculusXRXrApi_Statics::Enum_MetaDataParams))
	};
	UEnum* Z_Construct_UEnum_OculusXRHMD_EOculusXRXrApi()
	{
		if (!Z_Registration_Info_UEnum_EOculusXRXrApi.InnerSingleton)
		{
			UECodeGen_Private::ConstructUEnum(Z_Registration_Info_UEnum_EOculusXRXrApi.InnerSingleton, Z_Construct_UEnum_OculusXRHMD_EOculusXRXrApi_Statics::EnumParams);
		}
		return Z_Registration_Info_UEnum_EOculusXRXrApi.InnerSingleton;
	}
	static FStructRegistrationInfo Z_Registration_Info_UScriptStruct_OculusXRGuardianTestResult;
class UScriptStruct* FOculusXRGuardianTestResult::StaticStruct()
{
	if (!Z_Registration_Info_UScriptStruct_OculusXRGuardianTestResult.OuterSingleton)
	{
		Z_Registration_Info_UScriptStruct_OculusXRGuardianTestResult.OuterSingleton = GetStaticStruct(Z_Construct_UScriptStruct_FOculusXRGuardianTestResult, Z_Construct_UPackage__Script_OculusXRHMD(), TEXT("OculusXRGuardianTestResult"));
	}
	return Z_Registration_Info_UScriptStruct_OculusXRGuardianTestResult.OuterSingleton;
}
template<> OCULUSXRHMD_API UScriptStruct* StaticStruct<FOculusXRGuardianTestResult>()
{
	return FOculusXRGuardianTestResult::StaticStruct();
}
	struct Z_Construct_UScriptStruct_FOculusXRGuardianTestResult_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_IsTriggering_MetaData[];
#endif
		static void NewProp_IsTriggering_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_IsTriggering;
		static const UECodeGen_Private::FBytePropertyParams NewProp_DeviceType_Underlying;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_DeviceType_MetaData[];
#endif
		static const UECodeGen_Private::FEnumPropertyParams NewProp_DeviceType;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_ClosestDistance_MetaData[];
#endif
		static const UECodeGen_Private::FFloatPropertyParams NewProp_ClosestDistance;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_ClosestPoint_MetaData[];
#endif
		static const UECodeGen_Private::FStructPropertyParams NewProp_ClosestPoint;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_ClosestPointNormal_MetaData[];
#endif
		static const UECodeGen_Private::FStructPropertyParams NewProp_ClosestPointNormal;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UECodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOculusXRGuardianTestResult_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/*\n* Information about relationships between a triggered boundary (EOculusXRBoundaryType::Boundary_Outer or\n* EOculusXRBoundaryType::Boundary_PlayArea) and a device or point in the world.\n* All dimensions, points, and vectors are returned in Unreal world coordinate space.\n*/" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "ToolTip", "* Information about relationships between a triggered boundary (EOculusXRBoundaryType::Boundary_Outer or\n* EOculusXRBoundaryType::Boundary_PlayArea) and a device or point in the world.\n* All dimensions, points, and vectors are returned in Unreal world coordinate space." },
	};
#endif
	void* Z_Construct_UScriptStruct_FOculusXRGuardianTestResult_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOculusXRGuardianTestResult>();
	}
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOculusXRGuardianTestResult_Statics::NewProp_IsTriggering_MetaData[] = {
		{ "Category", "Boundary Test Result" },
		{ "Comment", "/** Is there a triggering interaction between the device/point and specified boundary? */" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "ToolTip", "Is there a triggering interaction between the device/point and specified boundary?" },
	};
#endif
	void Z_Construct_UScriptStruct_FOculusXRGuardianTestResult_Statics::NewProp_IsTriggering_SetBit(void* Obj)
	{
		((FOculusXRGuardianTestResult*)Obj)->IsTriggering = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FOculusXRGuardianTestResult_Statics::NewProp_IsTriggering = { "IsTriggering", nullptr, (EPropertyFlags)0x0010000000000015, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(FOculusXRGuardianTestResult), &Z_Construct_UScriptStruct_FOculusXRGuardianTestResult_Statics::NewProp_IsTriggering_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FOculusXRGuardianTestResult_Statics::NewProp_IsTriggering_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOculusXRGuardianTestResult_Statics::NewProp_IsTriggering_MetaData)) };
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FOculusXRGuardianTestResult_Statics::NewProp_DeviceType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOculusXRGuardianTestResult_Statics::NewProp_DeviceType_MetaData[] = {
		{ "Category", "Boundary Test Result" },
		{ "Comment", "/** Device type triggering boundary (EOculusXRTrackedDeviceType::None if BoundaryTestResult corresponds to a point rather than a device) */" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "ToolTip", "Device type triggering boundary (EOculusXRTrackedDeviceType::None if BoundaryTestResult corresponds to a point rather than a device)" },
	};
#endif
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FOculusXRGuardianTestResult_Statics::NewProp_DeviceType = { "DeviceType", nullptr, (EPropertyFlags)0x0010000000000015, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(FOculusXRGuardianTestResult, DeviceType), Z_Construct_UEnum_OculusXRHMD_EOculusXRTrackedDeviceType, METADATA_PARAMS(Z_Construct_UScriptStruct_FOculusXRGuardianTestResult_Statics::NewProp_DeviceType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOculusXRGuardianTestResult_Statics::NewProp_DeviceType_MetaData)) }; // 125253818
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOculusXRGuardianTestResult_Statics::NewProp_ClosestDistance_MetaData[] = {
		{ "Category", "Boundary Test Result" },
		{ "Comment", "/** Distance of device/point to surface of boundary specified by BoundaryType */" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "ToolTip", "Distance of device/point to surface of boundary specified by BoundaryType" },
	};
#endif
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FOculusXRGuardianTestResult_Statics::NewProp_ClosestDistance = { "ClosestDistance", nullptr, (EPropertyFlags)0x0010000000000015, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(FOculusXRGuardianTestResult, ClosestDistance), METADATA_PARAMS(Z_Construct_UScriptStruct_FOculusXRGuardianTestResult_Statics::NewProp_ClosestDistance_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOculusXRGuardianTestResult_Statics::NewProp_ClosestDistance_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOculusXRGuardianTestResult_Statics::NewProp_ClosestPoint_MetaData[] = {
		{ "Category", "Boundary Test Result" },
		{ "Comment", "/** Closest point on surface corresponding to specified boundary */" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "ToolTip", "Closest point on surface corresponding to specified boundary" },
	};
#endif
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOculusXRGuardianTestResult_Statics::NewProp_ClosestPoint = { "ClosestPoint", nullptr, (EPropertyFlags)0x0010000000000015, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(FOculusXRGuardianTestResult, ClosestPoint), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FOculusXRGuardianTestResult_Statics::NewProp_ClosestPoint_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOculusXRGuardianTestResult_Statics::NewProp_ClosestPoint_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOculusXRGuardianTestResult_Statics::NewProp_ClosestPointNormal_MetaData[] = {
		{ "Category", "Boundary Test Result" },
		{ "Comment", "/** Normal of closest point */" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "ToolTip", "Normal of closest point" },
	};
#endif
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOculusXRGuardianTestResult_Statics::NewProp_ClosestPointNormal = { "ClosestPointNormal", nullptr, (EPropertyFlags)0x0010000000000015, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(FOculusXRGuardianTestResult, ClosestPointNormal), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FOculusXRGuardianTestResult_Statics::NewProp_ClosestPointNormal_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOculusXRGuardianTestResult_Statics::NewProp_ClosestPointNormal_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOculusXRGuardianTestResult_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOculusXRGuardianTestResult_Statics::NewProp_IsTriggering,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOculusXRGuardianTestResult_Statics::NewProp_DeviceType_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOculusXRGuardianTestResult_Statics::NewProp_DeviceType,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOculusXRGuardianTestResult_Statics::NewProp_ClosestDistance,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOculusXRGuardianTestResult_Statics::NewProp_ClosestPoint,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOculusXRGuardianTestResult_Statics::NewProp_ClosestPointNormal,
	};
	const UECodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOculusXRGuardianTestResult_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OculusXRHMD,
		nullptr,
		&NewStructOps,
		"OculusXRGuardianTestResult",
		sizeof(FOculusXRGuardianTestResult),
		alignof(FOculusXRGuardianTestResult),
		Z_Construct_UScriptStruct_FOculusXRGuardianTestResult_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOculusXRGuardianTestResult_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOculusXRGuardianTestResult_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOculusXRGuardianTestResult_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOculusXRGuardianTestResult()
	{
		if (!Z_Registration_Info_UScriptStruct_OculusXRGuardianTestResult.InnerSingleton)
		{
			UECodeGen_Private::ConstructUScriptStruct(Z_Registration_Info_UScriptStruct_OculusXRGuardianTestResult.InnerSingleton, Z_Construct_UScriptStruct_FOculusXRGuardianTestResult_Statics::ReturnStructParams);
		}
		return Z_Registration_Info_UScriptStruct_OculusXRGuardianTestResult.InnerSingleton;
	}
	static FEnumRegistrationInfo Z_Registration_Info_UEnum_EOculusXRControllerPoseAlignment;
	static UEnum* EOculusXRControllerPoseAlignment_StaticEnum()
	{
		if (!Z_Registration_Info_UEnum_EOculusXRControllerPoseAlignment.OuterSingleton)
		{
			Z_Registration_Info_UEnum_EOculusXRControllerPoseAlignment.OuterSingleton = GetStaticEnum(Z_Construct_UEnum_OculusXRHMD_EOculusXRControllerPoseAlignment, Z_Construct_UPackage__Script_OculusXRHMD(), TEXT("EOculusXRControllerPoseAlignment"));
		}
		return Z_Registration_Info_UEnum_EOculusXRControllerPoseAlignment.OuterSingleton;
	}
	template<> OCULUSXRHMD_API UEnum* StaticEnum<EOculusXRControllerPoseAlignment>()
	{
		return EOculusXRControllerPoseAlignment_StaticEnum();
	}
	struct Z_Construct_UEnum_OculusXRHMD_EOculusXRControllerPoseAlignment_Statics
	{
		static const UECodeGen_Private::FEnumeratorParam Enumerators[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[];
#endif
		static const UECodeGen_Private::FEnumParams EnumParams;
	};
	const UECodeGen_Private::FEnumeratorParam Z_Construct_UEnum_OculusXRHMD_EOculusXRControllerPoseAlignment_Statics::Enumerators[] = {
		{ "EOculusXRControllerPoseAlignment::Default", (int64)EOculusXRControllerPoseAlignment::Default },
		{ "EOculusXRControllerPoseAlignment::Grip", (int64)EOculusXRControllerPoseAlignment::Grip },
		{ "EOculusXRControllerPoseAlignment::Aim", (int64)EOculusXRControllerPoseAlignment::Aim },
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UEnum_OculusXRHMD_EOculusXRControllerPoseAlignment_Statics::Enum_MetaDataParams[] = {
		{ "Aim.Name", "EOculusXRControllerPoseAlignment::Aim" },
		{ "Aim.ToolTip", "Aim pose alignment as defined by OpenXR. Use this for cross-plugin compatibility with assets designed for the native OpenXR aim pose." },
		{ "Default.Name", "EOculusXRControllerPoseAlignment::Default" },
		{ "Default.ToolTip", "Default pose alignment used in all versions of the Meta XR plugin. Recommended pose for compatibility with previous assets designed for the Meta XR plugin." },
		{ "Grip.Name", "EOculusXRControllerPoseAlignment::Grip" },
		{ "Grip.ToolTip", "Grip pose alignment as defined by OpenXR. Use this for cross-plugin compatibility with assets designed for the native OpenXR grip pose." },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
	};
#endif
	const UECodeGen_Private::FEnumParams Z_Construct_UEnum_OculusXRHMD_EOculusXRControllerPoseAlignment_Statics::EnumParams = {
		(UObject*(*)())Z_Construct_UPackage__Script_OculusXRHMD,
		nullptr,
		"EOculusXRControllerPoseAlignment",
		"EOculusXRControllerPoseAlignment",
		Z_Construct_UEnum_OculusXRHMD_EOculusXRControllerPoseAlignment_Statics::Enumerators,
		UE_ARRAY_COUNT(Z_Construct_UEnum_OculusXRHMD_EOculusXRControllerPoseAlignment_Statics::Enumerators),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EEnumFlags::None,
		(uint8)UEnum::ECppForm::EnumClass,
		METADATA_PARAMS(Z_Construct_UEnum_OculusXRHMD_EOculusXRControllerPoseAlignment_Statics::Enum_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UEnum_OculusXRHMD_EOculusXRControllerPoseAlignment_Statics::Enum_MetaDataParams))
	};
	UEnum* Z_Construct_UEnum_OculusXRHMD_EOculusXRControllerPoseAlignment()
	{
		if (!Z_Registration_Info_UEnum_EOculusXRControllerPoseAlignment.InnerSingleton)
		{
			UECodeGen_Private::ConstructUEnum(Z_Registration_Info_UEnum_EOculusXRControllerPoseAlignment.InnerSingleton, Z_Construct_UEnum_OculusXRHMD_EOculusXRControllerPoseAlignment_Statics::EnumParams);
		}
		return Z_Registration_Info_UEnum_EOculusXRControllerPoseAlignment.InnerSingleton;
	}
	DEFINE_FUNCTION(UOculusXRFunctionLibrary::execSetGuardianVisibility)
	{
		P_GET_UBOOL(Z_Param_GuardianVisible);
		P_FINISH;
		P_NATIVE_BEGIN;
		UOculusXRFunctionLibrary::SetGuardianVisibility(Z_Param_GuardianVisible);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UOculusXRFunctionLibrary::execGetNodeGuardianIntersection)
	{
		P_GET_ENUM(EOculusXRTrackedDeviceType,Z_Param_DeviceType);
		P_GET_ENUM(EOculusXRBoundaryType,Z_Param_BoundaryType);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FOculusXRGuardianTestResult*)Z_Param__Result=UOculusXRFunctionLibrary::GetNodeGuardianIntersection(EOculusXRTrackedDeviceType(Z_Param_DeviceType),EOculusXRBoundaryType(Z_Param_BoundaryType));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UOculusXRFunctionLibrary::execGetPointGuardianIntersection)
	{
		P_GET_STRUCT(FVector,Z_Param_Point);
		P_GET_ENUM(EOculusXRBoundaryType,Z_Param_BoundaryType);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FOculusXRGuardianTestResult*)Z_Param__Result=UOculusXRFunctionLibrary::GetPointGuardianIntersection(Z_Param_Point,EOculusXRBoundaryType(Z_Param_BoundaryType));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UOculusXRFunctionLibrary::execGetPlayAreaTransform)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FTransform*)Z_Param__Result=UOculusXRFunctionLibrary::GetPlayAreaTransform();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UOculusXRFunctionLibrary::execGetGuardianDimensions)
	{
		P_GET_ENUM(EOculusXRBoundaryType,Z_Param_BoundaryType);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FVector*)Z_Param__Result=UOculusXRFunctionLibrary::GetGuardianDimensions(EOculusXRBoundaryType(Z_Param_BoundaryType));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UOculusXRFunctionLibrary::execGetGuardianPoints)
	{
		P_GET_ENUM(EOculusXRBoundaryType,Z_Param_BoundaryType);
		P_GET_UBOOL(Z_Param_UsePawnSpace);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<FVector>*)Z_Param__Result=UOculusXRFunctionLibrary::GetGuardianPoints(EOculusXRBoundaryType(Z_Param_BoundaryType),Z_Param_UsePawnSpace);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UOculusXRFunctionLibrary::execIsGuardianConfigured)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UOculusXRFunctionLibrary::IsGuardianConfigured();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UOculusXRFunctionLibrary::execIsGuardianDisplayed)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UOculusXRFunctionLibrary::IsGuardianDisplayed();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UOculusXRFunctionLibrary::execIsColorPassthroughSupported)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UOculusXRFunctionLibrary::IsColorPassthroughSupported();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UOculusXRFunctionLibrary::execIsPassthroughSupported)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UOculusXRFunctionLibrary::IsPassthroughSupported();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UOculusXRFunctionLibrary::execSetLocalDimmingOn)
	{
		P_GET_UBOOL(Z_Param_LocalDimmingOn);
		P_FINISH;
		P_NATIVE_BEGIN;
		UOculusXRFunctionLibrary::SetLocalDimmingOn(Z_Param_LocalDimmingOn);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UOculusXRFunctionLibrary::execSetClientColorDesc)
	{
		P_GET_ENUM(EOculusXRColorSpace,Z_Param_ColorSpace);
		P_FINISH;
		P_NATIVE_BEGIN;
		UOculusXRFunctionLibrary::SetClientColorDesc(EOculusXRColorSpace(Z_Param_ColorSpace));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UOculusXRFunctionLibrary::execGetHmdColorDesc)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(EOculusXRColorSpace*)Z_Param__Result=UOculusXRFunctionLibrary::GetHmdColorDesc();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UOculusXRFunctionLibrary::execGetSystemHmd3DofModeEnabled)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UOculusXRFunctionLibrary::GetSystemHmd3DofModeEnabled();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UOculusXRFunctionLibrary::execSetColorScaleAndOffset)
	{
		P_GET_STRUCT(FLinearColor,Z_Param_ColorScale);
		P_GET_STRUCT(FLinearColor,Z_Param_ColorOffset);
		P_GET_UBOOL(Z_Param_bApplyToAllLayers);
		P_FINISH;
		P_NATIVE_BEGIN;
		UOculusXRFunctionLibrary::SetColorScaleAndOffset(Z_Param_ColorScale,Z_Param_ColorOffset,Z_Param_bApplyToAllLayers);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UOculusXRFunctionLibrary::execEnableOrientationTracking)
	{
		P_GET_UBOOL(Z_Param_bOrientationTracking);
		P_FINISH;
		P_NATIVE_BEGIN;
		UOculusXRFunctionLibrary::EnableOrientationTracking(Z_Param_bOrientationTracking);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UOculusXRFunctionLibrary::execEnablePositionTracking)
	{
		P_GET_UBOOL(Z_Param_bPositionTracking);
		P_FINISH;
		P_NATIVE_BEGIN;
		UOculusXRFunctionLibrary::EnablePositionTracking(Z_Param_bPositionTracking);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UOculusXRFunctionLibrary::execSetDisplayFrequency)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_RequestedFrequency);
		P_FINISH;
		P_NATIVE_BEGIN;
		UOculusXRFunctionLibrary::SetDisplayFrequency(Z_Param_RequestedFrequency);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UOculusXRFunctionLibrary::execGetCurrentDisplayFrequency)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(float*)Z_Param__Result=UOculusXRFunctionLibrary::GetCurrentDisplayFrequency();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UOculusXRFunctionLibrary::execGetAvailableDisplayFrequencies)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<float>*)Z_Param__Result=UOculusXRFunctionLibrary::GetAvailableDisplayFrequencies();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UOculusXRFunctionLibrary::execGetControllerType)
	{
		P_GET_ENUM(EControllerHand,Z_Param_deviceHand);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(EOculusXRControllerType*)Z_Param__Result=UOculusXRFunctionLibrary::GetControllerType(EControllerHand(Z_Param_deviceHand));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UOculusXRFunctionLibrary::execGetDeviceType)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(EOculusXRDeviceType*)Z_Param__Result=UOculusXRFunctionLibrary::GetDeviceType();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UOculusXRFunctionLibrary::execGetDeviceName)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=UOculusXRFunctionLibrary::GetDeviceName();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UOculusXRFunctionLibrary::execGetEyeTrackedFoveatedRenderingSupported)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UOculusXRFunctionLibrary::GetEyeTrackedFoveatedRenderingSupported();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UOculusXRFunctionLibrary::execSetFoveatedRenderingLevel)
	{
		P_GET_ENUM(EOculusXRFoveatedRenderingLevel,Z_Param_level);
		P_GET_UBOOL(Z_Param_isDynamic);
		P_FINISH;
		P_NATIVE_BEGIN;
		UOculusXRFunctionLibrary::SetFoveatedRenderingLevel(EOculusXRFoveatedRenderingLevel(Z_Param_level),Z_Param_isDynamic);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UOculusXRFunctionLibrary::execGetFoveatedRenderingLevel)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(EOculusXRFoveatedRenderingLevel*)Z_Param__Result=UOculusXRFunctionLibrary::GetFoveatedRenderingLevel();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UOculusXRFunctionLibrary::execSetFoveatedRenderingMethod)
	{
		P_GET_ENUM(EOculusXRFoveatedRenderingMethod,Z_Param_Method);
		P_FINISH;
		P_NATIVE_BEGIN;
		UOculusXRFunctionLibrary::SetFoveatedRenderingMethod(EOculusXRFoveatedRenderingMethod(Z_Param_Method));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UOculusXRFunctionLibrary::execGetFoveatedRenderingMethod)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(EOculusXRFoveatedRenderingMethod*)Z_Param__Result=UOculusXRFunctionLibrary::GetFoveatedRenderingMethod();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UOculusXRFunctionLibrary::execGetGPUFrameTime)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(float*)Z_Param__Result=UOculusXRFunctionLibrary::GetGPUFrameTime();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UOculusXRFunctionLibrary::execGetGPUUtilization)
	{
		P_GET_UBOOL_REF(Z_Param_Out_IsGPUAvailable);
		P_GET_PROPERTY_REF(FFloatProperty,Z_Param_Out_GPUUtilization);
		P_FINISH;
		P_NATIVE_BEGIN;
		UOculusXRFunctionLibrary::GetGPUUtilization(Z_Param_Out_IsGPUAvailable,Z_Param_Out_GPUUtilization);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UOculusXRFunctionLibrary::execHasSystemOverlayPresent)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UOculusXRFunctionLibrary::HasSystemOverlayPresent();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UOculusXRFunctionLibrary::execHasInputFocus)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UOculusXRFunctionLibrary::HasInputFocus();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UOculusXRFunctionLibrary::execClearLoadingSplashScreens)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		UOculusXRFunctionLibrary::ClearLoadingSplashScreens();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UOculusXRFunctionLibrary::execAddLoadingSplashScreen)
	{
		P_GET_OBJECT(UTexture2D,Z_Param_Texture);
		P_GET_STRUCT(FVector,Z_Param_TranslationInMeters);
		P_GET_STRUCT(FRotator,Z_Param_Rotation);
		P_GET_STRUCT(FVector2D,Z_Param_SizeInMeters);
		P_GET_STRUCT(FRotator,Z_Param_DeltaRotation);
		P_GET_UBOOL(Z_Param_bClearBeforeAdd);
		P_FINISH;
		P_NATIVE_BEGIN;
		UOculusXRFunctionLibrary::AddLoadingSplashScreen(Z_Param_Texture,Z_Param_TranslationInMeters,Z_Param_Rotation,Z_Param_SizeInMeters,Z_Param_DeltaRotation,Z_Param_bClearBeforeAdd);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UOculusXRFunctionLibrary::execGetBaseRotationAndPositionOffset)
	{
		P_GET_STRUCT_REF(FRotator,Z_Param_Out_OutRot);
		P_GET_STRUCT_REF(FVector,Z_Param_Out_OutPosOffset);
		P_FINISH;
		P_NATIVE_BEGIN;
		UOculusXRFunctionLibrary::GetBaseRotationAndPositionOffset(Z_Param_Out_OutRot,Z_Param_Out_OutPosOffset);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UOculusXRFunctionLibrary::execSetBaseRotationAndPositionOffset)
	{
		P_GET_STRUCT(FRotator,Z_Param_BaseRot);
		P_GET_STRUCT(FVector,Z_Param_PosOffset);
		P_GET_PROPERTY(FByteProperty,Z_Param_Options);
		P_FINISH;
		P_NATIVE_BEGIN;
		UOculusXRFunctionLibrary::SetBaseRotationAndPositionOffset(Z_Param_BaseRot,Z_Param_PosOffset,EOrientPositionSelector::Type(Z_Param_Options));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UOculusXRFunctionLibrary::execSetPositionScale3D)
	{
		P_GET_STRUCT(FVector,Z_Param_PosScale3D);
		P_FINISH;
		P_NATIVE_BEGIN;
		UOculusXRFunctionLibrary::SetPositionScale3D(Z_Param_PosScale3D);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UOculusXRFunctionLibrary::execGetBaseRotationAndBaseOffsetInMeters)
	{
		P_GET_STRUCT_REF(FRotator,Z_Param_Out_OutRotation);
		P_GET_STRUCT_REF(FVector,Z_Param_Out_OutBaseOffsetInMeters);
		P_FINISH;
		P_NATIVE_BEGIN;
		UOculusXRFunctionLibrary::GetBaseRotationAndBaseOffsetInMeters(Z_Param_Out_OutRotation,Z_Param_Out_OutBaseOffsetInMeters);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UOculusXRFunctionLibrary::execSetBaseRotationAndBaseOffsetInMeters)
	{
		P_GET_STRUCT(FRotator,Z_Param_Rotation);
		P_GET_STRUCT(FVector,Z_Param_BaseOffsetInMeters);
		P_GET_PROPERTY(FByteProperty,Z_Param_Options);
		P_FINISH;
		P_NATIVE_BEGIN;
		UOculusXRFunctionLibrary::SetBaseRotationAndBaseOffsetInMeters(Z_Param_Rotation,Z_Param_BaseOffsetInMeters,EOrientPositionSelector::Type(Z_Param_Options));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UOculusXRFunctionLibrary::execGetUserProfile)
	{
		P_GET_STRUCT_REF(FOculusXRHmdUserProfile,Z_Param_Out_Profile);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UOculusXRFunctionLibrary::GetUserProfile(Z_Param_Out_Profile);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UOculusXRFunctionLibrary::execSetSuggestedCpuAndGpuPerformanceLevels)
	{
		P_GET_ENUM(EOculusXRProcessorPerformanceLevel,Z_Param_CpuPerfLevel);
		P_GET_ENUM(EOculusXRProcessorPerformanceLevel,Z_Param_GpuPerfLevel);
		P_FINISH;
		P_NATIVE_BEGIN;
		UOculusXRFunctionLibrary::SetSuggestedCpuAndGpuPerformanceLevels(EOculusXRProcessorPerformanceLevel(Z_Param_CpuPerfLevel),EOculusXRProcessorPerformanceLevel(Z_Param_GpuPerfLevel));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UOculusXRFunctionLibrary::execGetSuggestedCpuAndGpuPerformanceLevels)
	{
		P_GET_ENUM_REF(EOculusXRProcessorPerformanceLevel,Z_Param_Out_CpuPerfLevel);
		P_GET_ENUM_REF(EOculusXRProcessorPerformanceLevel,Z_Param_Out_GpuPerfLevel);
		P_FINISH;
		P_NATIVE_BEGIN;
		UOculusXRFunctionLibrary::GetSuggestedCpuAndGpuPerformanceLevels((EOculusXRProcessorPerformanceLevel&)(Z_Param_Out_CpuPerfLevel),(EOculusXRProcessorPerformanceLevel&)(Z_Param_Out_GpuPerfLevel));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UOculusXRFunctionLibrary::execSetCPUAndGPULevels)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_CPULevel);
		P_GET_PROPERTY(FIntProperty,Z_Param_GPULevel);
		P_FINISH;
		P_NATIVE_BEGIN;
		UOculusXRFunctionLibrary::SetCPUAndGPULevels(Z_Param_CPULevel,Z_Param_GPULevel);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UOculusXRFunctionLibrary::execIsDeviceTracked)
	{
		P_GET_ENUM(EOculusXRTrackedDeviceType,Z_Param_DeviceType);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UOculusXRFunctionLibrary::IsDeviceTracked(EOculusXRTrackedDeviceType(Z_Param_DeviceType));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UOculusXRFunctionLibrary::execGetRawSensorData)
	{
		P_GET_STRUCT_REF(FVector,Z_Param_Out_AngularAcceleration);
		P_GET_STRUCT_REF(FVector,Z_Param_Out_LinearAcceleration);
		P_GET_STRUCT_REF(FVector,Z_Param_Out_AngularVelocity);
		P_GET_STRUCT_REF(FVector,Z_Param_Out_LinearVelocity);
		P_GET_PROPERTY_REF(FFloatProperty,Z_Param_Out_TimeInSeconds);
		P_GET_ENUM(EOculusXRTrackedDeviceType,Z_Param_DeviceType);
		P_FINISH;
		P_NATIVE_BEGIN;
		UOculusXRFunctionLibrary::GetRawSensorData(Z_Param_Out_AngularAcceleration,Z_Param_Out_LinearAcceleration,Z_Param_Out_AngularVelocity,Z_Param_Out_LinearVelocity,Z_Param_Out_TimeInSeconds,EOculusXRTrackedDeviceType(Z_Param_DeviceType));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UOculusXRFunctionLibrary::execGetPose)
	{
		P_GET_STRUCT_REF(FRotator,Z_Param_Out_DeviceRotation);
		P_GET_STRUCT_REF(FVector,Z_Param_Out_DevicePosition);
		P_GET_STRUCT_REF(FVector,Z_Param_Out_NeckPosition);
		P_GET_UBOOL(Z_Param_bUseOrienationForPlayerCamera);
		P_GET_UBOOL(Z_Param_bUsePositionForPlayerCamera);
		P_GET_STRUCT(FVector,Z_Param_PositionScale);
		P_FINISH;
		P_NATIVE_BEGIN;
		UOculusXRFunctionLibrary::GetPose(Z_Param_Out_DeviceRotation,Z_Param_Out_DevicePosition,Z_Param_Out_NeckPosition,Z_Param_bUseOrienationForPlayerCamera,Z_Param_bUsePositionForPlayerCamera,Z_Param_PositionScale);
		P_NATIVE_END;
	}
	void UOculusXRFunctionLibrary::StaticRegisterNativesUOculusXRFunctionLibrary()
	{
		UClass* Class = UOculusXRFunctionLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AddLoadingSplashScreen", &UOculusXRFunctionLibrary::execAddLoadingSplashScreen },
			{ "ClearLoadingSplashScreens", &UOculusXRFunctionLibrary::execClearLoadingSplashScreens },
			{ "EnableOrientationTracking", &UOculusXRFunctionLibrary::execEnableOrientationTracking },
			{ "EnablePositionTracking", &UOculusXRFunctionLibrary::execEnablePositionTracking },
			{ "GetAvailableDisplayFrequencies", &UOculusXRFunctionLibrary::execGetAvailableDisplayFrequencies },
			{ "GetBaseRotationAndBaseOffsetInMeters", &UOculusXRFunctionLibrary::execGetBaseRotationAndBaseOffsetInMeters },
			{ "GetBaseRotationAndPositionOffset", &UOculusXRFunctionLibrary::execGetBaseRotationAndPositionOffset },
			{ "GetControllerType", &UOculusXRFunctionLibrary::execGetControllerType },
			{ "GetCurrentDisplayFrequency", &UOculusXRFunctionLibrary::execGetCurrentDisplayFrequency },
			{ "GetDeviceName", &UOculusXRFunctionLibrary::execGetDeviceName },
			{ "GetDeviceType", &UOculusXRFunctionLibrary::execGetDeviceType },
			{ "GetEyeTrackedFoveatedRenderingSupported", &UOculusXRFunctionLibrary::execGetEyeTrackedFoveatedRenderingSupported },
			{ "GetFoveatedRenderingLevel", &UOculusXRFunctionLibrary::execGetFoveatedRenderingLevel },
			{ "GetFoveatedRenderingMethod", &UOculusXRFunctionLibrary::execGetFoveatedRenderingMethod },
			{ "GetGPUFrameTime", &UOculusXRFunctionLibrary::execGetGPUFrameTime },
			{ "GetGPUUtilization", &UOculusXRFunctionLibrary::execGetGPUUtilization },
			{ "GetGuardianDimensions", &UOculusXRFunctionLibrary::execGetGuardianDimensions },
			{ "GetGuardianPoints", &UOculusXRFunctionLibrary::execGetGuardianPoints },
			{ "GetHmdColorDesc", &UOculusXRFunctionLibrary::execGetHmdColorDesc },
			{ "GetNodeGuardianIntersection", &UOculusXRFunctionLibrary::execGetNodeGuardianIntersection },
			{ "GetPlayAreaTransform", &UOculusXRFunctionLibrary::execGetPlayAreaTransform },
			{ "GetPointGuardianIntersection", &UOculusXRFunctionLibrary::execGetPointGuardianIntersection },
			{ "GetPose", &UOculusXRFunctionLibrary::execGetPose },
			{ "GetRawSensorData", &UOculusXRFunctionLibrary::execGetRawSensorData },
			{ "GetSuggestedCpuAndGpuPerformanceLevels", &UOculusXRFunctionLibrary::execGetSuggestedCpuAndGpuPerformanceLevels },
			{ "GetSystemHmd3DofModeEnabled", &UOculusXRFunctionLibrary::execGetSystemHmd3DofModeEnabled },
			{ "GetUserProfile", &UOculusXRFunctionLibrary::execGetUserProfile },
			{ "HasInputFocus", &UOculusXRFunctionLibrary::execHasInputFocus },
			{ "HasSystemOverlayPresent", &UOculusXRFunctionLibrary::execHasSystemOverlayPresent },
			{ "IsColorPassthroughSupported", &UOculusXRFunctionLibrary::execIsColorPassthroughSupported },
			{ "IsDeviceTracked", &UOculusXRFunctionLibrary::execIsDeviceTracked },
			{ "IsGuardianConfigured", &UOculusXRFunctionLibrary::execIsGuardianConfigured },
			{ "IsGuardianDisplayed", &UOculusXRFunctionLibrary::execIsGuardianDisplayed },
			{ "IsPassthroughSupported", &UOculusXRFunctionLibrary::execIsPassthroughSupported },
			{ "SetBaseRotationAndBaseOffsetInMeters", &UOculusXRFunctionLibrary::execSetBaseRotationAndBaseOffsetInMeters },
			{ "SetBaseRotationAndPositionOffset", &UOculusXRFunctionLibrary::execSetBaseRotationAndPositionOffset },
			{ "SetClientColorDesc", &UOculusXRFunctionLibrary::execSetClientColorDesc },
			{ "SetColorScaleAndOffset", &UOculusXRFunctionLibrary::execSetColorScaleAndOffset },
			{ "SetCPUAndGPULevels", &UOculusXRFunctionLibrary::execSetCPUAndGPULevels },
			{ "SetDisplayFrequency", &UOculusXRFunctionLibrary::execSetDisplayFrequency },
			{ "SetFoveatedRenderingLevel", &UOculusXRFunctionLibrary::execSetFoveatedRenderingLevel },
			{ "SetFoveatedRenderingMethod", &UOculusXRFunctionLibrary::execSetFoveatedRenderingMethod },
			{ "SetGuardianVisibility", &UOculusXRFunctionLibrary::execSetGuardianVisibility },
			{ "SetLocalDimmingOn", &UOculusXRFunctionLibrary::execSetLocalDimmingOn },
			{ "SetPositionScale3D", &UOculusXRFunctionLibrary::execSetPositionScale3D },
			{ "SetSuggestedCpuAndGpuPerformanceLevels", &UOculusXRFunctionLibrary::execSetSuggestedCpuAndGpuPerformanceLevels },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UOculusXRFunctionLibrary_AddLoadingSplashScreen_Statics
	{
		struct OculusXRFunctionLibrary_eventAddLoadingSplashScreen_Parms
		{
			UTexture2D* Texture;
			FVector TranslationInMeters;
			FRotator Rotation;
			FVector2D SizeInMeters;
			FRotator DeltaRotation;
			bool bClearBeforeAdd;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_Texture;
		static const UECodeGen_Private::FStructPropertyParams NewProp_TranslationInMeters;
		static const UECodeGen_Private::FStructPropertyParams NewProp_Rotation;
		static const UECodeGen_Private::FStructPropertyParams NewProp_SizeInMeters;
		static const UECodeGen_Private::FStructPropertyParams NewProp_DeltaRotation;
		static void NewProp_bClearBeforeAdd_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_bClearBeforeAdd;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_AddLoadingSplashScreen_Statics::NewProp_Texture = { "Texture", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventAddLoadingSplashScreen_Parms, Texture), Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_AddLoadingSplashScreen_Statics::NewProp_TranslationInMeters = { "TranslationInMeters", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventAddLoadingSplashScreen_Parms, TranslationInMeters), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_AddLoadingSplashScreen_Statics::NewProp_Rotation = { "Rotation", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventAddLoadingSplashScreen_Parms, Rotation), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_AddLoadingSplashScreen_Statics::NewProp_SizeInMeters = { "SizeInMeters", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventAddLoadingSplashScreen_Parms, SizeInMeters), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_AddLoadingSplashScreen_Statics::NewProp_DeltaRotation = { "DeltaRotation", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventAddLoadingSplashScreen_Parms, DeltaRotation), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UOculusXRFunctionLibrary_AddLoadingSplashScreen_Statics::NewProp_bClearBeforeAdd_SetBit(void* Obj)
	{
		((OculusXRFunctionLibrary_eventAddLoadingSplashScreen_Parms*)Obj)->bClearBeforeAdd = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_AddLoadingSplashScreen_Statics::NewProp_bClearBeforeAdd = { "bClearBeforeAdd", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(OculusXRFunctionLibrary_eventAddLoadingSplashScreen_Parms), &Z_Construct_UFunction_UOculusXRFunctionLibrary_AddLoadingSplashScreen_Statics::NewProp_bClearBeforeAdd_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOculusXRFunctionLibrary_AddLoadingSplashScreen_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_AddLoadingSplashScreen_Statics::NewProp_Texture,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_AddLoadingSplashScreen_Statics::NewProp_TranslationInMeters,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_AddLoadingSplashScreen_Statics::NewProp_Rotation,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_AddLoadingSplashScreen_Statics::NewProp_SizeInMeters,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_AddLoadingSplashScreen_Statics::NewProp_DeltaRotation,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_AddLoadingSplashScreen_Statics::NewProp_bClearBeforeAdd,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOculusXRFunctionLibrary_AddLoadingSplashScreen_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "Comment", "/**\n\x09 * Adds loading splash screen with parameters\n\x09 *\n\x09 * @param Texture\x09\x09\x09(in) A texture asset to be used for the splash.\n\x09 * @param TranslationInMeters (in) Initial translation of the center of the splash screen (in meters).\n\x09 * @param Rotation\x09\x09\x09(in) Initial rotation of the splash screen, with the origin at the center of the splash screen.\n\x09 * @param SizeInMeters\x09\x09(in) Size, in meters, of the quad with the splash screen.\n\x09 * @param DeltaRotation\x09\x09(in) Incremental rotation, that is added each 2nd frame to the quad transform. The quad is rotated around the center of the quad.\n\x09 * @param bClearBeforeAdd\x09(in) If true, clears splashes before adding a new one.\n\x09 */" },
		{ "CPP_Default_bClearBeforeAdd", "false" },
		{ "CPP_Default_DeltaRotation", "" },
		{ "CPP_Default_SizeInMeters", "(X=1.000,Y=1.000)" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "Use Add Loading Screen Splash from the Head Mounted Display Loading Screen functions instead." },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "ToolTip", "Adds loading splash screen with parameters\n\n@param Texture                       (in) A texture asset to be used for the splash.\n@param TranslationInMeters (in) Initial translation of the center of the splash screen (in meters).\n@param Rotation                      (in) Initial rotation of the splash screen, with the origin at the center of the splash screen.\n@param SizeInMeters          (in) Size, in meters, of the quad with the splash screen.\n@param DeltaRotation         (in) Incremental rotation, that is added each 2nd frame to the quad transform. The quad is rotated around the center of the quad.\n@param bClearBeforeAdd       (in) If true, clears splashes before adding a new one." },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UOculusXRFunctionLibrary_AddLoadingSplashScreen_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOculusXRFunctionLibrary, nullptr, "AddLoadingSplashScreen", nullptr, nullptr, sizeof(Z_Construct_UFunction_UOculusXRFunctionLibrary_AddLoadingSplashScreen_Statics::OculusXRFunctionLibrary_eventAddLoadingSplashScreen_Parms), Z_Construct_UFunction_UOculusXRFunctionLibrary_AddLoadingSplashScreen_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_AddLoadingSplashScreen_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04822401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOculusXRFunctionLibrary_AddLoadingSplashScreen_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_AddLoadingSplashScreen_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOculusXRFunctionLibrary_AddLoadingSplashScreen()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UOculusXRFunctionLibrary_AddLoadingSplashScreen_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOculusXRFunctionLibrary_ClearLoadingSplashScreens_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOculusXRFunctionLibrary_ClearLoadingSplashScreens_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "Comment", "/**\n\x09 * Removes all the splash screens.\n\x09 */" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "Use Clear Loading Screen Splashes from the Head Mounted Display Loading Screen functions instead." },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "ToolTip", "Removes all the splash screens." },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UOculusXRFunctionLibrary_ClearLoadingSplashScreens_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOculusXRFunctionLibrary, nullptr, "ClearLoadingSplashScreens", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOculusXRFunctionLibrary_ClearLoadingSplashScreens_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_ClearLoadingSplashScreens_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOculusXRFunctionLibrary_ClearLoadingSplashScreens()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UOculusXRFunctionLibrary_ClearLoadingSplashScreens_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOculusXRFunctionLibrary_EnableOrientationTracking_Statics
	{
		struct OculusXRFunctionLibrary_eventEnableOrientationTracking_Parms
		{
			bool bOrientationTracking;
		};
		static void NewProp_bOrientationTracking_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_bOrientationTracking;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UOculusXRFunctionLibrary_EnableOrientationTracking_Statics::NewProp_bOrientationTracking_SetBit(void* Obj)
	{
		((OculusXRFunctionLibrary_eventEnableOrientationTracking_Parms*)Obj)->bOrientationTracking = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_EnableOrientationTracking_Statics::NewProp_bOrientationTracking = { "bOrientationTracking", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(OculusXRFunctionLibrary_eventEnableOrientationTracking_Parms), &Z_Construct_UFunction_UOculusXRFunctionLibrary_EnableOrientationTracking_Statics::NewProp_bOrientationTracking_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOculusXRFunctionLibrary_EnableOrientationTracking_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_EnableOrientationTracking_Statics::NewProp_bOrientationTracking,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOculusXRFunctionLibrary_EnableOrientationTracking_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "Comment", "/**\n\x09* Enables/disables orientation tracking on devices that support it.\n\x09*/" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "ToolTip", "Enables/disables orientation tracking on devices that support it." },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UOculusXRFunctionLibrary_EnableOrientationTracking_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOculusXRFunctionLibrary, nullptr, "EnableOrientationTracking", nullptr, nullptr, sizeof(Z_Construct_UFunction_UOculusXRFunctionLibrary_EnableOrientationTracking_Statics::OculusXRFunctionLibrary_eventEnableOrientationTracking_Parms), Z_Construct_UFunction_UOculusXRFunctionLibrary_EnableOrientationTracking_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_EnableOrientationTracking_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOculusXRFunctionLibrary_EnableOrientationTracking_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_EnableOrientationTracking_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOculusXRFunctionLibrary_EnableOrientationTracking()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UOculusXRFunctionLibrary_EnableOrientationTracking_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOculusXRFunctionLibrary_EnablePositionTracking_Statics
	{
		struct OculusXRFunctionLibrary_eventEnablePositionTracking_Parms
		{
			bool bPositionTracking;
		};
		static void NewProp_bPositionTracking_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_bPositionTracking;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UOculusXRFunctionLibrary_EnablePositionTracking_Statics::NewProp_bPositionTracking_SetBit(void* Obj)
	{
		((OculusXRFunctionLibrary_eventEnablePositionTracking_Parms*)Obj)->bPositionTracking = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_EnablePositionTracking_Statics::NewProp_bPositionTracking = { "bPositionTracking", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(OculusXRFunctionLibrary_eventEnablePositionTracking_Parms), &Z_Construct_UFunction_UOculusXRFunctionLibrary_EnablePositionTracking_Statics::NewProp_bPositionTracking_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOculusXRFunctionLibrary_EnablePositionTracking_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_EnablePositionTracking_Statics::NewProp_bPositionTracking,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOculusXRFunctionLibrary_EnablePositionTracking_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "Comment", "/**\n\x09* Enables/disables positional tracking on devices that support it.\n\x09*/" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "ToolTip", "Enables/disables positional tracking on devices that support it." },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UOculusXRFunctionLibrary_EnablePositionTracking_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOculusXRFunctionLibrary, nullptr, "EnablePositionTracking", nullptr, nullptr, sizeof(Z_Construct_UFunction_UOculusXRFunctionLibrary_EnablePositionTracking_Statics::OculusXRFunctionLibrary_eventEnablePositionTracking_Parms), Z_Construct_UFunction_UOculusXRFunctionLibrary_EnablePositionTracking_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_EnablePositionTracking_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOculusXRFunctionLibrary_EnablePositionTracking_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_EnablePositionTracking_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOculusXRFunctionLibrary_EnablePositionTracking()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UOculusXRFunctionLibrary_EnablePositionTracking_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOculusXRFunctionLibrary_GetAvailableDisplayFrequencies_Statics
	{
		struct OculusXRFunctionLibrary_eventGetAvailableDisplayFrequencies_Parms
		{
			TArray<float> ReturnValue;
		};
		static const UECodeGen_Private::FFloatPropertyParams NewProp_ReturnValue_Inner;
		static const UECodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetAvailableDisplayFrequencies_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetAvailableDisplayFrequencies_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventGetAvailableDisplayFrequencies_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOculusXRFunctionLibrary_GetAvailableDisplayFrequencies_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetAvailableDisplayFrequencies_Statics::NewProp_ReturnValue_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetAvailableDisplayFrequencies_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOculusXRFunctionLibrary_GetAvailableDisplayFrequencies_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "Comment", "/**\n\x09* Returns the current available frequencies\n\x09*/" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "ToolTip", "Returns the current available frequencies" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetAvailableDisplayFrequencies_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOculusXRFunctionLibrary, nullptr, "GetAvailableDisplayFrequencies", nullptr, nullptr, sizeof(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetAvailableDisplayFrequencies_Statics::OculusXRFunctionLibrary_eventGetAvailableDisplayFrequencies_Parms), Z_Construct_UFunction_UOculusXRFunctionLibrary_GetAvailableDisplayFrequencies_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetAvailableDisplayFrequencies_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetAvailableDisplayFrequencies_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetAvailableDisplayFrequencies_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOculusXRFunctionLibrary_GetAvailableDisplayFrequencies()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UOculusXRFunctionLibrary_GetAvailableDisplayFrequencies_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOculusXRFunctionLibrary_GetBaseRotationAndBaseOffsetInMeters_Statics
	{
		struct OculusXRFunctionLibrary_eventGetBaseRotationAndBaseOffsetInMeters_Parms
		{
			FRotator OutRotation;
			FVector OutBaseOffsetInMeters;
		};
		static const UECodeGen_Private::FStructPropertyParams NewProp_OutRotation;
		static const UECodeGen_Private::FStructPropertyParams NewProp_OutBaseOffsetInMeters;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetBaseRotationAndBaseOffsetInMeters_Statics::NewProp_OutRotation = { "OutRotation", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventGetBaseRotationAndBaseOffsetInMeters_Parms, OutRotation), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetBaseRotationAndBaseOffsetInMeters_Statics::NewProp_OutBaseOffsetInMeters = { "OutBaseOffsetInMeters", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventGetBaseRotationAndBaseOffsetInMeters_Parms, OutBaseOffsetInMeters), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOculusXRFunctionLibrary_GetBaseRotationAndBaseOffsetInMeters_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetBaseRotationAndBaseOffsetInMeters_Statics::NewProp_OutRotation,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetBaseRotationAndBaseOffsetInMeters_Statics::NewProp_OutBaseOffsetInMeters,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOculusXRFunctionLibrary_GetBaseRotationAndBaseOffsetInMeters_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "Comment", "/**\n\x09* Returns current base rotation and base offset.\n\x09* The base offset is currently used base position offset, previously set by the\n\x09* ResetPosition or SetBasePositionOffset calls. It represents a vector that translates the HMD's position\n\x09* into (0,0,0) point, in meters.\n\x09* The axis of the vector are the same as in Unreal: X - forward, Y - right, Z - up.\n\x09*\n\x09* @param OutRotation\x09\x09\x09(out) Rotator object with base rotation\n\x09* @param OutBaseOffsetInMeters\x09(out) base position offset, vector, in meters.\n\x09*/" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "ToolTip", "Returns current base rotation and base offset.\nThe base offset is currently used base position offset, previously set by the\nResetPosition or SetBasePositionOffset calls. It represents a vector that translates the HMD's position\ninto (0,0,0) point, in meters.\nThe axis of the vector are the same as in Unreal: X - forward, Y - right, Z - up.\n\n@param OutRotation                    (out) Rotator object with base rotation\n@param OutBaseOffsetInMeters  (out) base position offset, vector, in meters." },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetBaseRotationAndBaseOffsetInMeters_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOculusXRFunctionLibrary, nullptr, "GetBaseRotationAndBaseOffsetInMeters", nullptr, nullptr, sizeof(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetBaseRotationAndBaseOffsetInMeters_Statics::OculusXRFunctionLibrary_eventGetBaseRotationAndBaseOffsetInMeters_Parms), Z_Construct_UFunction_UOculusXRFunctionLibrary_GetBaseRotationAndBaseOffsetInMeters_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetBaseRotationAndBaseOffsetInMeters_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14C22401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetBaseRotationAndBaseOffsetInMeters_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetBaseRotationAndBaseOffsetInMeters_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOculusXRFunctionLibrary_GetBaseRotationAndBaseOffsetInMeters()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UOculusXRFunctionLibrary_GetBaseRotationAndBaseOffsetInMeters_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOculusXRFunctionLibrary_GetBaseRotationAndPositionOffset_Statics
	{
		struct OculusXRFunctionLibrary_eventGetBaseRotationAndPositionOffset_Parms
		{
			FRotator OutRot;
			FVector OutPosOffset;
		};
		static const UECodeGen_Private::FStructPropertyParams NewProp_OutRot;
		static const UECodeGen_Private::FStructPropertyParams NewProp_OutPosOffset;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetBaseRotationAndPositionOffset_Statics::NewProp_OutRot = { "OutRot", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventGetBaseRotationAndPositionOffset_Parms, OutRot), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetBaseRotationAndPositionOffset_Statics::NewProp_OutPosOffset = { "OutPosOffset", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventGetBaseRotationAndPositionOffset_Parms, OutPosOffset), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOculusXRFunctionLibrary_GetBaseRotationAndPositionOffset_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetBaseRotationAndPositionOffset_Statics::NewProp_OutRot,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetBaseRotationAndPositionOffset_Statics::NewProp_OutPosOffset,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOculusXRFunctionLibrary_GetBaseRotationAndPositionOffset_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "Comment", "/**\n\x09 * Returns current base rotation and position offset.\n\x09 *\n\x09 * @param OutRot\x09\x09\x09(out) Rotator object with base rotation\n\x09 * @param OutPosOffset\x09\x09(out) the vector with previously set position offset.\n\x09 */" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "A hack, proper camera positioning should be used" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "ToolTip", "Returns current base rotation and position offset.\n\n@param OutRot                        (out) Rotator object with base rotation\n@param OutPosOffset          (out) the vector with previously set position offset." },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetBaseRotationAndPositionOffset_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOculusXRFunctionLibrary, nullptr, "GetBaseRotationAndPositionOffset", nullptr, nullptr, sizeof(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetBaseRotationAndPositionOffset_Statics::OculusXRFunctionLibrary_eventGetBaseRotationAndPositionOffset_Parms), Z_Construct_UFunction_UOculusXRFunctionLibrary_GetBaseRotationAndPositionOffset_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetBaseRotationAndPositionOffset_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C22401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetBaseRotationAndPositionOffset_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetBaseRotationAndPositionOffset_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOculusXRFunctionLibrary_GetBaseRotationAndPositionOffset()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UOculusXRFunctionLibrary_GetBaseRotationAndPositionOffset_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOculusXRFunctionLibrary_GetControllerType_Statics
	{
		struct OculusXRFunctionLibrary_eventGetControllerType_Parms
		{
			EControllerHand deviceHand;
			EOculusXRControllerType ReturnValue;
		};
		static const UECodeGen_Private::FBytePropertyParams NewProp_deviceHand_Underlying;
		static const UECodeGen_Private::FEnumPropertyParams NewProp_deviceHand;
		static const UECodeGen_Private::FBytePropertyParams NewProp_ReturnValue_Underlying;
		static const UECodeGen_Private::FEnumPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetControllerType_Statics::NewProp_deviceHand_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetControllerType_Statics::NewProp_deviceHand = { "deviceHand", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventGetControllerType_Parms, deviceHand), Z_Construct_UEnum_InputCore_EControllerHand, METADATA_PARAMS(nullptr, 0) }; // 2206298931
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetControllerType_Statics::NewProp_ReturnValue_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetControllerType_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventGetControllerType_Parms, ReturnValue), Z_Construct_UEnum_OculusXRHMD_EOculusXRControllerType, METADATA_PARAMS(nullptr, 0) }; // 2817971156
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOculusXRFunctionLibrary_GetControllerType_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetControllerType_Statics::NewProp_deviceHand_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetControllerType_Statics::NewProp_deviceHand,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetControllerType_Statics::NewProp_ReturnValue_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetControllerType_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOculusXRFunctionLibrary_GetControllerType_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "Comment", "/**\n\x09* Returns the current controller's type\n\x09* @param deviceHand\x09\x09\x09\x09(in) The hand to get the position from\n\x09*/" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "ToolTip", "Returns the current controller's type\n@param deviceHand                             (in) The hand to get the position from" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetControllerType_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOculusXRFunctionLibrary, nullptr, "GetControllerType", nullptr, nullptr, sizeof(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetControllerType_Statics::OculusXRFunctionLibrary_eventGetControllerType_Parms), Z_Construct_UFunction_UOculusXRFunctionLibrary_GetControllerType_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetControllerType_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetControllerType_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetControllerType_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOculusXRFunctionLibrary_GetControllerType()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UOculusXRFunctionLibrary_GetControllerType_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOculusXRFunctionLibrary_GetCurrentDisplayFrequency_Statics
	{
		struct OculusXRFunctionLibrary_eventGetCurrentDisplayFrequency_Parms
		{
			float ReturnValue;
		};
		static const UECodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetCurrentDisplayFrequency_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventGetCurrentDisplayFrequency_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOculusXRFunctionLibrary_GetCurrentDisplayFrequency_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetCurrentDisplayFrequency_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOculusXRFunctionLibrary_GetCurrentDisplayFrequency_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "Comment", "/**\n\x09* Returns the current display frequency\n\x09*/" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "ToolTip", "Returns the current display frequency" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetCurrentDisplayFrequency_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOculusXRFunctionLibrary, nullptr, "GetCurrentDisplayFrequency", nullptr, nullptr, sizeof(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetCurrentDisplayFrequency_Statics::OculusXRFunctionLibrary_eventGetCurrentDisplayFrequency_Parms), Z_Construct_UFunction_UOculusXRFunctionLibrary_GetCurrentDisplayFrequency_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetCurrentDisplayFrequency_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetCurrentDisplayFrequency_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetCurrentDisplayFrequency_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOculusXRFunctionLibrary_GetCurrentDisplayFrequency()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UOculusXRFunctionLibrary_GetCurrentDisplayFrequency_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOculusXRFunctionLibrary_GetDeviceName_Statics
	{
		struct OculusXRFunctionLibrary_eventGetDeviceName_Parms
		{
			FString ReturnValue;
		};
		static const UECodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetDeviceName_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventGetDeviceName_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOculusXRFunctionLibrary_GetDeviceName_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetDeviceName_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOculusXRFunctionLibrary_GetDeviceName_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "UOculusXRFunctionLibrary::GetDeviceName has been deprecated and no longer functions as before. Please use the enum-based GetDeviceType instead." },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetDeviceName_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOculusXRFunctionLibrary, nullptr, "GetDeviceName", nullptr, nullptr, sizeof(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetDeviceName_Statics::OculusXRFunctionLibrary_eventGetDeviceName_Parms), Z_Construct_UFunction_UOculusXRFunctionLibrary_GetDeviceName_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetDeviceName_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetDeviceName_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetDeviceName_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOculusXRFunctionLibrary_GetDeviceName()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UOculusXRFunctionLibrary_GetDeviceName_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOculusXRFunctionLibrary_GetDeviceType_Statics
	{
		struct OculusXRFunctionLibrary_eventGetDeviceType_Parms
		{
			EOculusXRDeviceType ReturnValue;
		};
		static const UECodeGen_Private::FBytePropertyParams NewProp_ReturnValue_Underlying;
		static const UECodeGen_Private::FEnumPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetDeviceType_Statics::NewProp_ReturnValue_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetDeviceType_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventGetDeviceType_Parms, ReturnValue), Z_Construct_UEnum_OculusXRHMD_EOculusXRDeviceType, METADATA_PARAMS(nullptr, 0) }; // 4006955709
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOculusXRFunctionLibrary_GetDeviceType_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetDeviceType_Statics::NewProp_ReturnValue_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetDeviceType_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOculusXRFunctionLibrary_GetDeviceType_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetDeviceType_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOculusXRFunctionLibrary, nullptr, "GetDeviceType", nullptr, nullptr, sizeof(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetDeviceType_Statics::OculusXRFunctionLibrary_eventGetDeviceType_Parms), Z_Construct_UFunction_UOculusXRFunctionLibrary_GetDeviceType_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetDeviceType_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetDeviceType_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetDeviceType_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOculusXRFunctionLibrary_GetDeviceType()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UOculusXRFunctionLibrary_GetDeviceType_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOculusXRFunctionLibrary_GetEyeTrackedFoveatedRenderingSupported_Statics
	{
		struct OculusXRFunctionLibrary_eventGetEyeTrackedFoveatedRenderingSupported_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UOculusXRFunctionLibrary_GetEyeTrackedFoveatedRenderingSupported_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((OculusXRFunctionLibrary_eventGetEyeTrackedFoveatedRenderingSupported_Parms*)Obj)->ReturnValue = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetEyeTrackedFoveatedRenderingSupported_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(OculusXRFunctionLibrary_eventGetEyeTrackedFoveatedRenderingSupported_Parms), &Z_Construct_UFunction_UOculusXRFunctionLibrary_GetEyeTrackedFoveatedRenderingSupported_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOculusXRFunctionLibrary_GetEyeTrackedFoveatedRenderingSupported_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetEyeTrackedFoveatedRenderingSupported_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOculusXRFunctionLibrary_GetEyeTrackedFoveatedRenderingSupported_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "Comment", "/**\n\x09* Returns whether eye-tracked foveated rendering is supported or not\n\x09*/" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "ToolTip", "Returns whether eye-tracked foveated rendering is supported or not" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetEyeTrackedFoveatedRenderingSupported_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOculusXRFunctionLibrary, nullptr, "GetEyeTrackedFoveatedRenderingSupported", nullptr, nullptr, sizeof(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetEyeTrackedFoveatedRenderingSupported_Statics::OculusXRFunctionLibrary_eventGetEyeTrackedFoveatedRenderingSupported_Parms), Z_Construct_UFunction_UOculusXRFunctionLibrary_GetEyeTrackedFoveatedRenderingSupported_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetEyeTrackedFoveatedRenderingSupported_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetEyeTrackedFoveatedRenderingSupported_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetEyeTrackedFoveatedRenderingSupported_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOculusXRFunctionLibrary_GetEyeTrackedFoveatedRenderingSupported()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UOculusXRFunctionLibrary_GetEyeTrackedFoveatedRenderingSupported_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOculusXRFunctionLibrary_GetFoveatedRenderingLevel_Statics
	{
		struct OculusXRFunctionLibrary_eventGetFoveatedRenderingLevel_Parms
		{
			EOculusXRFoveatedRenderingLevel ReturnValue;
		};
		static const UECodeGen_Private::FBytePropertyParams NewProp_ReturnValue_Underlying;
		static const UECodeGen_Private::FEnumPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetFoveatedRenderingLevel_Statics::NewProp_ReturnValue_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetFoveatedRenderingLevel_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventGetFoveatedRenderingLevel_Parms, ReturnValue), Z_Construct_UEnum_OculusXRHMD_EOculusXRFoveatedRenderingLevel, METADATA_PARAMS(nullptr, 0) }; // 37067298
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOculusXRFunctionLibrary_GetFoveatedRenderingLevel_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetFoveatedRenderingLevel_Statics::NewProp_ReturnValue_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetFoveatedRenderingLevel_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOculusXRFunctionLibrary_GetFoveatedRenderingLevel_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "Comment", "/**\n\x09* Returns the current multiresolution level\n\x09*/" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "ToolTip", "Returns the current multiresolution level" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetFoveatedRenderingLevel_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOculusXRFunctionLibrary, nullptr, "GetFoveatedRenderingLevel", nullptr, nullptr, sizeof(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetFoveatedRenderingLevel_Statics::OculusXRFunctionLibrary_eventGetFoveatedRenderingLevel_Parms), Z_Construct_UFunction_UOculusXRFunctionLibrary_GetFoveatedRenderingLevel_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetFoveatedRenderingLevel_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetFoveatedRenderingLevel_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetFoveatedRenderingLevel_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOculusXRFunctionLibrary_GetFoveatedRenderingLevel()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UOculusXRFunctionLibrary_GetFoveatedRenderingLevel_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOculusXRFunctionLibrary_GetFoveatedRenderingMethod_Statics
	{
		struct OculusXRFunctionLibrary_eventGetFoveatedRenderingMethod_Parms
		{
			EOculusXRFoveatedRenderingMethod ReturnValue;
		};
		static const UECodeGen_Private::FBytePropertyParams NewProp_ReturnValue_Underlying;
		static const UECodeGen_Private::FEnumPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetFoveatedRenderingMethod_Statics::NewProp_ReturnValue_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetFoveatedRenderingMethod_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventGetFoveatedRenderingMethod_Parms, ReturnValue), Z_Construct_UEnum_OculusXRHMD_EOculusXRFoveatedRenderingMethod, METADATA_PARAMS(nullptr, 0) }; // 709529947
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOculusXRFunctionLibrary_GetFoveatedRenderingMethod_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetFoveatedRenderingMethod_Statics::NewProp_ReturnValue_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetFoveatedRenderingMethod_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOculusXRFunctionLibrary_GetFoveatedRenderingMethod_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "Comment", "/**\n\x09* Returns the foveated rendering method currently being used\n\x09*/" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "ToolTip", "Returns the foveated rendering method currently being used" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetFoveatedRenderingMethod_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOculusXRFunctionLibrary, nullptr, "GetFoveatedRenderingMethod", nullptr, nullptr, sizeof(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetFoveatedRenderingMethod_Statics::OculusXRFunctionLibrary_eventGetFoveatedRenderingMethod_Parms), Z_Construct_UFunction_UOculusXRFunctionLibrary_GetFoveatedRenderingMethod_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetFoveatedRenderingMethod_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetFoveatedRenderingMethod_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetFoveatedRenderingMethod_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOculusXRFunctionLibrary_GetFoveatedRenderingMethod()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UOculusXRFunctionLibrary_GetFoveatedRenderingMethod_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGPUFrameTime_Statics
	{
		struct OculusXRFunctionLibrary_eventGetGPUFrameTime_Parms
		{
			float ReturnValue;
		};
		static const UECodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGPUFrameTime_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventGetGPUFrameTime_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGPUFrameTime_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGPUFrameTime_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGPUFrameTime_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "Comment", "/**\n\x09* Returns the GPU frame time on supported mobile platforms (Go for now)\n\x09*/" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "ToolTip", "Returns the GPU frame time on supported mobile platforms (Go for now)" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGPUFrameTime_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOculusXRFunctionLibrary, nullptr, "GetGPUFrameTime", nullptr, nullptr, sizeof(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGPUFrameTime_Statics::OculusXRFunctionLibrary_eventGetGPUFrameTime_Parms), Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGPUFrameTime_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGPUFrameTime_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGPUFrameTime_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGPUFrameTime_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGPUFrameTime()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGPUFrameTime_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGPUUtilization_Statics
	{
		struct OculusXRFunctionLibrary_eventGetGPUUtilization_Parms
		{
			bool IsGPUAvailable;
			float GPUUtilization;
		};
		static void NewProp_IsGPUAvailable_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_IsGPUAvailable;
		static const UECodeGen_Private::FFloatPropertyParams NewProp_GPUUtilization;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGPUUtilization_Statics::NewProp_IsGPUAvailable_SetBit(void* Obj)
	{
		((OculusXRFunctionLibrary_eventGetGPUUtilization_Parms*)Obj)->IsGPUAvailable = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGPUUtilization_Statics::NewProp_IsGPUAvailable = { "IsGPUAvailable", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(OculusXRFunctionLibrary_eventGetGPUUtilization_Parms), &Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGPUUtilization_Statics::NewProp_IsGPUAvailable_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGPUUtilization_Statics::NewProp_GPUUtilization = { "GPUUtilization", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventGetGPUUtilization_Parms, GPUUtilization), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGPUUtilization_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGPUUtilization_Statics::NewProp_IsGPUAvailable,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGPUUtilization_Statics::NewProp_GPUUtilization,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGPUUtilization_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "Comment", "/**\n\x09* Returns the GPU utilization availability and value\n\x09*/" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "ToolTip", "Returns the GPU utilization availability and value" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGPUUtilization_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOculusXRFunctionLibrary, nullptr, "GetGPUUtilization", nullptr, nullptr, sizeof(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGPUUtilization_Statics::OculusXRFunctionLibrary_eventGetGPUUtilization_Parms), Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGPUUtilization_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGPUUtilization_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGPUUtilization_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGPUUtilization_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGPUUtilization()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGPUUtilization_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGuardianDimensions_Statics
	{
		struct OculusXRFunctionLibrary_eventGetGuardianDimensions_Parms
		{
			EOculusXRBoundaryType BoundaryType;
			FVector ReturnValue;
		};
		static const UECodeGen_Private::FBytePropertyParams NewProp_BoundaryType_Underlying;
		static const UECodeGen_Private::FEnumPropertyParams NewProp_BoundaryType;
		static const UECodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGuardianDimensions_Statics::NewProp_BoundaryType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGuardianDimensions_Statics::NewProp_BoundaryType = { "BoundaryType", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventGetGuardianDimensions_Parms, BoundaryType), Z_Construct_UEnum_OculusXRHMD_EOculusXRBoundaryType, METADATA_PARAMS(nullptr, 0) }; // 938409106
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGuardianDimensions_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventGetGuardianDimensions_Parms, ReturnValue), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGuardianDimensions_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGuardianDimensions_Statics::NewProp_BoundaryType_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGuardianDimensions_Statics::NewProp_BoundaryType,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGuardianDimensions_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGuardianDimensions_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary|Guardian" },
		{ "Comment", "/**\n\x09* Returns the dimensions in UE world space of the requested Boundary Type\n\x09* @param BoundaryType\x09\x09\x09(in) An enum representing the boundary type requested, either Outer Boundary (exact guardian bounds) or PlayArea (rectangle inside the Outer Boundary)\n\x09*/" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "ToolTip", "Returns the dimensions in UE world space of the requested Boundary Type\n@param BoundaryType                   (in) An enum representing the boundary type requested, either Outer Boundary (exact guardian bounds) or PlayArea (rectangle inside the Outer Boundary)" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGuardianDimensions_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOculusXRFunctionLibrary, nullptr, "GetGuardianDimensions", nullptr, nullptr, sizeof(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGuardianDimensions_Statics::OculusXRFunctionLibrary_eventGetGuardianDimensions_Parms), Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGuardianDimensions_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGuardianDimensions_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14822401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGuardianDimensions_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGuardianDimensions_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGuardianDimensions()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGuardianDimensions_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGuardianPoints_Statics
	{
		struct OculusXRFunctionLibrary_eventGetGuardianPoints_Parms
		{
			EOculusXRBoundaryType BoundaryType;
			bool UsePawnSpace;
			TArray<FVector> ReturnValue;
		};
		static const UECodeGen_Private::FBytePropertyParams NewProp_BoundaryType_Underlying;
		static const UECodeGen_Private::FEnumPropertyParams NewProp_BoundaryType;
		static void NewProp_UsePawnSpace_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_UsePawnSpace;
		static const UECodeGen_Private::FStructPropertyParams NewProp_ReturnValue_Inner;
		static const UECodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGuardianPoints_Statics::NewProp_BoundaryType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGuardianPoints_Statics::NewProp_BoundaryType = { "BoundaryType", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventGetGuardianPoints_Parms, BoundaryType), Z_Construct_UEnum_OculusXRHMD_EOculusXRBoundaryType, METADATA_PARAMS(nullptr, 0) }; // 938409106
	void Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGuardianPoints_Statics::NewProp_UsePawnSpace_SetBit(void* Obj)
	{
		((OculusXRFunctionLibrary_eventGetGuardianPoints_Parms*)Obj)->UsePawnSpace = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGuardianPoints_Statics::NewProp_UsePawnSpace = { "UsePawnSpace", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(OculusXRFunctionLibrary_eventGetGuardianPoints_Parms), &Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGuardianPoints_Statics::NewProp_UsePawnSpace_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGuardianPoints_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGuardianPoints_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventGetGuardianPoints_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGuardianPoints_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGuardianPoints_Statics::NewProp_BoundaryType_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGuardianPoints_Statics::NewProp_BoundaryType,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGuardianPoints_Statics::NewProp_UsePawnSpace,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGuardianPoints_Statics::NewProp_ReturnValue_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGuardianPoints_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGuardianPoints_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary|Guardian" },
		{ "Comment", "/**\n\x09* Returns the list of points in UE world space of the requested Boundary Type \n\x09* @param BoundaryType\x09\x09\x09(in) An enum representing the boundary type requested, either Outer Boundary (exact guardian bounds) or PlayArea (rectangle inside the Outer Boundary)\n\x09* @param UsePawnSpace\x09\x09\x09(in) Boolean indicating to return the points in world space or pawn space\n\x09*/" },
		{ "CPP_Default_UsePawnSpace", "false" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "ToolTip", "Returns the list of points in UE world space of the requested Boundary Type\n@param BoundaryType                   (in) An enum representing the boundary type requested, either Outer Boundary (exact guardian bounds) or PlayArea (rectangle inside the Outer Boundary)\n@param UsePawnSpace                   (in) Boolean indicating to return the points in world space or pawn space" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGuardianPoints_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOculusXRFunctionLibrary, nullptr, "GetGuardianPoints", nullptr, nullptr, sizeof(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGuardianPoints_Statics::OculusXRFunctionLibrary_eventGetGuardianPoints_Parms), Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGuardianPoints_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGuardianPoints_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGuardianPoints_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGuardianPoints_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGuardianPoints()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGuardianPoints_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOculusXRFunctionLibrary_GetHmdColorDesc_Statics
	{
		struct OculusXRFunctionLibrary_eventGetHmdColorDesc_Parms
		{
			EOculusXRColorSpace ReturnValue;
		};
		static const UECodeGen_Private::FBytePropertyParams NewProp_ReturnValue_Underlying;
		static const UECodeGen_Private::FEnumPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetHmdColorDesc_Statics::NewProp_ReturnValue_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetHmdColorDesc_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventGetHmdColorDesc_Parms, ReturnValue), Z_Construct_UEnum_OculusXRHMD_EOculusXRColorSpace, METADATA_PARAMS(nullptr, 0) }; // 2189630421
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOculusXRFunctionLibrary_GetHmdColorDesc_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetHmdColorDesc_Statics::NewProp_ReturnValue_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetHmdColorDesc_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOculusXRFunctionLibrary_GetHmdColorDesc_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "Comment", "/**\n\x09* Returns the color space of the target HMD\n\x09*/" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "ToolTip", "Returns the color space of the target HMD" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetHmdColorDesc_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOculusXRFunctionLibrary, nullptr, "GetHmdColorDesc", nullptr, nullptr, sizeof(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetHmdColorDesc_Statics::OculusXRFunctionLibrary_eventGetHmdColorDesc_Parms), Z_Construct_UFunction_UOculusXRFunctionLibrary_GetHmdColorDesc_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetHmdColorDesc_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetHmdColorDesc_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetHmdColorDesc_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOculusXRFunctionLibrary_GetHmdColorDesc()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UOculusXRFunctionLibrary_GetHmdColorDesc_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOculusXRFunctionLibrary_GetNodeGuardianIntersection_Statics
	{
		struct OculusXRFunctionLibrary_eventGetNodeGuardianIntersection_Parms
		{
			EOculusXRTrackedDeviceType DeviceType;
			EOculusXRBoundaryType BoundaryType;
			FOculusXRGuardianTestResult ReturnValue;
		};
		static const UECodeGen_Private::FBytePropertyParams NewProp_DeviceType_Underlying;
		static const UECodeGen_Private::FEnumPropertyParams NewProp_DeviceType;
		static const UECodeGen_Private::FBytePropertyParams NewProp_BoundaryType_Underlying;
		static const UECodeGen_Private::FEnumPropertyParams NewProp_BoundaryType;
		static const UECodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetNodeGuardianIntersection_Statics::NewProp_DeviceType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetNodeGuardianIntersection_Statics::NewProp_DeviceType = { "DeviceType", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventGetNodeGuardianIntersection_Parms, DeviceType), Z_Construct_UEnum_OculusXRHMD_EOculusXRTrackedDeviceType, METADATA_PARAMS(nullptr, 0) }; // 125253818
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetNodeGuardianIntersection_Statics::NewProp_BoundaryType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetNodeGuardianIntersection_Statics::NewProp_BoundaryType = { "BoundaryType", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventGetNodeGuardianIntersection_Parms, BoundaryType), Z_Construct_UEnum_OculusXRHMD_EOculusXRBoundaryType, METADATA_PARAMS(nullptr, 0) }; // 938409106
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetNodeGuardianIntersection_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventGetNodeGuardianIntersection_Parms, ReturnValue), Z_Construct_UScriptStruct_FOculusXRGuardianTestResult, METADATA_PARAMS(nullptr, 0) }; // 910032650
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOculusXRFunctionLibrary_GetNodeGuardianIntersection_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetNodeGuardianIntersection_Statics::NewProp_DeviceType_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetNodeGuardianIntersection_Statics::NewProp_DeviceType,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetNodeGuardianIntersection_Statics::NewProp_BoundaryType_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetNodeGuardianIntersection_Statics::NewProp_BoundaryType,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetNodeGuardianIntersection_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOculusXRFunctionLibrary_GetNodeGuardianIntersection_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary|Guardian" },
		{ "Comment", "/**\n\x09* Get the intersection result between a tracked device (HMD or controllers) and a guardian boundary\n\x09* @param DeviceType             (in) Tracked Device type to test against guardian boundaries\n\x09* @param BoundaryType\x09\x09\x09(in) An enum representing the boundary type requested, either Outer Boundary (exact guardian bounds) or PlayArea (rectangle inside the Outer Boundary)\n\x09*/" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "ToolTip", "Get the intersection result between a tracked device (HMD or controllers) and a guardian boundary\n@param DeviceType             (in) Tracked Device type to test against guardian boundaries\n@param BoundaryType                   (in) An enum representing the boundary type requested, either Outer Boundary (exact guardian bounds) or PlayArea (rectangle inside the Outer Boundary)" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetNodeGuardianIntersection_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOculusXRFunctionLibrary, nullptr, "GetNodeGuardianIntersection", nullptr, nullptr, sizeof(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetNodeGuardianIntersection_Statics::OculusXRFunctionLibrary_eventGetNodeGuardianIntersection_Parms), Z_Construct_UFunction_UOculusXRFunctionLibrary_GetNodeGuardianIntersection_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetNodeGuardianIntersection_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetNodeGuardianIntersection_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetNodeGuardianIntersection_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOculusXRFunctionLibrary_GetNodeGuardianIntersection()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UOculusXRFunctionLibrary_GetNodeGuardianIntersection_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPlayAreaTransform_Statics
	{
		struct OculusXRFunctionLibrary_eventGetPlayAreaTransform_Parms
		{
			FTransform ReturnValue;
		};
		static const UECodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPlayAreaTransform_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventGetPlayAreaTransform_Parms, ReturnValue), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPlayAreaTransform_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPlayAreaTransform_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPlayAreaTransform_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary|Guardian" },
		{ "Comment", "/**\n\x09* Returns the transform of the play area rectangle, defining its position, rotation and scale to apply to a unit cube to match it with the play area.\n\x09*/" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "ToolTip", "Returns the transform of the play area rectangle, defining its position, rotation and scale to apply to a unit cube to match it with the play area." },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPlayAreaTransform_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOculusXRFunctionLibrary, nullptr, "GetPlayAreaTransform", nullptr, nullptr, sizeof(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPlayAreaTransform_Statics::OculusXRFunctionLibrary_eventGetPlayAreaTransform_Parms), Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPlayAreaTransform_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPlayAreaTransform_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14822401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPlayAreaTransform_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPlayAreaTransform_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPlayAreaTransform()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPlayAreaTransform_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPointGuardianIntersection_Statics
	{
		struct OculusXRFunctionLibrary_eventGetPointGuardianIntersection_Parms
		{
			FVector Point;
			EOculusXRBoundaryType BoundaryType;
			FOculusXRGuardianTestResult ReturnValue;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Point_MetaData[];
#endif
		static const UECodeGen_Private::FStructPropertyParams NewProp_Point;
		static const UECodeGen_Private::FBytePropertyParams NewProp_BoundaryType_Underlying;
		static const UECodeGen_Private::FEnumPropertyParams NewProp_BoundaryType;
		static const UECodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPointGuardianIntersection_Statics::NewProp_Point_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPointGuardianIntersection_Statics::NewProp_Point = { "Point", nullptr, (EPropertyFlags)0x0010000000000082, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventGetPointGuardianIntersection_Parms, Point), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPointGuardianIntersection_Statics::NewProp_Point_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPointGuardianIntersection_Statics::NewProp_Point_MetaData)) };
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPointGuardianIntersection_Statics::NewProp_BoundaryType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPointGuardianIntersection_Statics::NewProp_BoundaryType = { "BoundaryType", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventGetPointGuardianIntersection_Parms, BoundaryType), Z_Construct_UEnum_OculusXRHMD_EOculusXRBoundaryType, METADATA_PARAMS(nullptr, 0) }; // 938409106
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPointGuardianIntersection_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventGetPointGuardianIntersection_Parms, ReturnValue), Z_Construct_UScriptStruct_FOculusXRGuardianTestResult, METADATA_PARAMS(nullptr, 0) }; // 910032650
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPointGuardianIntersection_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPointGuardianIntersection_Statics::NewProp_Point,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPointGuardianIntersection_Statics::NewProp_BoundaryType_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPointGuardianIntersection_Statics::NewProp_BoundaryType,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPointGuardianIntersection_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPointGuardianIntersection_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary|Guardian" },
		{ "Comment", "/**\n\x09* Get the intersection result between a UE4 coordinate and a guardian boundary\n\x09* @param Point\x09\x09\x09\x09\x09(in) Point in UE space to test against guardian boundaries\n\x09* @param BoundaryType\x09\x09\x09(in) An enum representing the boundary type requested, either Outer Boundary (exact guardian bounds) or PlayArea (rectangle inside the Outer Boundary)\n\x09*/" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "ToolTip", "Get the intersection result between a UE4 coordinate and a guardian boundary\n@param Point                                  (in) Point in UE space to test against guardian boundaries\n@param BoundaryType                   (in) An enum representing the boundary type requested, either Outer Boundary (exact guardian bounds) or PlayArea (rectangle inside the Outer Boundary)" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPointGuardianIntersection_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOculusXRFunctionLibrary, nullptr, "GetPointGuardianIntersection", nullptr, nullptr, sizeof(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPointGuardianIntersection_Statics::OculusXRFunctionLibrary_eventGetPointGuardianIntersection_Parms), Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPointGuardianIntersection_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPointGuardianIntersection_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04822401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPointGuardianIntersection_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPointGuardianIntersection_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPointGuardianIntersection()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPointGuardianIntersection_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPose_Statics
	{
		struct OculusXRFunctionLibrary_eventGetPose_Parms
		{
			FRotator DeviceRotation;
			FVector DevicePosition;
			FVector NeckPosition;
			bool bUseOrienationForPlayerCamera;
			bool bUsePositionForPlayerCamera;
			FVector PositionScale;
		};
		static const UECodeGen_Private::FStructPropertyParams NewProp_DeviceRotation;
		static const UECodeGen_Private::FStructPropertyParams NewProp_DevicePosition;
		static const UECodeGen_Private::FStructPropertyParams NewProp_NeckPosition;
		static void NewProp_bUseOrienationForPlayerCamera_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_bUseOrienationForPlayerCamera;
		static void NewProp_bUsePositionForPlayerCamera_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_bUsePositionForPlayerCamera;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_PositionScale_MetaData[];
#endif
		static const UECodeGen_Private::FStructPropertyParams NewProp_PositionScale;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPose_Statics::NewProp_DeviceRotation = { "DeviceRotation", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventGetPose_Parms, DeviceRotation), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPose_Statics::NewProp_DevicePosition = { "DevicePosition", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventGetPose_Parms, DevicePosition), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPose_Statics::NewProp_NeckPosition = { "NeckPosition", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventGetPose_Parms, NeckPosition), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPose_Statics::NewProp_bUseOrienationForPlayerCamera_SetBit(void* Obj)
	{
		((OculusXRFunctionLibrary_eventGetPose_Parms*)Obj)->bUseOrienationForPlayerCamera = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPose_Statics::NewProp_bUseOrienationForPlayerCamera = { "bUseOrienationForPlayerCamera", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(OculusXRFunctionLibrary_eventGetPose_Parms), &Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPose_Statics::NewProp_bUseOrienationForPlayerCamera_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPose_Statics::NewProp_bUsePositionForPlayerCamera_SetBit(void* Obj)
	{
		((OculusXRFunctionLibrary_eventGetPose_Parms*)Obj)->bUsePositionForPlayerCamera = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPose_Statics::NewProp_bUsePositionForPlayerCamera = { "bUsePositionForPlayerCamera", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(OculusXRFunctionLibrary_eventGetPose_Parms), &Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPose_Statics::NewProp_bUsePositionForPlayerCamera_SetBit, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPose_Statics::NewProp_PositionScale_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPose_Statics::NewProp_PositionScale = { "PositionScale", nullptr, (EPropertyFlags)0x0010000000000082, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventGetPose_Parms, PositionScale), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPose_Statics::NewProp_PositionScale_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPose_Statics::NewProp_PositionScale_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPose_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPose_Statics::NewProp_DeviceRotation,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPose_Statics::NewProp_DevicePosition,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPose_Statics::NewProp_NeckPosition,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPose_Statics::NewProp_bUseOrienationForPlayerCamera,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPose_Statics::NewProp_bUsePositionForPlayerCamera,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPose_Statics::NewProp_PositionScale,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPose_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "Comment", "/**\n\x09 * Grabs the current orientation and position for the HMD.  If positional tracking is not available, DevicePosition will be a zero vector\n\x09 *\n\x09 * @param DeviceRotation\x09(out) The device's current rotation\n\x09 * @param DevicePosition\x09(out) The device's current position, in its own tracking space\n\x09 * @param NeckPosition\x09\x09(out) The estimated neck position, calculated using NeckToEye vector from User Profile. Same coordinate space as DevicePosition.\n\x09 * @param bUseOrienationForPlayerCamera\x09(in) Should be set to 'true' if the orientation is going to be used to update orientation of the camera manually.\n\x09 * @param bUsePositionForPlayerCamera\x09(in) Should be set to 'true' if the position is going to be used to update position of the camera manually.\n\x09 * @param PositionScale\x09\x09(in) The 3D scale that will be applied to position.\n\x09 */" },
		{ "CPP_Default_bUseOrienationForPlayerCamera", "false" },
		{ "CPP_Default_bUsePositionForPlayerCamera", "false" },
		{ "CPP_Default_PositionScale", "" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "ToolTip", "Grabs the current orientation and position for the HMD.  If positional tracking is not available, DevicePosition will be a zero vector\n\n@param DeviceRotation        (out) The device's current rotation\n@param DevicePosition        (out) The device's current position, in its own tracking space\n@param NeckPosition          (out) The estimated neck position, calculated using NeckToEye vector from User Profile. Same coordinate space as DevicePosition.\n@param bUseOrienationForPlayerCamera (in) Should be set to 'true' if the orientation is going to be used to update orientation of the camera manually.\n@param bUsePositionForPlayerCamera   (in) Should be set to 'true' if the position is going to be used to update position of the camera manually.\n@param PositionScale         (in) The 3D scale that will be applied to position." },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPose_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOculusXRFunctionLibrary, nullptr, "GetPose", nullptr, nullptr, sizeof(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPose_Statics::OculusXRFunctionLibrary_eventGetPose_Parms), Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPose_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPose_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14C22401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPose_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPose_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPose()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPose_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOculusXRFunctionLibrary_GetRawSensorData_Statics
	{
		struct OculusXRFunctionLibrary_eventGetRawSensorData_Parms
		{
			FVector AngularAcceleration;
			FVector LinearAcceleration;
			FVector AngularVelocity;
			FVector LinearVelocity;
			float TimeInSeconds;
			EOculusXRTrackedDeviceType DeviceType;
		};
		static const UECodeGen_Private::FStructPropertyParams NewProp_AngularAcceleration;
		static const UECodeGen_Private::FStructPropertyParams NewProp_LinearAcceleration;
		static const UECodeGen_Private::FStructPropertyParams NewProp_AngularVelocity;
		static const UECodeGen_Private::FStructPropertyParams NewProp_LinearVelocity;
		static const UECodeGen_Private::FFloatPropertyParams NewProp_TimeInSeconds;
		static const UECodeGen_Private::FBytePropertyParams NewProp_DeviceType_Underlying;
		static const UECodeGen_Private::FEnumPropertyParams NewProp_DeviceType;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetRawSensorData_Statics::NewProp_AngularAcceleration = { "AngularAcceleration", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventGetRawSensorData_Parms, AngularAcceleration), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetRawSensorData_Statics::NewProp_LinearAcceleration = { "LinearAcceleration", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventGetRawSensorData_Parms, LinearAcceleration), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetRawSensorData_Statics::NewProp_AngularVelocity = { "AngularVelocity", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventGetRawSensorData_Parms, AngularVelocity), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetRawSensorData_Statics::NewProp_LinearVelocity = { "LinearVelocity", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventGetRawSensorData_Parms, LinearVelocity), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetRawSensorData_Statics::NewProp_TimeInSeconds = { "TimeInSeconds", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventGetRawSensorData_Parms, TimeInSeconds), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetRawSensorData_Statics::NewProp_DeviceType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetRawSensorData_Statics::NewProp_DeviceType = { "DeviceType", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventGetRawSensorData_Parms, DeviceType), Z_Construct_UEnum_OculusXRHMD_EOculusXRTrackedDeviceType, METADATA_PARAMS(nullptr, 0) }; // 125253818
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOculusXRFunctionLibrary_GetRawSensorData_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetRawSensorData_Statics::NewProp_AngularAcceleration,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetRawSensorData_Statics::NewProp_LinearAcceleration,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetRawSensorData_Statics::NewProp_AngularVelocity,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetRawSensorData_Statics::NewProp_LinearVelocity,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetRawSensorData_Statics::NewProp_TimeInSeconds,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetRawSensorData_Statics::NewProp_DeviceType_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetRawSensorData_Statics::NewProp_DeviceType,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOculusXRFunctionLibrary_GetRawSensorData_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "Comment", "/**\n\x09* Reports raw sensor data. If HMD doesn't support any of the parameters then it will be set to zero.\n\x09*\n\x09* @param AngularAcceleration\x09(out) Angular acceleration in radians per second per second.\n\x09* @param LinearAcceleration\x09\x09(out) Acceleration in meters per second per second.\n\x09* @param AngularVelocity\x09\x09(out) Angular velocity in radians per second.\n\x09* @param LinearVelocity\x09\x09\x09(out) Velocity in meters per second.\n\x09* @param TimeInSeconds\x09\x09\x09(out) Time when the reported IMU reading took place, in seconds.\n\x09*/" },
		{ "CPP_Default_DeviceType", "HMD" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "ToolTip", "Reports raw sensor data. If HMD doesn't support any of the parameters then it will be set to zero.\n\n@param AngularAcceleration    (out) Angular acceleration in radians per second per second.\n@param LinearAcceleration             (out) Acceleration in meters per second per second.\n@param AngularVelocity                (out) Angular velocity in radians per second.\n@param LinearVelocity                 (out) Velocity in meters per second.\n@param TimeInSeconds                  (out) Time when the reported IMU reading took place, in seconds." },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetRawSensorData_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOculusXRFunctionLibrary, nullptr, "GetRawSensorData", nullptr, nullptr, sizeof(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetRawSensorData_Statics::OculusXRFunctionLibrary_eventGetRawSensorData_Parms), Z_Construct_UFunction_UOculusXRFunctionLibrary_GetRawSensorData_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetRawSensorData_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14C22401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetRawSensorData_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetRawSensorData_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOculusXRFunctionLibrary_GetRawSensorData()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UOculusXRFunctionLibrary_GetRawSensorData_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOculusXRFunctionLibrary_GetSuggestedCpuAndGpuPerformanceLevels_Statics
	{
		struct OculusXRFunctionLibrary_eventGetSuggestedCpuAndGpuPerformanceLevels_Parms
		{
			EOculusXRProcessorPerformanceLevel CpuPerfLevel;
			EOculusXRProcessorPerformanceLevel GpuPerfLevel;
		};
		static const UECodeGen_Private::FBytePropertyParams NewProp_CpuPerfLevel_Underlying;
		static const UECodeGen_Private::FEnumPropertyParams NewProp_CpuPerfLevel;
		static const UECodeGen_Private::FBytePropertyParams NewProp_GpuPerfLevel_Underlying;
		static const UECodeGen_Private::FEnumPropertyParams NewProp_GpuPerfLevel;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetSuggestedCpuAndGpuPerformanceLevels_Statics::NewProp_CpuPerfLevel_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetSuggestedCpuAndGpuPerformanceLevels_Statics::NewProp_CpuPerfLevel = { "CpuPerfLevel", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventGetSuggestedCpuAndGpuPerformanceLevels_Parms, CpuPerfLevel), Z_Construct_UEnum_OculusXRHMD_EOculusXRProcessorPerformanceLevel, METADATA_PARAMS(nullptr, 0) }; // 3891469125
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetSuggestedCpuAndGpuPerformanceLevels_Statics::NewProp_GpuPerfLevel_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetSuggestedCpuAndGpuPerformanceLevels_Statics::NewProp_GpuPerfLevel = { "GpuPerfLevel", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventGetSuggestedCpuAndGpuPerformanceLevels_Parms, GpuPerfLevel), Z_Construct_UEnum_OculusXRHMD_EOculusXRProcessorPerformanceLevel, METADATA_PARAMS(nullptr, 0) }; // 3891469125
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOculusXRFunctionLibrary_GetSuggestedCpuAndGpuPerformanceLevels_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetSuggestedCpuAndGpuPerformanceLevels_Statics::NewProp_CpuPerfLevel_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetSuggestedCpuAndGpuPerformanceLevels_Statics::NewProp_CpuPerfLevel,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetSuggestedCpuAndGpuPerformanceLevels_Statics::NewProp_GpuPerfLevel_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetSuggestedCpuAndGpuPerformanceLevels_Statics::NewProp_GpuPerfLevel,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOculusXRFunctionLibrary_GetSuggestedCpuAndGpuPerformanceLevels_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "Comment", "/**\n\x09* Get the suggested CPU and GPU levels to the Oculus device.\n\x09*/" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "ToolTip", "Get the suggested CPU and GPU levels to the Oculus device." },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetSuggestedCpuAndGpuPerformanceLevels_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOculusXRFunctionLibrary, nullptr, "GetSuggestedCpuAndGpuPerformanceLevels", nullptr, nullptr, sizeof(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetSuggestedCpuAndGpuPerformanceLevels_Statics::OculusXRFunctionLibrary_eventGetSuggestedCpuAndGpuPerformanceLevels_Parms), Z_Construct_UFunction_UOculusXRFunctionLibrary_GetSuggestedCpuAndGpuPerformanceLevels_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetSuggestedCpuAndGpuPerformanceLevels_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetSuggestedCpuAndGpuPerformanceLevels_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetSuggestedCpuAndGpuPerformanceLevels_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOculusXRFunctionLibrary_GetSuggestedCpuAndGpuPerformanceLevels()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UOculusXRFunctionLibrary_GetSuggestedCpuAndGpuPerformanceLevels_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOculusXRFunctionLibrary_GetSystemHmd3DofModeEnabled_Statics
	{
		struct OculusXRFunctionLibrary_eventGetSystemHmd3DofModeEnabled_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UOculusXRFunctionLibrary_GetSystemHmd3DofModeEnabled_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((OculusXRFunctionLibrary_eventGetSystemHmd3DofModeEnabled_Parms*)Obj)->ReturnValue = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetSystemHmd3DofModeEnabled_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(OculusXRFunctionLibrary_eventGetSystemHmd3DofModeEnabled_Parms), &Z_Construct_UFunction_UOculusXRFunctionLibrary_GetSystemHmd3DofModeEnabled_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOculusXRFunctionLibrary_GetSystemHmd3DofModeEnabled_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetSystemHmd3DofModeEnabled_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOculusXRFunctionLibrary_GetSystemHmd3DofModeEnabled_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "Comment", "/**\n\x09* Returns true if system headset is in 3dof mode \n\x09*/" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "ToolTip", "Returns true if system headset is in 3dof mode" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetSystemHmd3DofModeEnabled_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOculusXRFunctionLibrary, nullptr, "GetSystemHmd3DofModeEnabled", nullptr, nullptr, sizeof(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetSystemHmd3DofModeEnabled_Statics::OculusXRFunctionLibrary_eventGetSystemHmd3DofModeEnabled_Parms), Z_Construct_UFunction_UOculusXRFunctionLibrary_GetSystemHmd3DofModeEnabled_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetSystemHmd3DofModeEnabled_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetSystemHmd3DofModeEnabled_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetSystemHmd3DofModeEnabled_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOculusXRFunctionLibrary_GetSystemHmd3DofModeEnabled()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UOculusXRFunctionLibrary_GetSystemHmd3DofModeEnabled_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOculusXRFunctionLibrary_GetUserProfile_Statics
	{
		struct OculusXRFunctionLibrary_eventGetUserProfile_Parms
		{
			FOculusXRHmdUserProfile Profile;
			bool ReturnValue;
		};
		static const UECodeGen_Private::FStructPropertyParams NewProp_Profile;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetUserProfile_Statics::NewProp_Profile = { "Profile", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventGetUserProfile_Parms, Profile), Z_Construct_UScriptStruct_FOculusXRHmdUserProfile, METADATA_PARAMS(nullptr, 0) }; // 2512357364
	void Z_Construct_UFunction_UOculusXRFunctionLibrary_GetUserProfile_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((OculusXRFunctionLibrary_eventGetUserProfile_Parms*)Obj)->ReturnValue = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetUserProfile_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(OculusXRFunctionLibrary_eventGetUserProfile_Parms), &Z_Construct_UFunction_UOculusXRFunctionLibrary_GetUserProfile_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOculusXRFunctionLibrary_GetUserProfile_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetUserProfile_Statics::NewProp_Profile,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_GetUserProfile_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOculusXRFunctionLibrary_GetUserProfile_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "Comment", "/**\n\x09* Returns current user profile.\n\x09*\n\x09* @param Profile\x09\x09(out) Structure to hold current user profile.\n\x09* @return (boolean)\x09True, if user profile was acquired.\n\x09*/" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "ToolTip", "Returns current user profile.\n\n@param Profile                (out) Structure to hold current user profile.\n@return (boolean)     True, if user profile was acquired." },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UOculusXRFunctionLibrary_GetUserProfile_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOculusXRFunctionLibrary, nullptr, "GetUserProfile", nullptr, nullptr, sizeof(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetUserProfile_Statics::OculusXRFunctionLibrary_eventGetUserProfile_Parms), Z_Construct_UFunction_UOculusXRFunctionLibrary_GetUserProfile_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetUserProfile_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetUserProfile_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_GetUserProfile_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOculusXRFunctionLibrary_GetUserProfile()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UOculusXRFunctionLibrary_GetUserProfile_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOculusXRFunctionLibrary_HasInputFocus_Statics
	{
		struct OculusXRFunctionLibrary_eventHasInputFocus_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UOculusXRFunctionLibrary_HasInputFocus_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((OculusXRFunctionLibrary_eventHasInputFocus_Parms*)Obj)->ReturnValue = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_HasInputFocus_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(OculusXRFunctionLibrary_eventHasInputFocus_Parms), &Z_Construct_UFunction_UOculusXRFunctionLibrary_HasInputFocus_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOculusXRFunctionLibrary_HasInputFocus_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_HasInputFocus_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOculusXRFunctionLibrary_HasInputFocus_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "Comment", "/**\n\x09* Returns true, if the app has input focus.\n\x09*/" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "ToolTip", "Returns true, if the app has input focus." },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UOculusXRFunctionLibrary_HasInputFocus_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOculusXRFunctionLibrary, nullptr, "HasInputFocus", nullptr, nullptr, sizeof(Z_Construct_UFunction_UOculusXRFunctionLibrary_HasInputFocus_Statics::OculusXRFunctionLibrary_eventHasInputFocus_Parms), Z_Construct_UFunction_UOculusXRFunctionLibrary_HasInputFocus_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_HasInputFocus_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOculusXRFunctionLibrary_HasInputFocus_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_HasInputFocus_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOculusXRFunctionLibrary_HasInputFocus()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UOculusXRFunctionLibrary_HasInputFocus_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOculusXRFunctionLibrary_HasSystemOverlayPresent_Statics
	{
		struct OculusXRFunctionLibrary_eventHasSystemOverlayPresent_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UOculusXRFunctionLibrary_HasSystemOverlayPresent_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((OculusXRFunctionLibrary_eventHasSystemOverlayPresent_Parms*)Obj)->ReturnValue = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_HasSystemOverlayPresent_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(OculusXRFunctionLibrary_eventHasSystemOverlayPresent_Parms), &Z_Construct_UFunction_UOculusXRFunctionLibrary_HasSystemOverlayPresent_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOculusXRFunctionLibrary_HasSystemOverlayPresent_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_HasSystemOverlayPresent_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOculusXRFunctionLibrary_HasSystemOverlayPresent_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "Comment", "/**\n\x09* Returns true, if the system overlay is present.\n\x09*/" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "ToolTip", "Returns true, if the system overlay is present." },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UOculusXRFunctionLibrary_HasSystemOverlayPresent_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOculusXRFunctionLibrary, nullptr, "HasSystemOverlayPresent", nullptr, nullptr, sizeof(Z_Construct_UFunction_UOculusXRFunctionLibrary_HasSystemOverlayPresent_Statics::OculusXRFunctionLibrary_eventHasSystemOverlayPresent_Parms), Z_Construct_UFunction_UOculusXRFunctionLibrary_HasSystemOverlayPresent_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_HasSystemOverlayPresent_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOculusXRFunctionLibrary_HasSystemOverlayPresent_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_HasSystemOverlayPresent_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOculusXRFunctionLibrary_HasSystemOverlayPresent()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UOculusXRFunctionLibrary_HasSystemOverlayPresent_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOculusXRFunctionLibrary_IsColorPassthroughSupported_Statics
	{
		struct OculusXRFunctionLibrary_eventIsColorPassthroughSupported_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UOculusXRFunctionLibrary_IsColorPassthroughSupported_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((OculusXRFunctionLibrary_eventIsColorPassthroughSupported_Parms*)Obj)->ReturnValue = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_IsColorPassthroughSupported_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(OculusXRFunctionLibrary_eventIsColorPassthroughSupported_Parms), &Z_Construct_UFunction_UOculusXRFunctionLibrary_IsColorPassthroughSupported_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOculusXRFunctionLibrary_IsColorPassthroughSupported_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_IsColorPassthroughSupported_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOculusXRFunctionLibrary_IsColorPassthroughSupported_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "Comment", "/**\n\x09* Checks if color passthrough is supported\n\x09*/" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "ToolTip", "Checks if color passthrough is supported" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UOculusXRFunctionLibrary_IsColorPassthroughSupported_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOculusXRFunctionLibrary, nullptr, "IsColorPassthroughSupported", nullptr, nullptr, sizeof(Z_Construct_UFunction_UOculusXRFunctionLibrary_IsColorPassthroughSupported_Statics::OculusXRFunctionLibrary_eventIsColorPassthroughSupported_Parms), Z_Construct_UFunction_UOculusXRFunctionLibrary_IsColorPassthroughSupported_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_IsColorPassthroughSupported_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOculusXRFunctionLibrary_IsColorPassthroughSupported_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_IsColorPassthroughSupported_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOculusXRFunctionLibrary_IsColorPassthroughSupported()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UOculusXRFunctionLibrary_IsColorPassthroughSupported_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOculusXRFunctionLibrary_IsDeviceTracked_Statics
	{
		struct OculusXRFunctionLibrary_eventIsDeviceTracked_Parms
		{
			EOculusXRTrackedDeviceType DeviceType;
			bool ReturnValue;
		};
		static const UECodeGen_Private::FBytePropertyParams NewProp_DeviceType_Underlying;
		static const UECodeGen_Private::FEnumPropertyParams NewProp_DeviceType;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_IsDeviceTracked_Statics::NewProp_DeviceType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_IsDeviceTracked_Statics::NewProp_DeviceType = { "DeviceType", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventIsDeviceTracked_Parms, DeviceType), Z_Construct_UEnum_OculusXRHMD_EOculusXRTrackedDeviceType, METADATA_PARAMS(nullptr, 0) }; // 125253818
	void Z_Construct_UFunction_UOculusXRFunctionLibrary_IsDeviceTracked_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((OculusXRFunctionLibrary_eventIsDeviceTracked_Parms*)Obj)->ReturnValue = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_IsDeviceTracked_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(OculusXRFunctionLibrary_eventIsDeviceTracked_Parms), &Z_Construct_UFunction_UOculusXRFunctionLibrary_IsDeviceTracked_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOculusXRFunctionLibrary_IsDeviceTracked_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_IsDeviceTracked_Statics::NewProp_DeviceType_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_IsDeviceTracked_Statics::NewProp_DeviceType,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_IsDeviceTracked_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOculusXRFunctionLibrary_IsDeviceTracked_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "Comment", "/**\n\x09* Returns if the device is currently tracked by the runtime or not.\n\x09*/" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "ToolTip", "Returns if the device is currently tracked by the runtime or not." },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UOculusXRFunctionLibrary_IsDeviceTracked_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOculusXRFunctionLibrary, nullptr, "IsDeviceTracked", nullptr, nullptr, sizeof(Z_Construct_UFunction_UOculusXRFunctionLibrary_IsDeviceTracked_Statics::OculusXRFunctionLibrary_eventIsDeviceTracked_Parms), Z_Construct_UFunction_UOculusXRFunctionLibrary_IsDeviceTracked_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_IsDeviceTracked_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOculusXRFunctionLibrary_IsDeviceTracked_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_IsDeviceTracked_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOculusXRFunctionLibrary_IsDeviceTracked()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UOculusXRFunctionLibrary_IsDeviceTracked_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOculusXRFunctionLibrary_IsGuardianConfigured_Statics
	{
		struct OculusXRFunctionLibrary_eventIsGuardianConfigured_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UOculusXRFunctionLibrary_IsGuardianConfigured_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((OculusXRFunctionLibrary_eventIsGuardianConfigured_Parms*)Obj)->ReturnValue = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_IsGuardianConfigured_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(OculusXRFunctionLibrary_eventIsGuardianConfigured_Parms), &Z_Construct_UFunction_UOculusXRFunctionLibrary_IsGuardianConfigured_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOculusXRFunctionLibrary_IsGuardianConfigured_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_IsGuardianConfigured_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOculusXRFunctionLibrary_IsGuardianConfigured_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary|Guardian" },
		{ "Comment", "/**\n\x09* Returns true if the Guardian has been set up by the user, false if the user is in \"seated\" mode and has not set up a play space.\n\x09*/" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "ToolTip", "Returns true if the Guardian has been set up by the user, false if the user is in \"seated\" mode and has not set up a play space." },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UOculusXRFunctionLibrary_IsGuardianConfigured_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOculusXRFunctionLibrary, nullptr, "IsGuardianConfigured", nullptr, nullptr, sizeof(Z_Construct_UFunction_UOculusXRFunctionLibrary_IsGuardianConfigured_Statics::OculusXRFunctionLibrary_eventIsGuardianConfigured_Parms), Z_Construct_UFunction_UOculusXRFunctionLibrary_IsGuardianConfigured_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_IsGuardianConfigured_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOculusXRFunctionLibrary_IsGuardianConfigured_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_IsGuardianConfigured_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOculusXRFunctionLibrary_IsGuardianConfigured()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UOculusXRFunctionLibrary_IsGuardianConfigured_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOculusXRFunctionLibrary_IsGuardianDisplayed_Statics
	{
		struct OculusXRFunctionLibrary_eventIsGuardianDisplayed_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UOculusXRFunctionLibrary_IsGuardianDisplayed_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((OculusXRFunctionLibrary_eventIsGuardianDisplayed_Parms*)Obj)->ReturnValue = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_IsGuardianDisplayed_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(OculusXRFunctionLibrary_eventIsGuardianDisplayed_Parms), &Z_Construct_UFunction_UOculusXRFunctionLibrary_IsGuardianDisplayed_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOculusXRFunctionLibrary_IsGuardianDisplayed_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_IsGuardianDisplayed_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOculusXRFunctionLibrary_IsGuardianDisplayed_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary|Guardian" },
		{ "Comment", "/**\n\x09* Returns true if the Guardian Outer Boundary is being displayed\n\x09*/" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "ToolTip", "Returns true if the Guardian Outer Boundary is being displayed" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UOculusXRFunctionLibrary_IsGuardianDisplayed_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOculusXRFunctionLibrary, nullptr, "IsGuardianDisplayed", nullptr, nullptr, sizeof(Z_Construct_UFunction_UOculusXRFunctionLibrary_IsGuardianDisplayed_Statics::OculusXRFunctionLibrary_eventIsGuardianDisplayed_Parms), Z_Construct_UFunction_UOculusXRFunctionLibrary_IsGuardianDisplayed_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_IsGuardianDisplayed_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOculusXRFunctionLibrary_IsGuardianDisplayed_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_IsGuardianDisplayed_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOculusXRFunctionLibrary_IsGuardianDisplayed()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UOculusXRFunctionLibrary_IsGuardianDisplayed_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOculusXRFunctionLibrary_IsPassthroughSupported_Statics
	{
		struct OculusXRFunctionLibrary_eventIsPassthroughSupported_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UOculusXRFunctionLibrary_IsPassthroughSupported_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((OculusXRFunctionLibrary_eventIsPassthroughSupported_Parms*)Obj)->ReturnValue = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_IsPassthroughSupported_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(OculusXRFunctionLibrary_eventIsPassthroughSupported_Parms), &Z_Construct_UFunction_UOculusXRFunctionLibrary_IsPassthroughSupported_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOculusXRFunctionLibrary_IsPassthroughSupported_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_IsPassthroughSupported_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOculusXRFunctionLibrary_IsPassthroughSupported_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "Comment", "/**\n\x09* Checks if passthrough is supported\n\x09*/" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "ToolTip", "Checks if passthrough is supported" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UOculusXRFunctionLibrary_IsPassthroughSupported_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOculusXRFunctionLibrary, nullptr, "IsPassthroughSupported", nullptr, nullptr, sizeof(Z_Construct_UFunction_UOculusXRFunctionLibrary_IsPassthroughSupported_Statics::OculusXRFunctionLibrary_eventIsPassthroughSupported_Parms), Z_Construct_UFunction_UOculusXRFunctionLibrary_IsPassthroughSupported_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_IsPassthroughSupported_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOculusXRFunctionLibrary_IsPassthroughSupported_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_IsPassthroughSupported_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOculusXRFunctionLibrary_IsPassthroughSupported()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UOculusXRFunctionLibrary_IsPassthroughSupported_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOculusXRFunctionLibrary_SetBaseRotationAndBaseOffsetInMeters_Statics
	{
		struct OculusXRFunctionLibrary_eventSetBaseRotationAndBaseOffsetInMeters_Parms
		{
			FRotator Rotation;
			FVector BaseOffsetInMeters;
			TEnumAsByte<EOrientPositionSelector::Type> Options;
		};
		static const UECodeGen_Private::FStructPropertyParams NewProp_Rotation;
		static const UECodeGen_Private::FStructPropertyParams NewProp_BaseOffsetInMeters;
		static const UECodeGen_Private::FBytePropertyParams NewProp_Options;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_SetBaseRotationAndBaseOffsetInMeters_Statics::NewProp_Rotation = { "Rotation", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventSetBaseRotationAndBaseOffsetInMeters_Parms, Rotation), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_SetBaseRotationAndBaseOffsetInMeters_Statics::NewProp_BaseOffsetInMeters = { "BaseOffsetInMeters", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventSetBaseRotationAndBaseOffsetInMeters_Parms, BaseOffsetInMeters), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_SetBaseRotationAndBaseOffsetInMeters_Statics::NewProp_Options = { "Options", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventSetBaseRotationAndBaseOffsetInMeters_Parms, Options), Z_Construct_UEnum_HeadMountedDisplay_EOrientPositionSelector, METADATA_PARAMS(nullptr, 0) }; // 3931628698
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOculusXRFunctionLibrary_SetBaseRotationAndBaseOffsetInMeters_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_SetBaseRotationAndBaseOffsetInMeters_Statics::NewProp_Rotation,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_SetBaseRotationAndBaseOffsetInMeters_Statics::NewProp_BaseOffsetInMeters,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_SetBaseRotationAndBaseOffsetInMeters_Statics::NewProp_Options,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOculusXRFunctionLibrary_SetBaseRotationAndBaseOffsetInMeters_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "Comment", "/**\n\x09* Sets 'base rotation' - the rotation that will be subtracted from\n\x09* the actual HMD orientation.\n\x09* Sets base position offset (in meters). The base position offset is the distance from the physical (0, 0, 0) position\n\x09* to current HMD position (bringing the (0, 0, 0) point to the current HMD position)\n\x09* Note, this vector is set by ResetPosition call; use this method with care.\n\x09* The axis of the vector are the same as in Unreal: X - forward, Y - right, Z - up.\n\x09*\n\x09* @param Rotation\x09\x09\x09(in) Rotator object with base rotation\n\x09* @param BaseOffsetInMeters (in) the vector to be set as base offset, in meters.\n\x09* @param Options\x09\x09\x09(in) specifies either position, orientation or both should be set.\n\x09*/" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "ToolTip", "Sets 'base rotation' - the rotation that will be subtracted from\nthe actual HMD orientation.\nSets base position offset (in meters). The base position offset is the distance from the physical (0, 0, 0) position\nto current HMD position (bringing the (0, 0, 0) point to the current HMD position)\nNote, this vector is set by ResetPosition call; use this method with care.\nThe axis of the vector are the same as in Unreal: X - forward, Y - right, Z - up.\n\n@param Rotation                       (in) Rotator object with base rotation\n@param BaseOffsetInMeters (in) the vector to be set as base offset, in meters.\n@param Options                        (in) specifies either position, orientation or both should be set." },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UOculusXRFunctionLibrary_SetBaseRotationAndBaseOffsetInMeters_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOculusXRFunctionLibrary, nullptr, "SetBaseRotationAndBaseOffsetInMeters", nullptr, nullptr, sizeof(Z_Construct_UFunction_UOculusXRFunctionLibrary_SetBaseRotationAndBaseOffsetInMeters_Statics::OculusXRFunctionLibrary_eventSetBaseRotationAndBaseOffsetInMeters_Parms), Z_Construct_UFunction_UOculusXRFunctionLibrary_SetBaseRotationAndBaseOffsetInMeters_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_SetBaseRotationAndBaseOffsetInMeters_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04822401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOculusXRFunctionLibrary_SetBaseRotationAndBaseOffsetInMeters_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_SetBaseRotationAndBaseOffsetInMeters_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOculusXRFunctionLibrary_SetBaseRotationAndBaseOffsetInMeters()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UOculusXRFunctionLibrary_SetBaseRotationAndBaseOffsetInMeters_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOculusXRFunctionLibrary_SetBaseRotationAndPositionOffset_Statics
	{
		struct OculusXRFunctionLibrary_eventSetBaseRotationAndPositionOffset_Parms
		{
			FRotator BaseRot;
			FVector PosOffset;
			TEnumAsByte<EOrientPositionSelector::Type> Options;
		};
		static const UECodeGen_Private::FStructPropertyParams NewProp_BaseRot;
		static const UECodeGen_Private::FStructPropertyParams NewProp_PosOffset;
		static const UECodeGen_Private::FBytePropertyParams NewProp_Options;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_SetBaseRotationAndPositionOffset_Statics::NewProp_BaseRot = { "BaseRot", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventSetBaseRotationAndPositionOffset_Parms, BaseRot), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_SetBaseRotationAndPositionOffset_Statics::NewProp_PosOffset = { "PosOffset", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventSetBaseRotationAndPositionOffset_Parms, PosOffset), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_SetBaseRotationAndPositionOffset_Statics::NewProp_Options = { "Options", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventSetBaseRotationAndPositionOffset_Parms, Options), Z_Construct_UEnum_HeadMountedDisplay_EOrientPositionSelector, METADATA_PARAMS(nullptr, 0) }; // 3931628698
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOculusXRFunctionLibrary_SetBaseRotationAndPositionOffset_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_SetBaseRotationAndPositionOffset_Statics::NewProp_BaseRot,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_SetBaseRotationAndPositionOffset_Statics::NewProp_PosOffset,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_SetBaseRotationAndPositionOffset_Statics::NewProp_Options,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOculusXRFunctionLibrary_SetBaseRotationAndPositionOffset_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "Comment", "/**\n\x09 * Sets 'base rotation' - the rotation that will be subtracted from\n\x09 * the actual HMD orientation.\n\x09 * The position offset might be added to current HMD position,\n\x09 * effectively moving the virtual camera by the specified offset. The addition\n\x09 * occurs after the HMD orientation and position are applied.\n\x09 *\n\x09 * @param BaseRot\x09\x09\x09(in) Rotator object with base rotation\n\x09 * @param PosOffset\x09\x09\x09(in) the vector to be added to HMD position.\n\x09 * @param Options\x09\x09\x09(in) specifies either position, orientation or both should be set.\n\x09 */" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "A hack, proper camera positioning should be used" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "ToolTip", "Sets 'base rotation' - the rotation that will be subtracted from\nthe actual HMD orientation.\nThe position offset might be added to current HMD position,\neffectively moving the virtual camera by the specified offset. The addition\noccurs after the HMD orientation and position are applied.\n\n@param BaseRot                       (in) Rotator object with base rotation\n@param PosOffset                     (in) the vector to be added to HMD position.\n@param Options                       (in) specifies either position, orientation or both should be set." },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UOculusXRFunctionLibrary_SetBaseRotationAndPositionOffset_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOculusXRFunctionLibrary, nullptr, "SetBaseRotationAndPositionOffset", nullptr, nullptr, sizeof(Z_Construct_UFunction_UOculusXRFunctionLibrary_SetBaseRotationAndPositionOffset_Statics::OculusXRFunctionLibrary_eventSetBaseRotationAndPositionOffset_Parms), Z_Construct_UFunction_UOculusXRFunctionLibrary_SetBaseRotationAndPositionOffset_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_SetBaseRotationAndPositionOffset_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04822401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOculusXRFunctionLibrary_SetBaseRotationAndPositionOffset_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_SetBaseRotationAndPositionOffset_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOculusXRFunctionLibrary_SetBaseRotationAndPositionOffset()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UOculusXRFunctionLibrary_SetBaseRotationAndPositionOffset_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOculusXRFunctionLibrary_SetClientColorDesc_Statics
	{
		struct OculusXRFunctionLibrary_eventSetClientColorDesc_Parms
		{
			EOculusXRColorSpace ColorSpace;
		};
		static const UECodeGen_Private::FBytePropertyParams NewProp_ColorSpace_Underlying;
		static const UECodeGen_Private::FEnumPropertyParams NewProp_ColorSpace;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_SetClientColorDesc_Statics::NewProp_ColorSpace_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_SetClientColorDesc_Statics::NewProp_ColorSpace = { "ColorSpace", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventSetClientColorDesc_Parms, ColorSpace), Z_Construct_UEnum_OculusXRHMD_EOculusXRColorSpace, METADATA_PARAMS(nullptr, 0) }; // 2189630421
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOculusXRFunctionLibrary_SetClientColorDesc_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_SetClientColorDesc_Statics::NewProp_ColorSpace_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_SetClientColorDesc_Statics::NewProp_ColorSpace,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOculusXRFunctionLibrary_SetClientColorDesc_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "Comment", "/**\n\x09* Sets the target HMD to do color space correction to a specific color space\n\x09*/" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "ToolTip", "Sets the target HMD to do color space correction to a specific color space" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UOculusXRFunctionLibrary_SetClientColorDesc_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOculusXRFunctionLibrary, nullptr, "SetClientColorDesc", nullptr, nullptr, sizeof(Z_Construct_UFunction_UOculusXRFunctionLibrary_SetClientColorDesc_Statics::OculusXRFunctionLibrary_eventSetClientColorDesc_Parms), Z_Construct_UFunction_UOculusXRFunctionLibrary_SetClientColorDesc_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_SetClientColorDesc_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOculusXRFunctionLibrary_SetClientColorDesc_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_SetClientColorDesc_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOculusXRFunctionLibrary_SetClientColorDesc()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UOculusXRFunctionLibrary_SetClientColorDesc_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOculusXRFunctionLibrary_SetColorScaleAndOffset_Statics
	{
		struct OculusXRFunctionLibrary_eventSetColorScaleAndOffset_Parms
		{
			FLinearColor ColorScale;
			FLinearColor ColorOffset;
			bool bApplyToAllLayers;
		};
		static const UECodeGen_Private::FStructPropertyParams NewProp_ColorScale;
		static const UECodeGen_Private::FStructPropertyParams NewProp_ColorOffset;
		static void NewProp_bApplyToAllLayers_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_bApplyToAllLayers;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_SetColorScaleAndOffset_Statics::NewProp_ColorScale = { "ColorScale", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventSetColorScaleAndOffset_Parms, ColorScale), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_SetColorScaleAndOffset_Statics::NewProp_ColorOffset = { "ColorOffset", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventSetColorScaleAndOffset_Parms, ColorOffset), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UOculusXRFunctionLibrary_SetColorScaleAndOffset_Statics::NewProp_bApplyToAllLayers_SetBit(void* Obj)
	{
		((OculusXRFunctionLibrary_eventSetColorScaleAndOffset_Parms*)Obj)->bApplyToAllLayers = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_SetColorScaleAndOffset_Statics::NewProp_bApplyToAllLayers = { "bApplyToAllLayers", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(OculusXRFunctionLibrary_eventSetColorScaleAndOffset_Parms), &Z_Construct_UFunction_UOculusXRFunctionLibrary_SetColorScaleAndOffset_Statics::NewProp_bApplyToAllLayers_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOculusXRFunctionLibrary_SetColorScaleAndOffset_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_SetColorScaleAndOffset_Statics::NewProp_ColorScale,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_SetColorScaleAndOffset_Statics::NewProp_ColorOffset,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_SetColorScaleAndOffset_Statics::NewProp_bApplyToAllLayers,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOculusXRFunctionLibrary_SetColorScaleAndOffset_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "Comment", "/**\n\x09* Set the Color Scale/Offset\n\x09*/" },
		{ "CPP_Default_bApplyToAllLayers", "false" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "ToolTip", "Set the Color Scale/Offset" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UOculusXRFunctionLibrary_SetColorScaleAndOffset_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOculusXRFunctionLibrary, nullptr, "SetColorScaleAndOffset", nullptr, nullptr, sizeof(Z_Construct_UFunction_UOculusXRFunctionLibrary_SetColorScaleAndOffset_Statics::OculusXRFunctionLibrary_eventSetColorScaleAndOffset_Parms), Z_Construct_UFunction_UOculusXRFunctionLibrary_SetColorScaleAndOffset_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_SetColorScaleAndOffset_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04822401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOculusXRFunctionLibrary_SetColorScaleAndOffset_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_SetColorScaleAndOffset_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOculusXRFunctionLibrary_SetColorScaleAndOffset()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UOculusXRFunctionLibrary_SetColorScaleAndOffset_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOculusXRFunctionLibrary_SetCPUAndGPULevels_Statics
	{
		struct OculusXRFunctionLibrary_eventSetCPUAndGPULevels_Parms
		{
			int32 CPULevel;
			int32 GPULevel;
		};
		static const UECodeGen_Private::FUnsizedIntPropertyParams NewProp_CPULevel;
		static const UECodeGen_Private::FUnsizedIntPropertyParams NewProp_GPULevel;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_SetCPUAndGPULevels_Statics::NewProp_CPULevel = { "CPULevel", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventSetCPUAndGPULevels_Parms, CPULevel), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_SetCPUAndGPULevels_Statics::NewProp_GPULevel = { "GPULevel", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventSetCPUAndGPULevels_Parms, GPULevel), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOculusXRFunctionLibrary_SetCPUAndGPULevels_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_SetCPUAndGPULevels_Statics::NewProp_CPULevel,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_SetCPUAndGPULevels_Statics::NewProp_GPULevel,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOculusXRFunctionLibrary_SetCPUAndGPULevels_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "Comment", "/**\n\x09* Set the CPU and GPU levels as hints to the Oculus device (Deprecated).\n\x09*/" },
		{ "DeprecatedFunction", "" },
		{ "DeprecatedMessage", "Deprecated. Please use Get/SetSuggestedCpuAndGpuPerformanceLevels instead" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "ToolTip", "Set the CPU and GPU levels as hints to the Oculus device (Deprecated)." },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UOculusXRFunctionLibrary_SetCPUAndGPULevels_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOculusXRFunctionLibrary, nullptr, "SetCPUAndGPULevels", nullptr, nullptr, sizeof(Z_Construct_UFunction_UOculusXRFunctionLibrary_SetCPUAndGPULevels_Statics::OculusXRFunctionLibrary_eventSetCPUAndGPULevels_Parms), Z_Construct_UFunction_UOculusXRFunctionLibrary_SetCPUAndGPULevels_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_SetCPUAndGPULevels_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOculusXRFunctionLibrary_SetCPUAndGPULevels_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_SetCPUAndGPULevels_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOculusXRFunctionLibrary_SetCPUAndGPULevels()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UOculusXRFunctionLibrary_SetCPUAndGPULevels_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOculusXRFunctionLibrary_SetDisplayFrequency_Statics
	{
		struct OculusXRFunctionLibrary_eventSetDisplayFrequency_Parms
		{
			float RequestedFrequency;
		};
		static const UECodeGen_Private::FFloatPropertyParams NewProp_RequestedFrequency;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_SetDisplayFrequency_Statics::NewProp_RequestedFrequency = { "RequestedFrequency", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventSetDisplayFrequency_Parms, RequestedFrequency), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOculusXRFunctionLibrary_SetDisplayFrequency_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_SetDisplayFrequency_Statics::NewProp_RequestedFrequency,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOculusXRFunctionLibrary_SetDisplayFrequency_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "Comment", "/**\n\x09* Sets the requested display frequency\n\x09*/" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "ToolTip", "Sets the requested display frequency" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UOculusXRFunctionLibrary_SetDisplayFrequency_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOculusXRFunctionLibrary, nullptr, "SetDisplayFrequency", nullptr, nullptr, sizeof(Z_Construct_UFunction_UOculusXRFunctionLibrary_SetDisplayFrequency_Statics::OculusXRFunctionLibrary_eventSetDisplayFrequency_Parms), Z_Construct_UFunction_UOculusXRFunctionLibrary_SetDisplayFrequency_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_SetDisplayFrequency_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOculusXRFunctionLibrary_SetDisplayFrequency_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_SetDisplayFrequency_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOculusXRFunctionLibrary_SetDisplayFrequency()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UOculusXRFunctionLibrary_SetDisplayFrequency_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOculusXRFunctionLibrary_SetFoveatedRenderingLevel_Statics
	{
		struct OculusXRFunctionLibrary_eventSetFoveatedRenderingLevel_Parms
		{
			EOculusXRFoveatedRenderingLevel level;
			bool isDynamic;
		};
		static const UECodeGen_Private::FBytePropertyParams NewProp_level_Underlying;
		static const UECodeGen_Private::FEnumPropertyParams NewProp_level;
		static void NewProp_isDynamic_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_isDynamic;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_SetFoveatedRenderingLevel_Statics::NewProp_level_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_SetFoveatedRenderingLevel_Statics::NewProp_level = { "level", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventSetFoveatedRenderingLevel_Parms, level), Z_Construct_UEnum_OculusXRHMD_EOculusXRFoveatedRenderingLevel, METADATA_PARAMS(nullptr, 0) }; // 37067298
	void Z_Construct_UFunction_UOculusXRFunctionLibrary_SetFoveatedRenderingLevel_Statics::NewProp_isDynamic_SetBit(void* Obj)
	{
		((OculusXRFunctionLibrary_eventSetFoveatedRenderingLevel_Parms*)Obj)->isDynamic = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_SetFoveatedRenderingLevel_Statics::NewProp_isDynamic = { "isDynamic", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(OculusXRFunctionLibrary_eventSetFoveatedRenderingLevel_Parms), &Z_Construct_UFunction_UOculusXRFunctionLibrary_SetFoveatedRenderingLevel_Statics::NewProp_isDynamic_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOculusXRFunctionLibrary_SetFoveatedRenderingLevel_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_SetFoveatedRenderingLevel_Statics::NewProp_level_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_SetFoveatedRenderingLevel_Statics::NewProp_level,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_SetFoveatedRenderingLevel_Statics::NewProp_isDynamic,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOculusXRFunctionLibrary_SetFoveatedRenderingLevel_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "Comment", "/**\n\x09* Set the requested foveated rendering level for the next frame, and whether FFR's level is now dynamic or not.\n\x09*/" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "ToolTip", "Set the requested foveated rendering level for the next frame, and whether FFR's level is now dynamic or not." },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UOculusXRFunctionLibrary_SetFoveatedRenderingLevel_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOculusXRFunctionLibrary, nullptr, "SetFoveatedRenderingLevel", nullptr, nullptr, sizeof(Z_Construct_UFunction_UOculusXRFunctionLibrary_SetFoveatedRenderingLevel_Statics::OculusXRFunctionLibrary_eventSetFoveatedRenderingLevel_Parms), Z_Construct_UFunction_UOculusXRFunctionLibrary_SetFoveatedRenderingLevel_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_SetFoveatedRenderingLevel_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOculusXRFunctionLibrary_SetFoveatedRenderingLevel_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_SetFoveatedRenderingLevel_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOculusXRFunctionLibrary_SetFoveatedRenderingLevel()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UOculusXRFunctionLibrary_SetFoveatedRenderingLevel_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOculusXRFunctionLibrary_SetFoveatedRenderingMethod_Statics
	{
		struct OculusXRFunctionLibrary_eventSetFoveatedRenderingMethod_Parms
		{
			EOculusXRFoveatedRenderingMethod Method;
		};
		static const UECodeGen_Private::FBytePropertyParams NewProp_Method_Underlying;
		static const UECodeGen_Private::FEnumPropertyParams NewProp_Method;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_SetFoveatedRenderingMethod_Statics::NewProp_Method_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_SetFoveatedRenderingMethod_Statics::NewProp_Method = { "Method", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventSetFoveatedRenderingMethod_Parms, Method), Z_Construct_UEnum_OculusXRHMD_EOculusXRFoveatedRenderingMethod, METADATA_PARAMS(nullptr, 0) }; // 709529947
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOculusXRFunctionLibrary_SetFoveatedRenderingMethod_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_SetFoveatedRenderingMethod_Statics::NewProp_Method_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_SetFoveatedRenderingMethod_Statics::NewProp_Method,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOculusXRFunctionLibrary_SetFoveatedRenderingMethod_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "Comment", "/**\n\x09* Set the requested foveated rendering method\n\x09*/" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "ToolTip", "Set the requested foveated rendering method" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UOculusXRFunctionLibrary_SetFoveatedRenderingMethod_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOculusXRFunctionLibrary, nullptr, "SetFoveatedRenderingMethod", nullptr, nullptr, sizeof(Z_Construct_UFunction_UOculusXRFunctionLibrary_SetFoveatedRenderingMethod_Statics::OculusXRFunctionLibrary_eventSetFoveatedRenderingMethod_Parms), Z_Construct_UFunction_UOculusXRFunctionLibrary_SetFoveatedRenderingMethod_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_SetFoveatedRenderingMethod_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOculusXRFunctionLibrary_SetFoveatedRenderingMethod_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_SetFoveatedRenderingMethod_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOculusXRFunctionLibrary_SetFoveatedRenderingMethod()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UOculusXRFunctionLibrary_SetFoveatedRenderingMethod_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOculusXRFunctionLibrary_SetGuardianVisibility_Statics
	{
		struct OculusXRFunctionLibrary_eventSetGuardianVisibility_Parms
		{
			bool GuardianVisible;
		};
		static void NewProp_GuardianVisible_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_GuardianVisible;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UOculusXRFunctionLibrary_SetGuardianVisibility_Statics::NewProp_GuardianVisible_SetBit(void* Obj)
	{
		((OculusXRFunctionLibrary_eventSetGuardianVisibility_Parms*)Obj)->GuardianVisible = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_SetGuardianVisibility_Statics::NewProp_GuardianVisible = { "GuardianVisible", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(OculusXRFunctionLibrary_eventSetGuardianVisibility_Parms), &Z_Construct_UFunction_UOculusXRFunctionLibrary_SetGuardianVisibility_Statics::NewProp_GuardianVisible_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOculusXRFunctionLibrary_SetGuardianVisibility_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_SetGuardianVisibility_Statics::NewProp_GuardianVisible,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOculusXRFunctionLibrary_SetGuardianVisibility_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary|Guardian" },
		{ "Comment", "/**\n\x09* Forces the runtime to render guardian at all times or not\n\x09* @param GuardianVisible\x09\x09\x09(in) True will display guardian, False will hide it\n\x09*/" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "ToolTip", "Forces the runtime to render guardian at all times or not\n@param GuardianVisible                        (in) True will display guardian, False will hide it" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UOculusXRFunctionLibrary_SetGuardianVisibility_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOculusXRFunctionLibrary, nullptr, "SetGuardianVisibility", nullptr, nullptr, sizeof(Z_Construct_UFunction_UOculusXRFunctionLibrary_SetGuardianVisibility_Statics::OculusXRFunctionLibrary_eventSetGuardianVisibility_Parms), Z_Construct_UFunction_UOculusXRFunctionLibrary_SetGuardianVisibility_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_SetGuardianVisibility_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOculusXRFunctionLibrary_SetGuardianVisibility_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_SetGuardianVisibility_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOculusXRFunctionLibrary_SetGuardianVisibility()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UOculusXRFunctionLibrary_SetGuardianVisibility_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOculusXRFunctionLibrary_SetLocalDimmingOn_Statics
	{
		struct OculusXRFunctionLibrary_eventSetLocalDimmingOn_Parms
		{
			bool LocalDimmingOn;
		};
		static void NewProp_LocalDimmingOn_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_LocalDimmingOn;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UOculusXRFunctionLibrary_SetLocalDimmingOn_Statics::NewProp_LocalDimmingOn_SetBit(void* Obj)
	{
		((OculusXRFunctionLibrary_eventSetLocalDimmingOn_Parms*)Obj)->LocalDimmingOn = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_SetLocalDimmingOn_Statics::NewProp_LocalDimmingOn = { "LocalDimmingOn", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(OculusXRFunctionLibrary_eventSetLocalDimmingOn_Parms), &Z_Construct_UFunction_UOculusXRFunctionLibrary_SetLocalDimmingOn_Statics::NewProp_LocalDimmingOn_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOculusXRFunctionLibrary_SetLocalDimmingOn_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_SetLocalDimmingOn_Statics::NewProp_LocalDimmingOn,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOculusXRFunctionLibrary_SetLocalDimmingOn_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "Comment", "/**\n\x09* Turns on or off local dimming\n\x09*/" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "ToolTip", "Turns on or off local dimming" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UOculusXRFunctionLibrary_SetLocalDimmingOn_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOculusXRFunctionLibrary, nullptr, "SetLocalDimmingOn", nullptr, nullptr, sizeof(Z_Construct_UFunction_UOculusXRFunctionLibrary_SetLocalDimmingOn_Statics::OculusXRFunctionLibrary_eventSetLocalDimmingOn_Parms), Z_Construct_UFunction_UOculusXRFunctionLibrary_SetLocalDimmingOn_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_SetLocalDimmingOn_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOculusXRFunctionLibrary_SetLocalDimmingOn_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_SetLocalDimmingOn_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOculusXRFunctionLibrary_SetLocalDimmingOn()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UOculusXRFunctionLibrary_SetLocalDimmingOn_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOculusXRFunctionLibrary_SetPositionScale3D_Statics
	{
		struct OculusXRFunctionLibrary_eventSetPositionScale3D_Parms
		{
			FVector PosScale3D;
		};
		static const UECodeGen_Private::FStructPropertyParams NewProp_PosScale3D;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_SetPositionScale3D_Statics::NewProp_PosScale3D = { "PosScale3D", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventSetPositionScale3D_Parms, PosScale3D), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOculusXRFunctionLibrary_SetPositionScale3D_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_SetPositionScale3D_Statics::NewProp_PosScale3D,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOculusXRFunctionLibrary_SetPositionScale3D_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "Comment", "/**\n\x09 * Scales the HMD position that gets added to the virtual camera position.\n\x09 *\n\x09 * @param PosScale3D\x09(in) the scale to apply to the HMD position.\n\x09 */" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "This feature is no longer supported." },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "ToolTip", "Scales the HMD position that gets added to the virtual camera position.\n\n@param PosScale3D    (in) the scale to apply to the HMD position." },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UOculusXRFunctionLibrary_SetPositionScale3D_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOculusXRFunctionLibrary, nullptr, "SetPositionScale3D", nullptr, nullptr, sizeof(Z_Construct_UFunction_UOculusXRFunctionLibrary_SetPositionScale3D_Statics::OculusXRFunctionLibrary_eventSetPositionScale3D_Parms), Z_Construct_UFunction_UOculusXRFunctionLibrary_SetPositionScale3D_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_SetPositionScale3D_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04822401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOculusXRFunctionLibrary_SetPositionScale3D_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_SetPositionScale3D_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOculusXRFunctionLibrary_SetPositionScale3D()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UOculusXRFunctionLibrary_SetPositionScale3D_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOculusXRFunctionLibrary_SetSuggestedCpuAndGpuPerformanceLevels_Statics
	{
		struct OculusXRFunctionLibrary_eventSetSuggestedCpuAndGpuPerformanceLevels_Parms
		{
			EOculusXRProcessorPerformanceLevel CpuPerfLevel;
			EOculusXRProcessorPerformanceLevel GpuPerfLevel;
		};
		static const UECodeGen_Private::FBytePropertyParams NewProp_CpuPerfLevel_Underlying;
		static const UECodeGen_Private::FEnumPropertyParams NewProp_CpuPerfLevel;
		static const UECodeGen_Private::FBytePropertyParams NewProp_GpuPerfLevel_Underlying;
		static const UECodeGen_Private::FEnumPropertyParams NewProp_GpuPerfLevel;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_SetSuggestedCpuAndGpuPerformanceLevels_Statics::NewProp_CpuPerfLevel_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_SetSuggestedCpuAndGpuPerformanceLevels_Statics::NewProp_CpuPerfLevel = { "CpuPerfLevel", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventSetSuggestedCpuAndGpuPerformanceLevels_Parms, CpuPerfLevel), Z_Construct_UEnum_OculusXRHMD_EOculusXRProcessorPerformanceLevel, METADATA_PARAMS(nullptr, 0) }; // 3891469125
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_SetSuggestedCpuAndGpuPerformanceLevels_Statics::NewProp_GpuPerfLevel_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UOculusXRFunctionLibrary_SetSuggestedCpuAndGpuPerformanceLevels_Statics::NewProp_GpuPerfLevel = { "GpuPerfLevel", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(OculusXRFunctionLibrary_eventSetSuggestedCpuAndGpuPerformanceLevels_Parms, GpuPerfLevel), Z_Construct_UEnum_OculusXRHMD_EOculusXRProcessorPerformanceLevel, METADATA_PARAMS(nullptr, 0) }; // 3891469125
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOculusXRFunctionLibrary_SetSuggestedCpuAndGpuPerformanceLevels_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_SetSuggestedCpuAndGpuPerformanceLevels_Statics::NewProp_CpuPerfLevel_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_SetSuggestedCpuAndGpuPerformanceLevels_Statics::NewProp_CpuPerfLevel,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_SetSuggestedCpuAndGpuPerformanceLevels_Statics::NewProp_GpuPerfLevel_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOculusXRFunctionLibrary_SetSuggestedCpuAndGpuPerformanceLevels_Statics::NewProp_GpuPerfLevel,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOculusXRFunctionLibrary_SetSuggestedCpuAndGpuPerformanceLevels_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "Comment", "/**\n\x09* Set the suggested CPU and GPU levels to the Oculus device.\n\x09*/" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
		{ "ToolTip", "Set the suggested CPU and GPU levels to the Oculus device." },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UOculusXRFunctionLibrary_SetSuggestedCpuAndGpuPerformanceLevels_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOculusXRFunctionLibrary, nullptr, "SetSuggestedCpuAndGpuPerformanceLevels", nullptr, nullptr, sizeof(Z_Construct_UFunction_UOculusXRFunctionLibrary_SetSuggestedCpuAndGpuPerformanceLevels_Statics::OculusXRFunctionLibrary_eventSetSuggestedCpuAndGpuPerformanceLevels_Parms), Z_Construct_UFunction_UOculusXRFunctionLibrary_SetSuggestedCpuAndGpuPerformanceLevels_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_SetSuggestedCpuAndGpuPerformanceLevels_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOculusXRFunctionLibrary_SetSuggestedCpuAndGpuPerformanceLevels_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOculusXRFunctionLibrary_SetSuggestedCpuAndGpuPerformanceLevels_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOculusXRFunctionLibrary_SetSuggestedCpuAndGpuPerformanceLevels()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UOculusXRFunctionLibrary_SetSuggestedCpuAndGpuPerformanceLevels_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UOculusXRFunctionLibrary);
	UClass* Z_Construct_UClass_UOculusXRFunctionLibrary_NoRegister()
	{
		return UOculusXRFunctionLibrary::StaticClass();
	}
	struct Z_Construct_UClass_UOculusXRFunctionLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UOculusXRFunctionLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_OculusXRHMD,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UOculusXRFunctionLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UOculusXRFunctionLibrary_AddLoadingSplashScreen, "AddLoadingSplashScreen" }, // 1671138875
		{ &Z_Construct_UFunction_UOculusXRFunctionLibrary_ClearLoadingSplashScreens, "ClearLoadingSplashScreens" }, // 3951177607
		{ &Z_Construct_UFunction_UOculusXRFunctionLibrary_EnableOrientationTracking, "EnableOrientationTracking" }, // 2250966862
		{ &Z_Construct_UFunction_UOculusXRFunctionLibrary_EnablePositionTracking, "EnablePositionTracking" }, // 186898872
		{ &Z_Construct_UFunction_UOculusXRFunctionLibrary_GetAvailableDisplayFrequencies, "GetAvailableDisplayFrequencies" }, // 3050009940
		{ &Z_Construct_UFunction_UOculusXRFunctionLibrary_GetBaseRotationAndBaseOffsetInMeters, "GetBaseRotationAndBaseOffsetInMeters" }, // 721226645
		{ &Z_Construct_UFunction_UOculusXRFunctionLibrary_GetBaseRotationAndPositionOffset, "GetBaseRotationAndPositionOffset" }, // 2375291341
		{ &Z_Construct_UFunction_UOculusXRFunctionLibrary_GetControllerType, "GetControllerType" }, // 1108286772
		{ &Z_Construct_UFunction_UOculusXRFunctionLibrary_GetCurrentDisplayFrequency, "GetCurrentDisplayFrequency" }, // 2927925362
		{ &Z_Construct_UFunction_UOculusXRFunctionLibrary_GetDeviceName, "GetDeviceName" }, // 2543174103
		{ &Z_Construct_UFunction_UOculusXRFunctionLibrary_GetDeviceType, "GetDeviceType" }, // 3155211097
		{ &Z_Construct_UFunction_UOculusXRFunctionLibrary_GetEyeTrackedFoveatedRenderingSupported, "GetEyeTrackedFoveatedRenderingSupported" }, // 3777974520
		{ &Z_Construct_UFunction_UOculusXRFunctionLibrary_GetFoveatedRenderingLevel, "GetFoveatedRenderingLevel" }, // 3744276399
		{ &Z_Construct_UFunction_UOculusXRFunctionLibrary_GetFoveatedRenderingMethod, "GetFoveatedRenderingMethod" }, // 3788517479
		{ &Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGPUFrameTime, "GetGPUFrameTime" }, // 1178992723
		{ &Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGPUUtilization, "GetGPUUtilization" }, // 2027813549
		{ &Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGuardianDimensions, "GetGuardianDimensions" }, // 2602478984
		{ &Z_Construct_UFunction_UOculusXRFunctionLibrary_GetGuardianPoints, "GetGuardianPoints" }, // 3130602906
		{ &Z_Construct_UFunction_UOculusXRFunctionLibrary_GetHmdColorDesc, "GetHmdColorDesc" }, // 2899296483
		{ &Z_Construct_UFunction_UOculusXRFunctionLibrary_GetNodeGuardianIntersection, "GetNodeGuardianIntersection" }, // 1199023883
		{ &Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPlayAreaTransform, "GetPlayAreaTransform" }, // 3198099423
		{ &Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPointGuardianIntersection, "GetPointGuardianIntersection" }, // 1715045094
		{ &Z_Construct_UFunction_UOculusXRFunctionLibrary_GetPose, "GetPose" }, // 1077828534
		{ &Z_Construct_UFunction_UOculusXRFunctionLibrary_GetRawSensorData, "GetRawSensorData" }, // 2425636706
		{ &Z_Construct_UFunction_UOculusXRFunctionLibrary_GetSuggestedCpuAndGpuPerformanceLevels, "GetSuggestedCpuAndGpuPerformanceLevels" }, // 453352908
		{ &Z_Construct_UFunction_UOculusXRFunctionLibrary_GetSystemHmd3DofModeEnabled, "GetSystemHmd3DofModeEnabled" }, // 93596005
		{ &Z_Construct_UFunction_UOculusXRFunctionLibrary_GetUserProfile, "GetUserProfile" }, // 994633094
		{ &Z_Construct_UFunction_UOculusXRFunctionLibrary_HasInputFocus, "HasInputFocus" }, // 586793708
		{ &Z_Construct_UFunction_UOculusXRFunctionLibrary_HasSystemOverlayPresent, "HasSystemOverlayPresent" }, // 2741260207
		{ &Z_Construct_UFunction_UOculusXRFunctionLibrary_IsColorPassthroughSupported, "IsColorPassthroughSupported" }, // 530393085
		{ &Z_Construct_UFunction_UOculusXRFunctionLibrary_IsDeviceTracked, "IsDeviceTracked" }, // 1322976981
		{ &Z_Construct_UFunction_UOculusXRFunctionLibrary_IsGuardianConfigured, "IsGuardianConfigured" }, // 2124481616
		{ &Z_Construct_UFunction_UOculusXRFunctionLibrary_IsGuardianDisplayed, "IsGuardianDisplayed" }, // 251843290
		{ &Z_Construct_UFunction_UOculusXRFunctionLibrary_IsPassthroughSupported, "IsPassthroughSupported" }, // 2569928076
		{ &Z_Construct_UFunction_UOculusXRFunctionLibrary_SetBaseRotationAndBaseOffsetInMeters, "SetBaseRotationAndBaseOffsetInMeters" }, // 1544057669
		{ &Z_Construct_UFunction_UOculusXRFunctionLibrary_SetBaseRotationAndPositionOffset, "SetBaseRotationAndPositionOffset" }, // 1753171022
		{ &Z_Construct_UFunction_UOculusXRFunctionLibrary_SetClientColorDesc, "SetClientColorDesc" }, // 499059602
		{ &Z_Construct_UFunction_UOculusXRFunctionLibrary_SetColorScaleAndOffset, "SetColorScaleAndOffset" }, // 1148478812
		{ &Z_Construct_UFunction_UOculusXRFunctionLibrary_SetCPUAndGPULevels, "SetCPUAndGPULevels" }, // 1124269654
		{ &Z_Construct_UFunction_UOculusXRFunctionLibrary_SetDisplayFrequency, "SetDisplayFrequency" }, // 2055857913
		{ &Z_Construct_UFunction_UOculusXRFunctionLibrary_SetFoveatedRenderingLevel, "SetFoveatedRenderingLevel" }, // 3306260990
		{ &Z_Construct_UFunction_UOculusXRFunctionLibrary_SetFoveatedRenderingMethod, "SetFoveatedRenderingMethod" }, // 953072568
		{ &Z_Construct_UFunction_UOculusXRFunctionLibrary_SetGuardianVisibility, "SetGuardianVisibility" }, // 3459772886
		{ &Z_Construct_UFunction_UOculusXRFunctionLibrary_SetLocalDimmingOn, "SetLocalDimmingOn" }, // 2505563310
		{ &Z_Construct_UFunction_UOculusXRFunctionLibrary_SetPositionScale3D, "SetPositionScale3D" }, // 3740830627
		{ &Z_Construct_UFunction_UOculusXRFunctionLibrary_SetSuggestedCpuAndGpuPerformanceLevels, "SetSuggestedCpuAndGpuPerformanceLevels" }, // 1483247467
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOculusXRFunctionLibrary_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "OculusXRFunctionLibrary.h" },
		{ "ModuleRelativePath", "Public/OculusXRFunctionLibrary.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UOculusXRFunctionLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UOculusXRFunctionLibrary>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UOculusXRFunctionLibrary_Statics::ClassParams = {
		&UOculusXRFunctionLibrary::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UOculusXRFunctionLibrary_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UOculusXRFunctionLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UOculusXRFunctionLibrary()
	{
		if (!Z_Registration_Info_UClass_UOculusXRFunctionLibrary.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UOculusXRFunctionLibrary.OuterSingleton, Z_Construct_UClass_UOculusXRFunctionLibrary_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UOculusXRFunctionLibrary.OuterSingleton;
	}
	template<> OCULUSXRHMD_API UClass* StaticClass<UOculusXRFunctionLibrary>()
	{
		return UOculusXRFunctionLibrary::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UOculusXRFunctionLibrary);
	UOculusXRFunctionLibrary::~UOculusXRFunctionLibrary() {}
	struct Z_CompiledInDeferFile_FID_OculusXR_release_HostProject_Plugins_OculusXR_Source_OculusXRHMD_Public_OculusXRFunctionLibrary_h_Statics
	{
		static const FEnumRegisterCompiledInInfo EnumInfo[];
		static const FStructRegisterCompiledInInfo ScriptStructInfo[];
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FEnumRegisterCompiledInInfo Z_CompiledInDeferFile_FID_OculusXR_release_HostProject_Plugins_OculusXR_Source_OculusXRHMD_Public_OculusXRFunctionLibrary_h_Statics::EnumInfo[] = {
		{ EOculusXRTrackedDeviceType_StaticEnum, TEXT("EOculusXRTrackedDeviceType"), &Z_Registration_Info_UEnum_EOculusXRTrackedDeviceType, CONSTRUCT_RELOAD_VERSION_INFO(FEnumReloadVersionInfo, 125253818U) },
		{ EOculusXRFoveatedRenderingMethod_StaticEnum, TEXT("EOculusXRFoveatedRenderingMethod"), &Z_Registration_Info_UEnum_EOculusXRFoveatedRenderingMethod, CONSTRUCT_RELOAD_VERSION_INFO(FEnumReloadVersionInfo, 709529947U) },
		{ EOculusXRFoveatedRenderingLevel_StaticEnum, TEXT("EOculusXRFoveatedRenderingLevel"), &Z_Registration_Info_UEnum_EOculusXRFoveatedRenderingLevel, CONSTRUCT_RELOAD_VERSION_INFO(FEnumReloadVersionInfo, 37067298U) },
		{ EOculusXRBoundaryType_StaticEnum, TEXT("EOculusXRBoundaryType"), &Z_Registration_Info_UEnum_EOculusXRBoundaryType, CONSTRUCT_RELOAD_VERSION_INFO(FEnumReloadVersionInfo, 938409106U) },
		{ EOculusXRColorSpace_StaticEnum, TEXT("EOculusXRColorSpace"), &Z_Registration_Info_UEnum_EOculusXRColorSpace, CONSTRUCT_RELOAD_VERSION_INFO(FEnumReloadVersionInfo, 2189630421U) },
		{ EOculusXRHandTrackingSupport_StaticEnum, TEXT("EOculusXRHandTrackingSupport"), &Z_Registration_Info_UEnum_EOculusXRHandTrackingSupport, CONSTRUCT_RELOAD_VERSION_INFO(FEnumReloadVersionInfo, 4061556218U) },
		{ EOculusXRHandTrackingFrequency_StaticEnum, TEXT("EOculusXRHandTrackingFrequency"), &Z_Registration_Info_UEnum_EOculusXRHandTrackingFrequency, CONSTRUCT_RELOAD_VERSION_INFO(FEnumReloadVersionInfo, 4151629882U) },
		{ EOculusXRHandTrackingVersion_StaticEnum, TEXT("EOculusXRHandTrackingVersion"), &Z_Registration_Info_UEnum_EOculusXRHandTrackingVersion, CONSTRUCT_RELOAD_VERSION_INFO(FEnumReloadVersionInfo, 105284321U) },
		{ EOculusXRProcessorPerformanceLevel_StaticEnum, TEXT("EOculusXRProcessorPerformanceLevel"), &Z_Registration_Info_UEnum_EOculusXRProcessorPerformanceLevel, CONSTRUCT_RELOAD_VERSION_INFO(FEnumReloadVersionInfo, 3891469125U) },
		{ EOculusXRDeviceType_StaticEnum, TEXT("EOculusXRDeviceType"), &Z_Registration_Info_UEnum_EOculusXRDeviceType, CONSTRUCT_RELOAD_VERSION_INFO(FEnumReloadVersionInfo, 4006955709U) },
		{ EOculusXRControllerType_StaticEnum, TEXT("EOculusXRControllerType"), &Z_Registration_Info_UEnum_EOculusXRControllerType, CONSTRUCT_RELOAD_VERSION_INFO(FEnumReloadVersionInfo, 2817971156U) },
		{ EOculusXRXrApi_StaticEnum, TEXT("EOculusXRXrApi"), &Z_Registration_Info_UEnum_EOculusXRXrApi, CONSTRUCT_RELOAD_VERSION_INFO(FEnumReloadVersionInfo, 1075886423U) },
		{ EOculusXRControllerPoseAlignment_StaticEnum, TEXT("EOculusXRControllerPoseAlignment"), &Z_Registration_Info_UEnum_EOculusXRControllerPoseAlignment, CONSTRUCT_RELOAD_VERSION_INFO(FEnumReloadVersionInfo, 2197898862U) },
	};
	const FStructRegisterCompiledInInfo Z_CompiledInDeferFile_FID_OculusXR_release_HostProject_Plugins_OculusXR_Source_OculusXRHMD_Public_OculusXRFunctionLibrary_h_Statics::ScriptStructInfo[] = {
		{ FOculusXRHmdUserProfileField::StaticStruct, Z_Construct_UScriptStruct_FOculusXRHmdUserProfileField_Statics::NewStructOps, TEXT("OculusXRHmdUserProfileField"), &Z_Registration_Info_UScriptStruct_OculusXRHmdUserProfileField, CONSTRUCT_RELOAD_VERSION_INFO(FStructReloadVersionInfo, sizeof(FOculusXRHmdUserProfileField), 3875323592U) },
		{ FOculusXRHmdUserProfile::StaticStruct, Z_Construct_UScriptStruct_FOculusXRHmdUserProfile_Statics::NewStructOps, TEXT("OculusXRHmdUserProfile"), &Z_Registration_Info_UScriptStruct_OculusXRHmdUserProfile, CONSTRUCT_RELOAD_VERSION_INFO(FStructReloadVersionInfo, sizeof(FOculusXRHmdUserProfile), 2512357364U) },
		{ FOculusXRGuardianTestResult::StaticStruct, Z_Construct_UScriptStruct_FOculusXRGuardianTestResult_Statics::NewStructOps, TEXT("OculusXRGuardianTestResult"), &Z_Registration_Info_UScriptStruct_OculusXRGuardianTestResult, CONSTRUCT_RELOAD_VERSION_INFO(FStructReloadVersionInfo, sizeof(FOculusXRGuardianTestResult), 910032650U) },
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_OculusXR_release_HostProject_Plugins_OculusXR_Source_OculusXRHMD_Public_OculusXRFunctionLibrary_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UOculusXRFunctionLibrary, UOculusXRFunctionLibrary::StaticClass, TEXT("UOculusXRFunctionLibrary"), &Z_Registration_Info_UClass_UOculusXRFunctionLibrary, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UOculusXRFunctionLibrary), 2959971376U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_OculusXR_release_HostProject_Plugins_OculusXR_Source_OculusXRHMD_Public_OculusXRFunctionLibrary_h_2382117929(TEXT("/Script/OculusXRHMD"),
		Z_CompiledInDeferFile_FID_OculusXR_release_HostProject_Plugins_OculusXR_Source_OculusXRHMD_Public_OculusXRFunctionLibrary_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_OculusXR_release_HostProject_Plugins_OculusXR_Source_OculusXRHMD_Public_OculusXRFunctionLibrary_h_Statics::ClassInfo),
		Z_CompiledInDeferFile_FID_OculusXR_release_HostProject_Plugins_OculusXR_Source_OculusXRHMD_Public_OculusXRFunctionLibrary_h_Statics::ScriptStructInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_OculusXR_release_HostProject_Plugins_OculusXR_Source_OculusXRHMD_Public_OculusXRFunctionLibrary_h_Statics::ScriptStructInfo),
		Z_CompiledInDeferFile_FID_OculusXR_release_HostProject_Plugins_OculusXR_Source_OculusXRHMD_Public_OculusXRFunctionLibrary_h_Statics::EnumInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_OculusXR_release_HostProject_Plugins_OculusXR_Source_OculusXRHMD_Public_OculusXRFunctionLibrary_h_Statics::EnumInfo));
PRAGMA_ENABLE_DEPRECATION_WARNINGS
